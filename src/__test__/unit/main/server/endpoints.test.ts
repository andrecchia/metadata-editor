import { ServerAPI } from "main/server/ServerAPI";
import express from 'express';
import { BrowserWindow, BrowserWindowConstructorOptions } from "electron";
import request from "supertest";
import { createServer } from "http";

// Mock BrowserWindow
jest.mock('electron', () => {
    return {
      BrowserWindow: class BrowserWindow {
        constructor(options: BrowserWindowConstructorOptions) {
        }
  
        // Mocking the send method
        webContents = {
          send: jest.fn(),
        };
      },
    };
  });  



const expressApp = express()

describe('Test actual endpoint calls',()=>{
    let server
  
    beforeEach(() => {
      server = createServer(expressApp)
      server.listen(3333, () => {  
        console.log(`Server listening on port 3333`);
      })
    });
    afterEach(()=>{
      server.close()
    })  
    
    it("Invalid JSON in request body",async ()=>{
      new ServerAPI(new BrowserWindow(), expressApp, 3333);

      // Do the POST request
      const requestBody = "Wrong JSON";
      const response = await request(expressApp)
        .post("/api/v1/compile")
        .set('Content-Type', 'application/json')
        .send(requestBody);
  
        expect(response.statusCode).toBe(400);
        expect(response.text).toBe(JSON.stringify({
          error: 'Bad Request',
          message: 'The body of the request is not valid JSON'
        }));
    })

    it("Unsupported type in request body",async ()=>{
        new ServerAPI(new BrowserWindow(), expressApp, 3333);
  
        // Do the POST request
        const requestBody = "Plain text";
        const response = await request(expressApp)
        .post("/api/v1/compile")
        .set('Content-Type', 'text/plain')
        .send(requestBody);


        expect(response.statusCode).toBe(415);
        expect(response.text).toBe(JSON.stringify({
            error: 'Unsupported Media Type',
            message: 'Content-Type must be application/json'
        }));
    })

    it("GET request to a non-existent endpoint", async () => {

        // Do the GET request
        const dummyEndpoint = "/dummyEndpoint";
        const response = await request(expressApp).get(dummyEndpoint);
    
        // Assertion
        expect(response.statusCode).toBe(404);
        expect(response.text).
        toEqual(expect.stringContaining("Cannot GET " + dummyEndpoint));
    });

    it("POST request to a non-existent endpoint", async () => {

        const dummyEndpoint = "/dummyEndpoint";
        // Do the POST request
        const requestBody = {
          "study.studyTitle": "Prova title",
          "study.program": "Prova program",
          "fake.path": "Fake value"
        }
        const response = await request(expressApp)
          .post(dummyEndpoint)
          .set('Content-Type', 'application/json')
          .send(requestBody);
    
        // Assertion
        expect(response.statusCode).toBe(404);
        expect(response.text).
        toEqual(expect.stringContaining("Cannot POST " + dummyEndpoint));
      });

  })