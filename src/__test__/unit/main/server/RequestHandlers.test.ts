import { RequestHandlers } from 'main/server/RequestHandlers';
import { BrowserWindow, BrowserWindowConstructorOptions, IpcMainEvent, IpcRendererEvent} from 'electron';
import { ipcRenderer, ipcMain } from '__test__/test-utils/electron-mock';
import { ElectronChannel } from 'main/electronChannels';

// Mock BrowserWindow and ipcMain
const webContentSend = jest.fn().mockImplementation((channel, message) =>{
    ipcMain.on("trigger-event", (event:IpcMainEvent) => {
        event.sender.send(channel, message);
   })
})
jest.mock('electron', () => {
    return {
      ...jest.requireActual('electron'),
      BrowserWindow: class BrowserWindow {
        constructor(options: BrowserWindowConstructorOptions) {
        }
          // Mocking the send method
        webContents = {
          send: webContentSend
        };
      }
    };
  }); 


describe('RequestHandlers', () => {
  describe('handleCompileRequest', () => {
    beforeEach(() => {
        /**
         * To send message from main to render:
         * 1) trigger fake event `trigger-event` from renderer
         * 2) listen that renderer on BrowserWindow().webContents.send() and use the callback
         *    to send a message to renderer
         */
        ipcRenderer.send('trigger-event')
      })
    it('Correct request and response', async () => {
      
        ipcRenderer.once(ElectronChannel.API_REQUEST, (event:IpcRendererEvent)=>{
            const respBack={requestName: "RESP_COMPILE", resp: []}
            event.sender.send('API:response', JSON.stringify(respBack))
        })
      const requestHandlers = new RequestHandlers(new BrowserWindow());
      const req = { path: '/api/v1/compile', body: { field0: 'content0', field1: { field2: 'content2' } } };
      const res = {
        status: jest.fn(function(code){
            return this;
          }),
        send: jest.fn(),
        json: jest.fn()
      };

      requestHandlers.handleCompileRequest(req, res);

      // Await for the response
      await new Promise(resolve => setTimeout(resolve, 50)); 

      expect(webContentSend).toHaveBeenCalledWith(ElectronChannel.API_REQUEST,JSON.stringify(req).replace('body', 'content'))
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.send).toHaveBeenCalled();     
      expect(res.json).not.toHaveBeenCalled();     
    });

    it('Compile a non-existent schema path', async () => {
      
        const wrongPaths = ['some','non.field']
        ipcRenderer.once(ElectronChannel.API_REQUEST, (event:IpcRendererEvent)=>{
            const respBack={requestName: "RESP_COMPILE", resp: wrongPaths}
            event.sender.send('API:response', JSON.stringify(respBack))
        })
      const requestHandlers = new RequestHandlers(new BrowserWindow());
      const req = { path: '/api/v1/compile', body: { field0: 'content0', field1: { field2: 'content2' } } };
      const res = {
        status: jest.fn(function(code){
            return this;
          }),
        send: jest.fn(),
        json: jest.fn(function(arg){
            return arg
        })
      };

      requestHandlers.handleCompileRequest(req, res);

      // Await for the response
      await new Promise(resolve => setTimeout(resolve, 50)); 

      expect(webContentSend).toHaveBeenCalledWith(ElectronChannel.API_REQUEST,JSON.stringify(req).replace('body', 'content'))
      expect(res.status).toHaveBeenCalledWith(422);
      expect(res.json).toHaveBeenCalledWith({
        error: "Invalid fields",
        message : `Cannot find [${wrongPaths}] keys in the schema`
      })
      expect(res.send).not.toHaveBeenCalled();     
    });

    it('Bad response', async () => {
      
        ipcRenderer.once(ElectronChannel.API_REQUEST, (event:IpcRendererEvent)=>{
            const respBack='bad response'
            event.sender.send('API:response', respBack)
        })
      const requestHandlers = new RequestHandlers(new BrowserWindow());
      const req = { path: '/api/v1/compile', body: { field0: 'content0', field1: { field2: 'content2' } } };
      const res = {
        status: jest.fn(function(code){
            return this;
          }),
        send: jest.fn(),
        json: jest.fn(function(arg){
            return arg
        })
      };

      requestHandlers.handleCompileRequest(req, res);

      // Await for the response
      await new Promise(resolve => setTimeout(resolve, 50)); 

      expect(webContentSend).toHaveBeenCalledWith(ElectronChannel.API_REQUEST,JSON.stringify(req).replace('body', 'content'))
      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.send).not.toHaveBeenCalled();     
      expect(res.json).not.toHaveBeenCalled();     
    });
  });

  describe('handleReadRequest', () => {
    beforeEach(() => {
        /**
         * To send message from main to render:
         * 1) trigger fake event `trigger-event` from renderer
         * 2) listen that renderer on BrowserWindow().webContents.send() and use the callback
         *    to send a message to renderer
         */
        ipcRenderer.send('trigger-event')
      })
    it('Correct request and response', async ()=>{

        const response = { field0: 'content0', field1: { field2: 'content2' } } 
        ipcRenderer.once(ElectronChannel.API_REQUEST, (event:IpcRendererEvent)=>{
            const respBack={requestName: "RESP_READ", resp: response}
            event.sender.send('API:response', JSON.stringify(respBack))
        })
      const requestHandlers = new RequestHandlers(new BrowserWindow());
      const req = { path: '/api/v1/read' };
      const res = {
        status: jest.fn(function(code){
            return this;
          }),
        send: jest.fn(),
        json: jest.fn()
      };

      requestHandlers.handleReadRequest(req, res);

      // Await for the response
      await new Promise(resolve => setTimeout(resolve, 50)); 

      expect(webContentSend).toHaveBeenCalledWith(ElectronChannel.API_REQUEST,JSON.stringify({...req, content: undefined}))
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith(response);     
      expect(res.send).not.toHaveBeenCalled();     
    })

    it('Bad response', async ()=>{
       ipcRenderer.once(ElectronChannel.API_REQUEST, (event:IpcRendererEvent)=>{
            event.sender.send('API:response')
        })
      const requestHandlers = new RequestHandlers(new BrowserWindow());
      const req = { path: '/api/v1/read' };
      const res = {
        status: jest.fn(function(code){
            return this;
          }),
        send: jest.fn(),
        json: jest.fn()
      };

      requestHandlers.handleReadRequest(req, res);

      // Await for the response
      await new Promise(resolve => setTimeout(resolve, 50)); 

      expect(webContentSend).toHaveBeenCalledWith(ElectronChannel.API_REQUEST,JSON.stringify({...req, content: undefined}))
      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).not.toHaveBeenCalledWith();     
      expect(res.send).not.toHaveBeenCalled();     
    })
  });
});
