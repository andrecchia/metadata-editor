import { ServerAPI, handleNotValidJSON, acceptOnlyJSON } from "main/server/ServerAPI";
import express, { json, static as staticExpress, Express, Request, Response } from 'express';
import { BrowserWindow, BrowserWindowConstructorOptions } from "electron";
import cors from 'cors';
import { join } from 'path';

// Mock BrowserWindow
jest.mock('electron', () => {
    return {
      BrowserWindow: class BrowserWindow {
        constructor(options: BrowserWindowConstructorOptions) {
        }
  
        // Mocking the send method
        webContents = {
          send: jest.fn(),
        };
      },
    };
  });  

jest.mock('main/server/swagger/swagger.json', () => ({
  __esModule: true, // If the module uses ES modules
  default: { 
      openapi: "3.0.0",
      info: {
        title: "Metadata Editor API",
        description: "API documentation for Metadata Editor"
      },
      servers: [
        {
          url: "http://localhost:3333"
        }
      ],
    }
  })
);

describe('ServerAPI test', () => {
  let mainWindowMock;

  beforeEach(() => {
      mainWindowMock = new BrowserWindow();
  });

  afterEach(() => {
      jest.clearAllMocks();
  });

  it('Should initialize correctly', () => {
      const expressUse = jest.fn();
      jest.spyOn(express.application, 'use').mockImplementation(expressUse);
      const expressApp = express()

      new ServerAPI(mainWindowMock, expressApp, 3333);

      expect(expressUse).toHaveBeenCalledTimes(6);
  });

  it('Should call the middlewares required', () => {
    const expressApp = express()
    const useMock = jest.spyOn(expressApp, 'use');

    new ServerAPI(mainWindowMock, expressApp, 3333);

    // Extract the middleware functions from the calls to expressApp.use
    const middlewareFunctions = useMock.mock.calls.map(call => call[0]);

    // Check if the middleware stack contains the expected middleware functions
    expect(middlewareFunctions).toContain("/api-docs");
    expect(middlewareFunctions.some(fn => fn.toString() === cors().toString())).toBe(true);
    expect(middlewareFunctions.some(fn => fn.toString() === json().toString())).toBe(true);
    expect(middlewareFunctions.some(fn => fn.toString() === handleNotValidJSON.toString())).toBe(true);
    expect(middlewareFunctions.some(fn => fn.toString() === acceptOnlyJSON.toString())).toBe(true);
    expect(middlewareFunctions.some(fn => fn.toString() === staticExpress(join(__dirname, '../../build')).toString())).toBe(true);
  });

  it('Should change server port when the relative env variable is defined', () => {
    const expressApp = express()
    const newPort = "666"
    process.env.ME_API_PORT = newPort
    new ServerAPI(mainWindowMock, expressApp, 3333);

    const mockedSwaggerDocument = require('main/server/swagger/swagger.json').default;
    expect(mockedSwaggerDocument.servers[0].url).toEqual(`http://localhost:${newPort}`)
  });

  it('Should not serve static files in development', () => {
      process.env.NODE_ENV = 'development';
      const expressApp = express()
      const staticUse = jest.spyOn(expressApp, 'use');

      new ServerAPI(mainWindowMock, expressApp, 3333);

      const middlewareFunctions = staticUse.mock.calls.map(call => call[0]);
      expect(middlewareFunctions.some(fn => fn.toString() === staticExpress(join(__dirname, '../../build')).toString())).toBe(false);

  });

  it('Should correctly set up API endpoints', () => {
      const expressApp = express()
      const postMock = jest.spyOn(expressApp, 'post');
      const getMock = jest.spyOn(expressApp, 'get');

      new ServerAPI(mainWindowMock, expressApp, 3333);

      expect(postMock).toHaveBeenCalledWith('/api/v1/compile', expect.any(Function));
      expect(getMock).toHaveBeenCalledWith('/api/v1/read', expect.any(Function));
      expect(getMock).toHaveBeenCalledWith('/app*', expect.any(Function));
  });
});


describe('handleNotValidJSON middleware', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;
  let next: jest.Mock;

  beforeEach(() => {
      req = { headers: {}, method: 'POST' };
      res = { status: jest.fn().mockReturnThis(), json: jest.fn() };
      next = jest.fn();
  });

  it('should return 400 with error message when request body is not valid JSON', () => {
      const err = new SyntaxError('Invalid JSON');
      (err as any).status = 400;
      (err as any).body = true;

      handleNotValidJSON(err, req as Request, res as Response, next);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith({
          error: 'Bad Request',
          message: 'The body of the request is not valid JSON'
      });
  });

  it('should call next middleware if error is not related to JSON syntax', () => {
      const err = new Error('Some other error');
      handleNotValidJSON(err, req as Request, res as Response, next);

      expect(next).toHaveBeenCalled();
  });
});



describe('acceptOnlyJSON middleware', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;
  let next: jest.Mock;

  beforeEach(() => {
      req = { headers: { 'content-type': 'application/json' }, method: 'POST' };
      res = { status: jest.fn().mockReturnThis(), json: jest.fn() };
      next = jest.fn();
  });

  test('should call next middleware if content-type is application/json', () => {
      acceptOnlyJSON(req as Request, res as Response, next);

      expect(next).toHaveBeenCalled();
  });

  test('should return 415 with error message if content-type is not application/json', () => {
      req.headers!['content-type'] = 'text/plain';

      acceptOnlyJSON(req as Request, res as Response, next);

      expect(res.status).toHaveBeenCalledWith(415);
      expect(res.json).toHaveBeenCalledWith({
          error: 'Unsupported Media Type',
          message: 'Content-Type must be application/json'
      });
  });
});