import { resolveHtmlPath } from 'main/util';
import * as path from 'path';

describe('resolveHtmlPath', () => {
  const originalEnv = process.env.NODE_ENV;

  afterEach(() => {
    process.env.NODE_ENV = originalEnv;
  });

  it('should return development URL when NODE_ENV is development', () => {
    process.env.NODE_ENV = 'development';
    process.env.PORT = '3000'; // Set a sample port for testing

    const htmlFileName = 'index.html';
    const expectedUrl = `http://localhost:3000/${htmlFileName}`;

    expect(resolveHtmlPath(htmlFileName)).toBe(expectedUrl);
  });

  it('should return file path when NODE_ENV is not development', () => {
    process.env.NODE_ENV = 'production';

    const htmlFileName = 'index.html';
    const expectedPath = `file://${path.resolve(__dirname, '../../../renderer/', htmlFileName)}`;

    expect(resolveHtmlPath(htmlFileName)).toBe(expectedPath);
  });
});