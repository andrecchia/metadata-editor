import { MaskHandler, flattenToArrays } from "main/MaskHandler/MaskHandler";
import maskAllFalse from "__test__/test-utils/masks/sem_mask_all_false.json"
import maskWithTrue from "__test__/test-utils/masks/sem_mask_with_true.json"
import mriMask from "__test__/test-utils/masks/mri_schema_all_false.json"
import { flattenedToArrayMaskRes } from "__test__/test-utils/masks/results";
import { BrowserWindow, BrowserWindowConstructorOptions } from 'electron';
import { ElectronChannel } from "main/electronChannels";

// Mock BrowserWindow
jest.mock('electron', () => {
    return {
      BrowserWindow: class BrowserWindow {
        constructor(options: BrowserWindowConstructorOptions) {
        }
  
        // Mocking the send method
        webContents = {
          send: jest.fn(),
        };
      },
    };
  });

describe('flattenToArrays tests', () => {
    it('mask all false test', () => {
        const {flattenedToArrayMask, initialTrueKeys} = flattenToArrays(maskAllFalse, "sem@18")
        expect(flattenedToArrayMask).toEqual(flattenedToArrayMaskRes)
        expect(initialTrueKeys).toEqual([])
    })
    it('mask with true test', () =>{
        const initialTrueKeysRes = [
            'sem@18.entry.technique',
            'sem@18.entry.measurementDescription',
            'sem@18.entry.endTime',
            'sem@18.entry.instrument.FIB.FIBSpotSize.uncertainty.value',
            'sem@18.entry.instrument.chamberPressure.value',
            'sem@18.entry.instrument.chamberPressure.unit',
            'sem@18.entry.instrument.instrumentManufacturer.modelName',
            'sem@18.entry.instrument.instrumentID.identifierValue',
            'sem@18.entry.revision.revisonID.identifierType',
            'sem@18.entry.entryID.identifierType'
          ]
          const {flattenedToArrayMask, initialTrueKeys} = flattenToArrays(maskWithTrue, "sem@18")
          expect(flattenedToArrayMask).toEqual(flattenedToArrayMaskRes)
          expect(initialTrueKeys).toEqual(initialTrueKeysRes)
    })
})

describe('MaskHandler test', () =>  {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('test getGroupVisibilityState', () => {
        const fakeWindow = new BrowserWindow()
        const maskHandler = new MaskHandler(fakeWindow, maskWithTrue , "sem@18")
    
        // Assertion on call to the window `send` function
        expect(fakeWindow.webContents.send).toHaveBeenCalledWith(ElectronChannel.ELECTRON_STORE_DID_CHANGE + 'sem@18.entry', true);
        expect(fakeWindow.webContents.send).toHaveBeenCalledTimes(11)

        // Assertion on good cases based on values of `maskWithTrue` mask
        expect(maskHandler.getGroupVisibilityState('sem@18.entry')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.entryID')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.revision')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.revision.revisonID')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.instrumentID')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.instrumentManufacturer')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.chamberPressure')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.FIB')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.FIB.FIBSpotSize')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.FIB.FIBSpotSize.uncertainty')).toEqual(true)

        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.sourceID')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.accelerationVoltage')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.beamCurrent')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.sourceLifetime')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.gunVacuum')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.eBeamSource.gunPressure')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.instrument.stage.eBeamWorkingDistance')).toEqual(false)

        // assertion on fake path
        expect(maskHandler.getGroupVisibilityState('absolutely.inexistent.path.whatsoever')).toEqual(undefined)
    })
    it('test setGroupsVisibilityState', () => {
        const fakeWindow = new BrowserWindow()
        const maskHandler = new MaskHandler(fakeWindow, maskAllFalse , "sem@18")

        // Assert before to change the visibility
        expect(maskHandler.getGroupVisibilityState('sem@18.entry')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation.institutionID')).toEqual(false)

        // Change visibility state to true for some elements
        maskHandler.setGroupsVisibilityState('sem@18.entry.user.affiliation.institutionID.someValue', true)

        // Assertion on call to the window `send` function
        expect(fakeWindow.webContents.send).toHaveBeenCalledWith(ElectronChannel.ELECTRON_STORE_DID_CHANGE + 'sem@18.entry.user.affiliation.institutionID', true)
        expect(fakeWindow.webContents.send).toHaveBeenCalledTimes(4)

        // Assert after having changed the visibility to true
        expect(maskHandler.getGroupVisibilityState('sem@18.entry')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation')).toEqual(true)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation.institutionID')).toEqual(true)

        // Change back visibility to false
        maskHandler.setGroupsVisibilityState('sem@18.entry.user.affiliation.institutionID.someValue', false)

        // Assertion on call to the window `send` function
        expect(fakeWindow.webContents.send).toHaveBeenCalledWith(ElectronChannel.ELECTRON_STORE_DID_CHANGE + 'sem@18.entry.user.affiliation.institutionID', false)
        expect(fakeWindow.webContents.send).toHaveBeenCalledTimes(8)

        // Assert after having changed the visibility to false
        expect(maskHandler.getGroupVisibilityState('sem@18.entry')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation')).toEqual(false)
        expect(maskHandler.getGroupVisibilityState('sem@18.entry.user.affiliation.institutionID')).toEqual(false)

    })
    it('test insertIntoGroupVisibilityState', () => {
      const fakeWindow = new BrowserWindow()
      const maskHandler = new MaskHandler(fakeWindow, mriMask , "mri_schema@8")

      // Assertion before insertion
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series')).toEqual(undefined)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.sequenceProtocol')).toEqual(undefined)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.images')).toEqual(undefined)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.images.allImages')).toEqual(undefined)

      const pathToInsert = 'mri_schema@8.study.series'
      const maskToInsert = {
          "seriesID": false,
          "seriesTitle": false,
          "sequenceProtocol": {
              "sequenceProtocolName": false,
              "effectiveEchoTime": {
                  "value": false,
                  "unit": false
              },
              "repetitionTime": {
                  "value": false,
                  "unit": false
              },
              "flipAngle": {
                  "value": false,
                  "unit": false
              }
          },
          "images": {
              "allImages": {
                  "numberOfImages": false,
                  "pixelSpacing": {
                      "unit": false
                  },
                  "sliceThickness": {
                      "value": false,
                      "unit": false
                  },
                  "imageSize": {
                      "rows": false,
                      "columns": false
                  },
                  "pixelRange": {
                      "pixelBandwidth": {
                          "value": false,
                          "unit": false
                      },
                      "smallestImagePixelValue": false,
                      "largestImagePixelValue": false,
                      "pixelRepresentation": false
                  }
              }
          }
      }

      // Insertion
      maskHandler.insertIntoGroupVisibilityState(maskToInsert, pathToInsert)

      // Assertion after insertion
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series')).toEqual(false)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.sequenceProtocol')).toEqual(false)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.images')).toEqual(false)
      expect(maskHandler.getGroupVisibilityState('mri_schema@8.study.series.images.allImages')).toEqual(false)

    })

})