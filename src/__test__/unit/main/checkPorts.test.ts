import { checkPort, checkPorts } from 'main/checkPorts'; // Update the path accordingly

jest.mock('detect-port', () => jest.fn());
jest.mock('electron', () => ({
  app: {
    whenReady: jest.fn(() => Promise.resolve())
  },
  Notification: jest.fn(() => ({
    show: jest.fn()
  }))
}));

const mockExit = jest.spyOn(process, 'exit')
    .mockImplementation((number) => { throw new Error('process.exit: ' + number); });

describe('checkPort', () => {
    afterEach(()=>{
        jest.clearAllMocks()
    })
    it('should exit process if port is undefined', async () => {
        // Mock console.error
        console.error = jest.fn(); 
        // Force an undefined value to a string type
        let undefinedValue
        const undefinedString:string = undefinedValue
        try {
            await checkPort(undefinedString, 'testPort');
        } catch (e) {
            expect(String(e)).toBe('Error: process.exit: 1');
            expect(console.error).toHaveBeenCalled();
            expect(process.exit).toHaveBeenCalledWith(1);
        }
    })

    it('should exit process if port is not an integer string', async () => {
        console.error = jest.fn(); // Mock console.error
        try{
            await checkPort('abc', 'testPort');
        } catch(e){
            expect(String(e)).toBe('Error: process.exit: 1');
            expect(console.error).toHaveBeenCalled();
            expect(process.exit).toHaveBeenCalledWith(1);
        }
    })

    it('should exit process if port is out of range', async () => {
        console.error = jest.fn(); // Mock console.error
        try{
            await checkPort(100000, 'testPort');
        } catch(e){
            expect(String(e)).toBe('Error: process.exit: 1');
            expect(console.error).toHaveBeenCalled();
            expect(process.exit).toHaveBeenCalledWith(1);
        }
    });

    it('should exit process if port is occupied', async () => {
        console.error = jest.fn(); // Mock console.error
        require('detect-port').mockReturnValue(92345); // Mock port as occupied
        try{
            await checkPort(12345, 'rest API');
        } catch(e){
            expect(String(e)).toBe('Error: process.exit: 1');
            expect(console.error).toHaveBeenCalled();
            expect(process.exit).toHaveBeenCalledWith(1);
        }
    });

    it('should handle unexpected error', async () => {
        console.error = jest.fn(); // Mock console.error
        require('detect-port').mockRejectedValue(new Error('Test error')); // Mock error
        try{
            await checkPort(12345, 'testPort');
        } catch(e){
            expect(String(e)).toBe('Error: process.exit: 1');
            expect(console.error).toHaveBeenCalled();
            expect(process.exit).toHaveBeenCalledWith(1);
        }
    });

    it('should handle valid port without errors', async () => {
        console.error = jest.fn(); // Mock console.error
        require('detect-port').mockReturnValue(12345); // Mock port as available
        await checkPort(12345, 'testPort');
        expect(console.error).not.toHaveBeenCalled();
        expect(process.exit).not.toHaveBeenCalled();
    });

});