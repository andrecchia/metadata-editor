import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom'
import App from 'renderer/App'
import {BrowserRouter} from 'react-router-dom'
import { METAREPO } from 'renderer/constants'


describe('App rendering',() => { 
  beforeAll(()=>{
    window.localStorage.setItem('lastMetaStoreInstance', METAREPO)
  })
  afterAll(()=>{
    window.localStorage.clear()
  })

  it('Home rendering',async () => {
    render(<App />, {wrapper: BrowserRouter})
    const user = userEvent.setup()
  
    // verify page content for default route
    const logo = screen.getByRole('img');
    expect(logo).toHaveAttribute('src', 'test-file-stub');
    // expect(logo).toHaveAttribute('src', 'metadataEditorLogo.png');
    expect(screen.getByText('MetaStore schemas')).toBeInTheDocument()
    expect(screen.getByText('Local documents')).toBeInTheDocument()
  });

  it('Pages navigation',async () => {
    render(<App />, {wrapper: BrowserRouter})
    const user = userEvent.setup()
  
    // Navigate from Home back and forth to the divverent pages:

    // '/' -> '/metaStoreSchemas'
    await user.click(screen.getByLabelText('MetaStore_schemas'))
    expect(screen.getByText(/Loading.../i)).toBeInTheDocument()
    expect(screen.getByText(/Home/i)).toBeInTheDocument()

    //  '/metaStoreSchemas' -> '/'
    await user.click(screen.getByText(/Home/i))
    expect(screen.getByText('MetaStore schemas')).toBeInTheDocument()
    expect(screen.getByText('Local documents')).toBeInTheDocument()

    // '/' -> '/local_documents'
    await user.click(screen.getByLabelText('local_documents'))
    expect(screen.getByText(/Home/i)).toBeInTheDocument()
    expect(screen.getByText('Load .jme file')).toBeInTheDocument()
  });

})
