const ONE_OF_KEY = 'oneOf'

export const oneOfData = {
  name: 'second_option',
  flag: true,
  inner_spec: {
    name: 'inner_spec',
    special_spec: {
      name: 'special_spec',
      cpg_params: 'blah',
    },
  },
};
export const oneOfSchema = {
    type: 'object',
    title: 'Testing OneOfs',
    $defs: {
      special_spec_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'special_spec',
            readOnly: true,
          },
          cpg_params: {
            type: 'string',
          },
        },
        required: ['name'],
      },
      inner_first_choice_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'inner_first_choice',
            readOnly: true,
          },
          params: {
            type: 'string',
          },
        },
        required: ['name', 'params'],
        additionalProperties: false,
      },
      inner_second_choice_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'inner_second_choice',
            readOnly: true,
          },
          enumeration: {
            type: 'string',
            enum: ['enum_1', 'enum_2', 'enum_3'],
          },
          params: {
            type: 'string',
            default: '',
          },
        },
        required: ['name', 'enumeration'],
        additionalProperties: false,
      },
      inner_spec_2_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'inner_spec_2',
            readOnly: true,
          },
          inner_one_of: {
            oneOf: [
              {
                $ref: '#/$defs/inner_first_choice_def',
                title: 'inner_first_choice',
              },
              {
                $ref: '#/$defs/inner_second_choice_def',
                title: 'inner_second_choice',
              },
            ],
          },
        },
        required: ['name', 'inner_one_of'],
      },
      first_option_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'first_option',
            readOnly: true,
          },
          flag: {
            type: 'boolean',
            default: false,
          },
          inner_spec: {
            $ref: '#/$defs/inner_spec_2_def',
          },
          unlabeled_options: {
            oneOf: [
              {
                type: 'integer',
              },
              {
                type: 'array',
                items: {
                  type: 'integer',
                },
              },
            ],
          },
        },
        required: ['name', 'inner_spec'],
        additionalProperties: false,
      },
      inner_spec_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'inner_spec',
            readOnly: true,
          },
          inner_one_of: {
            oneOf: [
              {
                $ref: '#/$defs/inner_first_choice_def',
                title: 'inner_first_choice',
              },
              {
                $ref: '#/$defs/inner_second_choice_def',
                title: 'inner_second_choice',
              },
            ],
          },
          special_spec: {
            $ref: '#/$defs/special_spec_def',
          },
        },
        required: ['name'],
      },
      second_option_def: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            default: 'second_option',
            readOnly: true,
          },
          flag: {
            type: 'boolean',
            default: false,
          },
          inner_spec: {
            $ref: '#/$defs/inner_spec_def',
          },
          unique_to_second: {
            type: 'integer',
          },
          labeled_options: {
            oneOf: [
              {
                type: 'string',
              },
              {
                type: 'array',
                items: {
                  type: 'string',
                },
              },
            ],
          },
        },
        required: ['name', 'inner_spec'],
        additionalProperties: false,
      },
    },
    oneOf: [
      {
        $ref: '#/$defs/first_option_def',
        title: 'first option',
      },
      {
        $ref: '#/$defs/second_option_def',
        title: 'second option',
      },
    ],
  };
export const ONE_OF_SCHEMA_OPTIONS = oneOfSchema[ONE_OF_KEY]!
export const FIRST_ONE_OF= ONE_OF_SCHEMA_OPTIONS[0];
export const SECOND_ONE_OF= ONE_OF_SCHEMA_OPTIONS[1];
export const OPTIONAL_ONE_OF_SCHEMA = {
  oneOf: [
    {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          default: 'first_option',
          readOnly: true,
        },
      },
      additionalProperties: false,
    },
    {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          default: 'second_option',
          readOnly: true,
        },
        flag: {
          type: 'boolean',
          default: false,
        },
      },
      additionalProperties: false,
    },
    {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          default: 'third_option',
          readOnly: true,
        },
        flag: {
          type: 'boolean',
          default: false,
        },
        inner_obj: {
          type: 'object',
          properties: {
            foo: {
              type: 'string',
            },
          },
        },
      },
      additionalProperties: false,
    },
  ],
};
export const OPTIONAL_ONE_OF_SCHEMA_ONEOF = OPTIONAL_ONE_OF_SCHEMA[ONE_OF_KEY];
export const OPTIONAL_ONE_OF_DATA = { flag: true, inner_obj: { foo: 'bar' } };
export const SIMPLE_ONE_OF_SCHEMA = {
  oneOf: [
    {}, // object with no type should take the type from its parent schema
    { type: 'string' },
    { type: 'array', items: { type: 'string' } },
  ],
};
export const FIRST_OPTION_ONE_OF_DATA = {
  flag: true,
  inner_spec: {
    name: 'inner_spec_2',
    special_spec: undefined,
  },
  name: 'first_option',
  unique_to_second: undefined,
};
export const ONE_OF_SCHEMA_DATA = { ...oneOfData, unique_to_second: 5 };
