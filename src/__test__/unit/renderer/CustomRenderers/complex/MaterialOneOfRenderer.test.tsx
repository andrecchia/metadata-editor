import { render, fireEvent, waitFor, screen, within } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { JsonForms, JsonFormsStateProvider, JsonFormsDispatch } from '@jsonforms/react';
import { materialRenderers } from '@jsonforms/material-renderers';
import MaterialOneOfRenderer, { materialOneOfControlTester } from 'renderer/components/CustomRenderers/complex/MaterialOneOfRenderer';
import { initCore, TestEmitter } from '__test__/test-utils/jsonformsUtils';
import { ControlElement, JsonSchema } from '@jsonforms/core';
import { materialRenderersWithCheck } from 'renderer/components/CustomRenderers/controlsWithCheck';
import MaterializedGroupLayoutRenderer, { materialGroupTester } from 'renderer/components/CustomRenderers/layouts/MaterialGroupLayout';
import MaterialListWithDetailRenderer, { materialListWithDetailTester } from 'renderer/components/CustomRenderers/ListWithDetails/ListWithDetailsRenderer';
import MaterialVerticalLayoutRenderer, { materialVerticalLayoutTester } from 'renderer/components/CustomRenderers/layouts/MaterialVerticalLayout';
import { HandleFieldsCheckProvider } from 'renderer/components/CustomRenderers/util/lockedUtils';
import '__test__/test-utils/matchMedia.mock'

const customRenderers = [
  ...materialRenderers,
    //register custom renderers
    ...materialRenderersWithCheck,
    { tester: materialListWithDetailTester, renderer: MaterialListWithDetailRenderer},
    { tester: materialGroupTester, renderer: MaterializedGroupLayoutRenderer},
    { tester: materialVerticalLayoutTester, renderer: MaterialVerticalLayoutRenderer},
    { tester: materialOneOfControlTester, renderer: MaterialOneOfRenderer},
]

const clickAddButton = (button, times: number) => {
  for (let i = 0; i < times; i++) {
    fireEvent.click(button);
  }
};

const selectOneOfDropdown = async (button:HTMLElement, toSelect: number, expectConfirm: boolean) => {
  // Open dropdown menu
  fireEvent.mouseDown(button)
  // These are the options in the dropdown
  const options = screen.getAllByRole('option');
  fireEvent.click(options[toSelect]);
  await waitFor(() => {
    if (expectConfirm) {

      const dialog = screen.getByRole('dialog')
      const confirmButton = within(dialog).getByRole('button', { name: 'Yes' });
      fireEvent.click(confirmButton);
    } else {
      const dialog = screen.queryByRole('dialog')
      expect(dialog).not.toBeInTheDocument()
    }
  });
};

describe('Material oneOf renderer', () => {
  it('should render and select first dropdown entry by default', () => {
    const schema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const core = initCore(schema, uischema, { data: undefined });
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <MaterialOneOfRenderer schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );
    const dropdownButton = screen.getByRole('button')
    expect(dropdownButton).toBeInTheDocument()
    expect(dropdownButton.innerHTML).toBe('String')
  });

  it('should render and select second dropdown entry due to datatype', () => {
    const schema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const data = {
      value: 5,
    };
    render(
      <HandleFieldsCheckProvider>
        <JsonForms data={data} schema={schema} uischema={uischema} renderers={customRenderers} />
      </HandleFieldsCheckProvider>
    );

    const dropdownButton = screen.getByRole('button')
    expect(dropdownButton).toBeInTheDocument()
    expect(dropdownButton.innerHTML).toBe('Number')
  });

  it('should render and select second dropdown entry due to schema with additionalProperties', () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'object',
              properties: {
                foo: { type: 'string' },
              },
              additionalProperties: false,
            },
            {
              title: 'Number',
              type: 'object',
              properties: {
                bar: { type: 'string' },
              },
              additionalProperties: false,
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const data = {
      value: { bar: 'bar' },
    };
    render(
      <HandleFieldsCheckProvider>
        <JsonForms data={data} schema={schema} uischema={uischema} renderers={customRenderers} />
      </HandleFieldsCheckProvider>
    );

    const dropdownButton = screen.getByRole('button')
    expect(dropdownButton).toBeInTheDocument()
    expect(dropdownButton.innerHTML).toBe('Number')
  });

  it('should render and select second dropdown entry due to schema with required', () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'object',
              properties: {
                foo: { type: 'string' },
              },
              required: ['foo'],
            },
            {
              title: 'Number',
              type: 'object',
              properties: {
                bar: { type: 'string' },
              },
              required: ['bar'],
            },
          ],
        },
      },
    };
    const uischema = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const data = {
      value: { bar: 'bar' },
    };
    render(
      <HandleFieldsCheckProvider>
        <JsonForms data={data} schema={schema} uischema={uischema} renderers={customRenderers} />
      </HandleFieldsCheckProvider>
    );

    const dropdownButton = screen.getByRole('button')
    expect(dropdownButton).toBeInTheDocument()
    expect(dropdownButton.innerHTML).toBe('Number')
  });

  it('should add an item at correct path', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
  
    let onChangeData;
    
    render(
      <HandleFieldsCheckProvider>
        <JsonForms
          data={undefined}
          schema={schema}
          uischema={uischema}
          renderers={customRenderers}
          onChange={({ data }) => {
            onChangeData = data;
          }}
        />
      </HandleFieldsCheckProvider>
    )
  
    // Wait for the component to render
    await screen.findByRole('textbox')
    const input = screen.getByRole('textbox');
  
    fireEvent.change(input, { target: { value: 'test' } })
  
    // Wait for the data change
    await waitFor(() => {
      expect(onChangeData).toEqual({ value: 'test' });
    });
  });

  it('should add an item within an array', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        thingOrThings: {
          oneOf: [
            {
              title: 'Thing',
              type: 'string'
            },
            {
              title: 'Things',
              type: 'array',
              items: {
                title: 'Thing',
                type: 'string'
              }
            },
          ],
        },
      }
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/thingOrThings',
    };

    const core = initCore(schema, uischema, { data: {} });

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <JsonFormsDispatch schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const dropdownButton = screen.getByRole('button')
    selectOneOfDropdown(dropdownButton,1 ,false); // Assuming false for expectConfirm

    const nrOfRowsBeforeAdd = screen.getAllByRole('row');

    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton = screen.getAllByRole('button')[1]
    clickAddButton(addButton, 2);
    
    const nrOfRowsAfterAdd = screen.getAllByRole('row');

    // Asserting the number of rows before and after adding an item
    expect(nrOfRowsBeforeAdd.length).toBe(2);
    expect(nrOfRowsAfterAdd.length).toBe(3);
  });

  it('should add an object within an array', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        thingOrThings: {
          oneOf: [
            {
              title: 'Thing',
              type: 'object',
              properties: {
                thing: {
                  title: 'Thing',
                  type: 'string'
                },
              },
            },
            {
              title: 'Things',
              type: 'array',
              items: {
                title: 'Thing',
                type: 'string'
              }
            },
          ],
        },
      }
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/thingOrThings',
    };

    const core = initCore(schema, uischema, {});
    const onChangeData = {
      data: undefined,
    };

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <TestEmitter
            onChange={({ data }) => {
              onChangeData.data = data;
            }}
          />
          <JsonFormsDispatch schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const dropdownButton = screen.getByRole('button')
    selectOneOfDropdown(dropdownButton, 1, false); // Assuming false for expectConfirm

    const nrOfRowsBeforeAdd = screen.getAllByRole('row');

    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton = screen.getAllByRole('button')[1]
    clickAddButton(addButton, 2)

    const nrOfRowsAfterAdd = screen.getAllByRole('row');

    expect(nrOfRowsBeforeAdd).toHaveLength(2);
    expect(nrOfRowsAfterAdd).toHaveLength(3);
    expect(onChangeData.data).toEqual({
      thingOrThings: ['', ''],
    });
  });

  it('should switch to array based oneOf subschema, then switch back, then edit', async () => {
    const schema = {
      type: 'object',
      properties: {
        thingOrThings: {
          oneOf: [
            {
              title: 'Thing',
              type: 'object',
              properties: {
                thing: {
                  title: 'Thing',
                  type: 'string'
                },
              },
            },
            {
              title: 'Things',
              type: 'array',
              items: {
                title: 'Thing',
                type: 'string'
              }
            },
          ],
        },
      }
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/thingOrThings',
    };

    const core = initCore(schema, uischema, {});
    const onChangeData = {
      data: undefined,
    };

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <TestEmitter
            onChange={({ data }) => {
              onChangeData.data = data;
            }}
          />
          <JsonFormsDispatch schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );


    // Select 2nd tab
    const dropdownButton = screen.getByRole('button')
    selectOneOfDropdown(dropdownButton, 1, false); // Assuming false for expectConfirm
    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton = screen.getAllByRole('button')[1]
    // Add something
    clickAddButton(addButton, 2)
    // Switch back to 1st tab, a confirm must appear, then click Yes
    selectOneOfDropdown(dropdownButton, 0, true);

    // Wait that dialog is closed
    await screen.findByRole('textbox')
    const input = screen.getByRole('textbox')

    fireEvent.change(input, { target: { value: 'test' } })

    await waitFor(() => {
      expect(onChangeData.data).toEqual({ thingOrThings: { thing: 'test' } });
    });
  });

  it('should show confirm dialog when data is not an empty object', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };

    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };

    // Insert initial data
    const data = {
      value: 'Foo Bar',
    };
    render(
      <HandleFieldsCheckProvider>
        <JsonForms data={data} schema={schema} uischema={uischema} renderers={customRenderers} />
      </HandleFieldsCheckProvider>
    );

    // Check that changing tab with initial data triggers dialog
    const dropdownButton = screen.getByRole('button')
    selectOneOfDropdown(dropdownButton, 1, true);

  });

  it('should be hideable', () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          oneOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const core = initCore(schema, uischema, { data: undefined });
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <MaterialOneOfRenderer
            schema={schema}
            uischema={uischema}
            visible={false}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );
    const inputs = screen.queryAllByRole('button');
    expect(inputs).toHaveLength(0);
  });
});
