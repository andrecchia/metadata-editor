import getClosestMatchingOption, { getMatchingOption, guessType } from "renderer/components/CustomRenderers/complex/matchingOptions";
import { calculateIndexScore } from "renderer/components/CustomRenderers/complex/matchingOptions";
import { 
    ONE_OF_SCHEMA_DATA, 
    ONE_OF_SCHEMA_OPTIONS, 
    OPTIONAL_ONE_OF_DATA, 
    OPTIONAL_ONE_OF_SCHEMA, 
    OPTIONAL_ONE_OF_SCHEMA_ONEOF, 
    oneOfData, 
    oneOfSchema } from "./testData";
import { JsonSchema } from "@jsonforms/core";
import get from "lodash/get";
import $RefParser from "@apidevtools/json-schema-ref-parser";

let resolvedOneOfSchema, firstOption, secondOption

// Adapted from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/test/schema/getFirstMatchingOptionTest.ts
describe('getMatchingOption()', () => {
    it('should infer correct anyOf schema based on data if passing undefined', () => {
        const options: JsonSchema[] = [
            { type: 'object', properties: { id: { enum: ['a'] } } },
            {
            type: 'object',
            properties: {
                id: { enum: ['nested'] },
                child: { anyOf: [
                    { type: 'object', properties: { id: { enum: ['a'] } } }, 
                    { type: 'object',
                        properties: {
                        id: { enum: ['nested'] }
                        }
                    }
                ]  },
            },
            },
        ];
        expect(getMatchingOption( undefined, options)).toEqual(0);
    });
    it('should infer correct anyOf schema with properties also having anyOf/allOf', () => {
        const options: JsonSchema[] = [
            {
            type: 'object',
            properties: { id: { enum: ['a'] } },
            anyOf: [{ type: 'string' }, { type: 'boolean' }],
            },
            {
            type: 'object',
            properties: {
                id: { enum: ['nested'] },
                child: { anyOf: [
                    { type: 'object', properties: { id: { enum: ['a'] } } }, 
                    { type: 'object',
                        properties: {
                        id: { enum: ['nested'] }
                        }
                    }
                ]  },
            },
            anyOf: [{ type: 'number' }, { type: 'boolean' }],
            allOf: [{ type: 'string' }],
            },
        ];
        expect(getMatchingOption( null, options)).toEqual(0);
    });
    it('returns 0 if no options match', () => {
        const options: JsonSchema[] = [{ type: 'string' }, { type: 'string' }, { type: 'null' }];
        expect(getMatchingOption( undefined, options)).toEqual(0);
    });
    it('should infer correct anyOf schema based on data if passing null and option 2 is {type: null}', () => {
        const options: JsonSchema[] = [{ type: 'string' }, { type: 'string' }, { type: 'null' }];
        expect(getMatchingOption( null, options)).toEqual(2);
    });
    it('should infer correct anyOf schema based on data', () => {
        const options: JsonSchema[] = [
            { type: 'object', properties: { id: { enum: ['a'] } } },
            {
            type: 'object',
            properties: {
                id: { enum: ['nested'] },
                child: {
                    anyOf: [
                        { type: 'object', properties: { id: { enum: ['a'] } } }, 
                        { type: 'object',
                            properties: {
                            id: { enum: ['nested'] }
                            }
                        }
                    ]  
                },
            },
            },
        ];
        const formData = {
            id: 'nested',
            child: {
                id: 'nested',
                child: {
                    id: 'a',
                },
            },
        };
        expect(getMatchingOption(formData, options)).toEqual(1);
        expect(getMatchingOption(formData, options)).toEqual(1);
    });
});

// Adapted from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/test/schema/getClosestMatchingOptionTest.ts
describe('calculateIndexScore', () => {
    beforeAll(async ()=>{
        resolvedOneOfSchema = await  $RefParser.dereference(oneOfSchema)
        firstOption = (resolvedOneOfSchema as any).$defs!.first_option_def
        secondOption = (resolvedOneOfSchema as any).$defs!.second_option_def
    })
    afterAll(()=>{
        resolvedOneOfSchema=undefined
        firstOption=undefined
        secondOption=undefined
    })
    it('returns 0 when schema is not specified', () => {
        expect(calculateIndexScore(OPTIONAL_ONE_OF_SCHEMA as JsonSchema)).toEqual(0);
    });
    it('returns 0 when schema.properties is undefined', () => {
        expect(calculateIndexScore( OPTIONAL_ONE_OF_SCHEMA as JsonSchema, {})).toEqual(0);
    });
    it('returns 0 when schema.properties is not an object', () => {
        expect(
            calculateIndexScore(OPTIONAL_ONE_OF_SCHEMA as JsonSchema, {
            properties: 'foo',
            } as unknown as JsonSchema)
        ).toEqual(0);
    });
    it('returns 0 when properties type is boolean', () => {
        expect(
            calculateIndexScore(OPTIONAL_ONE_OF_SCHEMA as JsonSchema, {
            properties: { foo: true },
            } as unknown as JsonSchema)
        ).toEqual(0);
    });
    it('returns 0 when formData is empty object', () => {
        expect(calculateIndexScore( resolvedOneOfSchema, firstOption, {})).toEqual(0);
    });
    it('returns 1 for first option in oneOf schema', () => {
        expect(calculateIndexScore( resolvedOneOfSchema, firstOption, ONE_OF_SCHEMA_DATA)).toEqual(1);
    });
    it('returns 8 for second option in oneOf schema', () => {
        expect(calculateIndexScore( resolvedOneOfSchema, secondOption, ONE_OF_SCHEMA_DATA)).toEqual(8);
    });
    it('returns 1 for a schema that has a type matching the formData type', () => {
        expect(calculateIndexScore( resolvedOneOfSchema, { type: 'boolean' }, true)).toEqual(1);
    });
    it('returns 2 for a schema that has a const matching the formData value', () => {
        expect(
            calculateIndexScore(
            resolvedOneOfSchema,
            { properties: { foo: { type: 'string', const: 'constValue' } } },
            { foo: 'constValue' }
            )
        ).toEqual(2);
    });
    it('returns 0 for a schema that has a const that does not match the formData value', () => {
        expect(
            calculateIndexScore(
            
            resolvedOneOfSchema,
            { properties: { foo: { type: 'string', const: 'constValue' } } },
            { foo: 'aValue' }
            )
        ).toEqual(0);
    });
});
describe('getClosestMatchingOption', () => {
    it('oneOfSchema, oneOfData data, no options, returns undefined', () => {
        expect(getClosestMatchingOption(resolvedOneOfSchema, oneOfData, [])).toEqual(undefined);
    });
    it('oneOfSchema, no data, 2 options, returns undefined', () => {
        expect(getClosestMatchingOption(resolvedOneOfSchema, undefined, [{ type: 'string' }, { type: 'number' }])).toEqual(undefined);
    });
    it('oneOfSchema, oneOfData, no options, selectedOption 2, returns 2', () => {
        expect(getClosestMatchingOption(resolvedOneOfSchema, oneOfData, [], 2)).toEqual(2);
    });
    it('oneOfSchema, no data, 2 options, returns -1', () => {
        expect(getClosestMatchingOption(resolvedOneOfSchema, undefined, [{ type: 'string' }, { type: 'number' }], 2)).toEqual(2);
    });
    it('returns the first option, which kind of matches the data', () => {
        expect(getClosestMatchingOption( resolvedOneOfSchema, { flag: true }, ONE_OF_SCHEMA_OPTIONS)).toEqual(0);
    });
    it('returns the second option, which exactly matches the data', () => {
        expect(getClosestMatchingOption(resolvedOneOfSchema, ONE_OF_SCHEMA_DATA, ONE_OF_SCHEMA_OPTIONS)).toEqual(
            1
        );
    });
    it('returns the first matching option (i.e. second index) when data is ambiguous', () => {
        const formData = { flag: false };
        expect(
            getClosestMatchingOption( 
                OPTIONAL_ONE_OF_SCHEMA as JsonSchema, 
                formData, 
                OPTIONAL_ONE_OF_SCHEMA_ONEOF as JsonSchema[])
        ).toEqual(1);
        });
    it('returns the third index when data is clear', () => {
        expect(
            getClosestMatchingOption(
            OPTIONAL_ONE_OF_SCHEMA as JsonSchema,
            OPTIONAL_ONE_OF_DATA,
            OPTIONAL_ONE_OF_SCHEMA_ONEOF as JsonSchema[]
            )
        ).toEqual(2);
    });
    it('returns the second option when data matches for oneOf', () => {
        // From https://github.com/rjsf-team/react-jsonschema-form/issues/2944
        const schema: JsonSchema= {
            type: 'array',
            items: {
            oneOf: [
                {
                properties: {
                    lorem: {
                    type: 'string',
                    },
                },
                required: ['lorem'],
                },
                {
                properties: {
                    ipsum: {
                    oneOf: [
                        {
                        properties: {
                            day: {
                            type: 'string',
                            },
                        },
                        },
                        {
                        properties: {
                            night: {
                            type: 'string',
                            },
                        },
                        },
                    ],
                    },
                },
                required: ['ipsum'],
                },
            ],
            },
        };
        const formData = { ipsum: { night: 'nicht' } };
        expect(getClosestMatchingOption( schema, formData, get(schema, 'items.oneOf') as unknown as JsonSchema[])).toEqual(1);
    });
    it('returns the second option when data matches for anyOf', () => {
        const schema: JsonSchema = {
            type: 'array',
            items: {
            anyOf: [
                {
                properties: {
                    lorem: {
                    type: 'string',
                    },
                },
                required: ['lorem'],
                },
                {
                properties: {
                    ipsum: {
                    anyOf: [
                        {
                        properties: {
                            day: {
                            type: 'string',
                            },
                        },
                        },
                        {
                        properties: {
                            night: {
                            type: 'string',
                            },
                        },
                        },
                    ],
                    },
                },
                required: ['ipsum'],
                },
            ],
            },
        };
        const formData = { ipsum: { night: 'nicht' } };
        expect(getClosestMatchingOption( schema, formData, get(schema, 'items.anyOf') as unknown as JsonSchema[])).toEqual(1);
    });
});

// Taken from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/test/guessType.test.ts
describe('guessType()', () => {
    it('should guess the type of array values', () => {
      expect(guessType([1, 2, 3])).toEqual('array');
    });
  
    it('should guess the type of string values', () => {
      expect(guessType('foobar')).toEqual('string');
    });
  
    it('should guess the type of null values', () => {
      expect(guessType(null)).toEqual('null');
    });
  
    it('should treat undefined values as null values', () => {
      expect(guessType(undefined)).toEqual('null');
    });
  
    it('should guess the type of boolean values', () => {
      expect(guessType(true)).toEqual('boolean');
    });
  
    it('should guess the type of object values', () => {
      expect(guessType({})).toEqual('object');
    });
  
    it('falls through to string', () => {
      expect(guessType(NaN)).toEqual('string');
    });
  });