import { render, fireEvent, waitFor, screen, within } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { JsonForms, JsonFormsStateProvider, JsonFormsDispatch } from '@jsonforms/react';
import { materialRenderers } from '@jsonforms/material-renderers';
import { initCore, TestEmitter } from '__test__/test-utils/jsonformsUtils';
import { ControlElement, JsonSchema } from '@jsonforms/core';
import { materialRenderersWithCheck } from 'renderer/components/CustomRenderers/controlsWithCheck';
import MaterializedGroupLayoutRenderer, { materialGroupTester } from 'renderer/components/CustomRenderers/layouts/MaterialGroupLayout';
import MaterialListWithDetailRenderer, { materialListWithDetailTester } from 'renderer/components/CustomRenderers/ListWithDetails/ListWithDetailsRenderer';
import MaterialVerticalLayoutRenderer, { materialVerticalLayoutTester } from 'renderer/components/CustomRenderers/layouts/MaterialVerticalLayout';
import { HandleFieldsCheckProvider } from 'renderer/components/CustomRenderers/util/lockedUtils';
import '__test__/test-utils/matchMedia.mock'
import MaterialAnyOfRenderer, { materialAnyOfControlTester } from 'renderer/components/CustomRenderers/complex/MaterialAnyOfRenderer';

const customRenderers = [
  ...materialRenderers,
    //register custom renderers
    ...materialRenderersWithCheck,
    { tester: materialListWithDetailTester, renderer: MaterialListWithDetailRenderer},
    { tester: materialGroupTester, renderer: MaterializedGroupLayoutRenderer},
    { tester: materialVerticalLayoutTester, renderer: MaterialVerticalLayoutRenderer},
    { tester: materialAnyOfControlTester, renderer: MaterialAnyOfRenderer},
]

const clickAddButton = (button, times: number) => {
  for (let i = 0; i < times; i++) {
    fireEvent.click(button);
  }
};

const selectAnyOfDropdown = async (button:HTMLElement, toSelect: number) => {
  // Open dropdown menu
  fireEvent.mouseDown(button)
  // These are the options in the dropdown
  const options = screen.getAllByRole('option');
  fireEvent.click(options[toSelect]);
};

describe('Material anyOf renderer', () => {

  it('should add an item at correct path', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          anyOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    let onChangeData;
    render(
      <HandleFieldsCheckProvider>
        <JsonForms
          data={undefined}
          schema={schema}
          uischema={uischema}
          renderers={customRenderers}
          onChange={({ data }) => {
            onChangeData = data;
          }}
        />
      </HandleFieldsCheckProvider>
    );

    // Wait for the component to render
    await screen.findByRole('textbox')
    const input = screen.getByRole('textbox');
  
    fireEvent.change(input, { target: { value: 'test' } })
  
    // Wait for the data change
    await waitFor(() => {
      expect(onChangeData).toEqual({ value: 'test' });
    });

  });

  it('should add a "mything"', async () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        myThingsAndOrYourThings: {
          anyOf: [
            {
              $ref: '#/definitions/myThings',
            },
            {
              $ref: '#/definitions/yourThings',
            },
          ],
        },
      },
      definitions: {
        myThings: {
          title: 'MyThing',
          type: 'array',
          items: {
            type: 'object',
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
        yourThings: {
          title: 'YourThings',
          type: 'array',
          items: {
            type: 'object',
            properties: {
              age: {
                type: 'number',
              },
            },
          },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/myThingsAndOrYourThings',
    };
    const core = initCore(schema, uischema);
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }} >
          <MaterialAnyOfRenderer schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    expect(screen.queryAllByRole('listitem')).toHaveLength(0)

    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton = screen.getAllByRole('button')[1]
    clickAddButton(addButton, 2);
    

    const nrOfRowsAfterAdd = screen.getAllByRole('listitem');

    expect(nrOfRowsAfterAdd.length).toBe(2);
  });

  it('should switch to "yourThing" edit, then switch back, then edit', async () => {
    const schema = {
      type: 'object',
      properties: {
        myThingsAndOrYourThings: {
          anyOf: [
            {
              $ref: '#/definitions/myThings',
            },
            {
              $ref: '#/definitions/yourThings',
            },
          ],
        },
      },
      definitions: {
        myThings: {
          title: 'MyThing',
          type: 'array',
          items: {
            type: 'object',
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        },
        yourThings: {
          title: 'YourThings',
          type: 'array',
          items: {
            type: 'object',
            properties: {
              age: {
                type: 'number',
              },
            },
          },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/myThingsAndOrYourThings',
    };
    const onChangeData = {
      data: undefined,
    };
    const core = initCore(schema, uischema);
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider initState={{ renderers: customRenderers, core }}>
          <TestEmitter
            onChange={({ data }) => {
              onChangeData.data = data;
            }}
          />
          <MaterialAnyOfRenderer schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );


    // Select 2nd entry
    const dropdownButton = screen.getAllByRole('button')[0]
    selectAnyOfDropdown(dropdownButton, 1); 

    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton2nd = screen.getAllByRole('button')[1]
    // Add something
    clickAddButton(addButton2nd, 1)

    // Change value on 2nd entry
    const input2nd = screen.getByRole('spinbutton')
    fireEvent.change(input2nd, { target: { value: 5 } })

    // Switch back to 1st entry
    selectAnyOfDropdown(dropdownButton, 0);

    // Select the 'add to array' button 
    // (it's the 2nd in the screen because the 1st is the dropdown button)
    const addButton1st = screen.getAllByRole('button')[1]
    // Add something
    clickAddButton(addButton1st, 1)
    // Change value on 1st entry
    await screen.findByRole('textbox')
    const input1st = screen.getByRole('textbox')
    fireEvent.change(input1st, { target: { value: 'test' } })

    await waitFor(() => {
      expect(onChangeData.data).toEqual({"myThingsAndOrYourThings": [{"age": 5, "name": "test"}, {}]});
    })
  });

  it('should be hideable', () => {
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        value: {
          anyOf: [
            {
              title: 'String',
              type: 'string',
            },
            {
              title: 'Number',
              type: 'number',
            },
          ],
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      label: 'Value',
      scope: '#/properties/value',
    };
    const core = initCore(schema, uischema);
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, core }}
        >
          <MaterialAnyOfRenderer
            schema={schema}
            uischema={uischema}
            visible={false}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );
    const inputs = screen.queryAllByRole('button');
    expect(inputs).toHaveLength(0);
  });

});
