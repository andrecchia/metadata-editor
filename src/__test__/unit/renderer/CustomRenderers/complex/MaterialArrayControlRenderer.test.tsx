import { render, fireEvent, waitFor, screen, within } from '@testing-library/react';
import {
  ControlElement,
  DispatchCellProps,
  JsonSchema,
} from '@jsonforms/core';
import * as React from 'react';

import MaterialArrayControlRenderer, { materialArrayControlTester } from 'renderer/components/CustomRenderers/complex/MaterialArrayControlRenderer';
import { materialCells, materialRenderers } from '@jsonforms/material-renderers';
import { JsonFormsStateProvider, StatelessRenderer } from '@jsonforms/react';
import { initCore, TestEmitter } from '__test__/test-utils/jsonformsUtils';
import '__test__/test-utils/matchMedia.mock'
import { HandleFieldsCheckProvider, handleFieldsCheck } from 'renderer/components/CustomRenderers/util/lockedUtils';
import { materialRenderersWithCheck } from 'renderer/components/CustomRenderers/controlsWithCheck';
import { MaterialListWithDetailRenderer, materialListWithDetailTester } from 'renderer/components/CustomRenderers/ListWithDetails/ListWithDetailsRenderer';
import { MaterializedGroupLayoutRenderer, materialGroupTester } from 'renderer/components/CustomRenderers/layouts/MaterialGroupLayout';
import { MaterialVerticalLayoutRenderer, materialVerticalLayoutTester } from 'renderer/components/CustomRenderers/layouts/MaterialVerticalLayout';

const customRenderers = [
    ...materialRenderers,
      //register custom renderers
      ...materialRenderersWithCheck,
      { tester: materialListWithDetailTester, renderer: MaterialListWithDetailRenderer},
      { tester: materialGroupTester, renderer: MaterializedGroupLayoutRenderer},
      { tester: materialVerticalLayoutTester, renderer: MaterialVerticalLayoutRenderer},
      { tester: materialArrayControlTester, renderer: MaterialArrayControlRenderer}
  ]

const fixture: {
  data: any;
  schema: JsonSchema;
  uischema: ControlElement;
} = {
  data: [
    {
      message: 'El Barto was here',
      done: true,
    },
    {
      message: 'Yolo',
    },
  ],
  schema: {
    type: 'array',
    items: {
      type: 'object',
      properties: {
        message: {
          type: 'string',
          maxLength: 3,
        },
        done: {
          type: 'boolean',
        },
      },
    },
  },
  uischema: {
    type: 'Control',
    scope: '#',
  },
};

const fixture2: {
  data: any;
  schema: JsonSchema;
  uischema: ControlElement;
} = {
  data: { test: ['foo', 'baz', 'bar'] },
  schema: {
    type: 'object',
    properties: {
      test: {
        type: 'array',
        items: { type: 'string' },
      },
    },
  },
  uischema: {
    type: 'Control',
    scope: '#/properties/test',
    options: {
      showSortButtons: true,
    },
  },
};

describe('Material array control', () => {

  it('should render', () => {
    const core = initCore(fixture.schema, fixture.uischema, fixture.data);
    render(
    <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
            initState={{ renderers: customRenderers, core }}
        >
            <MaterialArrayControlRenderer
            schema={fixture.schema}
            uischema={fixture.uischema}
            />
        </JsonFormsStateProvider>
    </HandleFieldsCheckProvider>
            
    );

    const rows = screen.getAllByRole('row');
    // 2 header rows + 2 data entries
    expect(rows.length).toBe(4);
  });

  it('should render empty', () => {
    const core = initCore(fixture.schema, fixture.uischema);
    render(
    <HandleFieldsCheckProvider>        
      <JsonFormsStateProvider
        initState={{ renderers: customRenderers, core }}
      >
        <MaterialArrayControlRenderer
          schema={fixture.schema}
          uischema={fixture.uischema}
        />
      </JsonFormsStateProvider>
    </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByRole('row');
    // two header rows + no data row
    expect(rows.length).toBe(3);
    const headerColumns = rows[1].children;
    // 3 columns: message & done properties + column for delete button
    expect(headerColumns).toHaveLength(3);
  });

  it('should render even without properties', () => {
    // re-init
    const data: any = { test: [] };
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        test: {
          type: 'array',
          items: { type: 'object' },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/test',
    };
    const core = initCore(schema, uischema, data);

    render(
    <HandleFieldsCheckProvider>
      <JsonFormsStateProvider
        initState={{ renderers: customRenderers, core }}
      >
        <MaterialArrayControlRenderer schema={schema} uischema={uischema} />
      </JsonFormsStateProvider>
    </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByRole('row');
    // two header rows + no data row
    expect(rows.length).toBe(3);
    const headerColumns = rows[1].children;
    // 2 columns: content + buttons
    expect(headerColumns).toHaveLength(2);
  });

  it('should use title as a header if it exists', () => {
    // re-init
    const data: any = { test: [] };
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        test: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              test1: {
                type: 'string',
                title: 'first test',
              },
              test_2: {
                type: 'string',
              },
            },
          },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/test',
    };
    const core = initCore(schema, uischema, data);

    render(
    <HandleFieldsCheckProvider>   
      <JsonFormsStateProvider
        initState={{ renderers: customRenderers, core }}
      >
        <MaterialArrayControlRenderer schema={schema} uischema={uischema} />
      </JsonFormsStateProvider>
    </HandleFieldsCheckProvider>
    );

    // column headings are in the second row of the table, wrapped in <th>
    const rows = screen.getAllByRole('row');
    // Access the second row (indexing starts from 0)
    const secondRow = rows[1];
    // Find header cells within the second row
    const headers = secondRow.querySelectorAll('th');

    // the first property has a title, so we expect it to be rendered as the first column heading
    expect(headers[0].textContent).toEqual('first test');

    // the second property has no title, so we expect to see the property name in start case
    expect(headers[1].textContent).toEqual('Test 2');
  });

  it('should render empty primitives', () => {
    // re-init
    const data: any = { test: [] };
    const schema: JsonSchema = {
      type: 'object',
      properties: {
        test: {
          type: 'array',
          items: { type: 'string' },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/test',
    };
    const core = initCore(schema, uischema, data);
    render(
        <HandleFieldsCheckProvider>   
          <JsonFormsStateProvider
            initState={{ renderers: customRenderers, core }}
          >
            <MaterialArrayControlRenderer schema={schema} uischema={uischema} />
          </JsonFormsStateProvider>
        </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByRole('row');
    // header + no data row
    expect(rows).toHaveLength(2);
  
    const emptyDataCol = rows[1].querySelectorAll('td')
    expect(emptyDataCol).toHaveLength(1);
    // selection column + 1 data column
    expect(emptyDataCol[0].getAttribute('colSpan')).toBe("2");
  });

  it('should render primitives', () => {
    // re-init
    const data = { test: ['foo', 'bar'] };
    const schema = {
      type: 'object',
      properties: {
        test: {
          type: 'array',
          items: { type: 'string' },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/test',
    };
    const core = initCore(schema, uischema, data);
    render(
        <HandleFieldsCheckProvider>   
          <JsonFormsStateProvider
            initState={{ renderers: customRenderers, core }}
          >
            <MaterialArrayControlRenderer schema={schema} uischema={uischema} />
          </JsonFormsStateProvider>
        </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByRole('row');
    // header + 2 data entries
    expect(rows).toHaveLength(3);
  });


  it('should delete an item', async () => {
    const core = initCore(fixture.schema, fixture.uischema, fixture.data);
    const onChangeData: any = {
      data: undefined,
    };
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, cells: materialCells, core }}
        >
          <TestEmitter
            onChange={({ data }) => {
              onChangeData.data = data;
            }}
          />
          <MaterialArrayControlRenderer
            schema={fixture.schema}
            uischema={fixture.uischema}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    // There will be 3 buttons:
    // - add to array
    // - 2 delete buttons, one for each entry
    const buttons = screen.getAllByRole('button');

    const nrOfRowsBeforeDelete = screen.getAllByRole('row').length;

    // delete 1st entry
    const deleteButton = buttons[1];
    fireEvent.click(deleteButton)

    // Confirm deletion
    await waitFor(() => {
        const dialog = screen.getByRole('dialog')
        const confirmButton = within(dialog).getByRole('button', { name: 'Yes' });
        fireEvent.click(confirmButton);
    });

    // Await for the element to render
    await screen.findAllByRole('row')
    const nrOfRowsAfterDelete = screen.getAllByRole('row').length;

    expect(nrOfRowsBeforeDelete).toBe(4);
    expect(nrOfRowsAfterDelete).toBe(3);
    expect(onChangeData.data.length).toBe(1);
  });

  const CellRenderer1: StatelessRenderer<DispatchCellProps> = () => (
    <div data-testid='cell test 1' />
  );
  const CellRenderer2: StatelessRenderer<DispatchCellProps> = () => (
    <div data-testid='cell test 2' />
  );

  it('should use cells from store', () => {
    const core = initCore(fixture.schema, fixture.uischema, fixture.data);
    const cells = [
      {
        tester: () => 50,
        cell: CellRenderer1,
      },
      {
        tester: () => 51,
        cell: CellRenderer2,
      },
    ];

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, core }}
        >
          <MaterialArrayControlRenderer
            schema={fixture.schema}
            uischema={fixture.uischema}
            cells={cells}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByTestId('cell test 2' );
    // 2 header rows + 2 data entries
    expect(rows.length).toBe(4);
  });

  it('should use cells from own props', () => {
    const core = initCore(fixture.schema, fixture.uischema, fixture.data);
    const cell = {
      tester: () => 50,
      cell: CellRenderer1,
    };

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: materialRenderers, core }}
        >
          <MaterialArrayControlRenderer
            schema={fixture.schema}
            uischema={fixture.uischema}
            cells={[cell, { tester: () => 60, cell: CellRenderer2 }]}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const rows = screen.getAllByTestId('cell test 2' );
    // 2 header rows + 2 data entries
    expect(rows.length).toBe(4);
  });

  it('should support adding rows that contain enums', async () => {
    const schema = {
      type: 'object',
      properties: {
        things: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              somethingElse: {
                type: 'string',
              },
              thing: {
                type: 'string',
                enum: ['thing'],
              },
            },
          },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/things',
    };
    const core = initCore(schema, uischema, {});
    const onChangeData: any = {
      data: undefined,
    };

    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, core }}
        >
          <TestEmitter
            onChange={({ data }) => {
              onChangeData.data = data;
            }}
          />
          <MaterialArrayControlRenderer schema={schema} uischema={uischema} />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const addButton = screen.getByRole('button');

    const nrOfRowsBeforeAdd = screen.getAllByRole('row').length;

    fireEvent.click(addButton);
    fireEvent.click(addButton);
    
    // Await for the element to render
    await screen.findAllByRole('row')
    const nrOfRowsAfterAdd = screen.getAllByRole('row').length;

    // 2 header rows + 'no data' row
    expect(nrOfRowsBeforeAdd).toBe(3);
    expect(nrOfRowsAfterAdd).toBe(4);
    expect(onChangeData.data).toEqual({ things: [{}, {}] });
  });

  it('should be hideable', () => {
    const schema = {
      type: 'object',
      properties: {
        things: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              somethingElse: {
                type: 'string',
              },
              thing: {
                type: 'string',
                enum: ['thing'],
              },
            },
          },
        },
      },
    };
    const uischema: ControlElement = {
      type: 'Control',
      scope: '#/properties/things',
    };
    const core = initCore(schema, uischema, {});
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, core }}
        >
          <MaterialArrayControlRenderer
            schema={schema}
            uischema={uischema}
            visible={false}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );

    const nrOfButtons = screen.queryAllByRole('button').length;
    expect(nrOfButtons).toBe(0);

    const nrOfRows = screen.queryAllByRole('row').length;
    expect(nrOfRows).toBe(0);
  });

  it('should have fields enabled', () => {
    const core = initCore(fixture2.schema, fixture2.uischema, fixture2.data);
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, cells: materialCells, core }}
        >
          <MaterialArrayControlRenderer
            schema={fixture2.schema}
            uischema={fixture2.uischema}
            enabled={true}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );
    // first row is header in table
    const input = screen.getAllByRole('row')[1].querySelector('input');
    expect(input).not.toHaveAttribute('disabled');
  });

  it('should have fields disabled', () => {
    const core = initCore(fixture2.schema, fixture2.uischema, fixture2.data);
    render(
      <HandleFieldsCheckProvider>
        <JsonFormsStateProvider
          initState={{ renderers: customRenderers, cells: materialCells, core }}
        >
          <MaterialArrayControlRenderer
            schema={fixture2.schema}
            uischema={fixture2.uischema}
            enabled={false}
          />
        </JsonFormsStateProvider>
      </HandleFieldsCheckProvider>
    );
    // first row is header in table
    const input = screen.getAllByRole('row')[1].querySelector('input');
    expect(input).toHaveAttribute('disabled');
  });
  it('test checkbox mask', async ()=>{
    const core = initCore(fixture.schema,fixture.uischema, fixture.data)

    const mockUseState = (initValue) => {
      let value = initValue
      const setState = (newValue) => {
        value = newValue
      }
      return [value, setState]
    }
    const useInitState = () => mockUseState({checkFields: true, hideUncheckedFields: true})

    render(
      <handleFieldsCheck.Provider value={useInitState() as [{checkFields: boolean;hideUncheckedFields: boolean;}, React.Dispatch<React.SetStateAction<{checkFields: boolean;hideUncheckedFields: boolean;}>>]}>
        <JsonFormsStateProvider
            initState={{ renderers: customRenderers, core }}
          >
          <MaterialArrayControlRenderer schema={fixture.schema} uischema={fixture.uischema} />
        </JsonFormsStateProvider>
      </handleFieldsCheck.Provider>
    )

    // Click checkbox
    const checkBoxMask = screen.getAllByRole('checkbox')[0]
    fireEvent.click(checkBoxMask)
    expect(checkBoxMask).toBeChecked()

    fireEvent.click(checkBoxMask)
    expect(checkBoxMask).not.toBeChecked()
  });
});