import { render, screen } from "@testing-library/react";
import { MaterialEnumControlWithCheck, materialEnumControlWithCheckTester 
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps, OwnPropsOfEnum, Translator } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'
import { WithOptionLabel } from '@jsonforms/material-renderers/lib/mui-controls/MuiAutocomplete';
import { TranslateProps } from "@jsonforms/react";
import { theme } from "renderer/utils/theme";
import { ThemeProvider } from '@mui/material/styles';

const mockProps: ControlProps & OwnPropsOfEnum & WithOptionLabel & TranslateProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "string", enum:["one", "two"]},
    uischema: {type: 'Control', scope: '#/properties/some_field'},
    visible: true,
    options:[{label:"one",value:"one"},{label:"two",value:"two"}],
    locale: "en",
    t: (id: string, defaultMessage="default") => defaultMessage  
}

describe('MaterialEnumControlWithCheck', () => {
    it('MaterialEnumControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <ThemeProvider theme={theme}>
                <MaterialEnumControlWithCheck {...mockProps}/>
            </ThemeProvider>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryByRole('combobox');
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id)
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialEnumControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(102)
    });
})
