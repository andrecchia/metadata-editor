import { render, screen } from "@testing-library/react";
import { MaterialSliderControlWithCheck , materialSliderControlWithCheckTester
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'

const mockProps: ControlProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: number) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "number", minimum: 1, maximum: 5, default: 2},
    uischema: {type: 'Control', scope: '#/properties/some_field', options: { slider: true }},
    visible: true
}
describe('MaterialSliderControlWithCheck', () => {
    it('MaterialSliderControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <MaterialSliderControlWithCheck {...mockProps}/>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryByRole('slider');
        expect(renderedField).toBeInTheDocument()
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialSliderControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialSliderControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(104)
    });
})
