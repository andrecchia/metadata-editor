import { render, screen } from "@testing-library/react";
import { MaterialIntegerControlWithCheck, materialIntegerControlWithCheckTester 
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'
import { theme } from "renderer/utils/theme";
import { ThemeProvider } from "@mui/material/styles";

const mockProps: ControlProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "integer"},
    uischema: {type: 'Control', scope: '#/properties/some_field'},
    visible: true
}
describe('MaterialIntegerControlWithCheck', () => {
    it('MaterialIntegerControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <ThemeProvider theme={theme}>
                <MaterialIntegerControlWithCheck {...mockProps}/>
            </ThemeProvider>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryByRole('spinbutton');
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id+'-input')
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialIntegerControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(102)
    });
})
