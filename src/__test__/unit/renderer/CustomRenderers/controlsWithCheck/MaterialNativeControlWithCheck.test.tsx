import { render, screen } from "@testing-library/react";
import { MaterialNativeControlWithCheck , materialNativeControlWithCheckTester
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'

let mockProps: ControlProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "string", format: "date"},
    uischema: {type: 'Control', scope: '#/properties/some_field'},
    visible: true
}
describe('MaterialNativeControlWithCheck', () => {
    it('MaterialNativeControlWithCheck renderer (date)',()=>{
        render(
        <HandleFieldsCheckProvider>
            <MaterialNativeControlWithCheck {...mockProps}/>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryAllByRole('textbox')[0];
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id+'-input')
        expect(screen.getAllByText('Some Field')[0]).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialNativeControlWithCheck renderer (time)',()=>{
        mockProps.schema.format = "time";
        render(
        <HandleFieldsCheckProvider>
            <MaterialNativeControlWithCheck {...mockProps}/>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryAllByRole('textbox')[0];
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id+'-input')
        expect(screen.getAllByText('Some Field')[0]).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialNativeControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(102)
    });
})
