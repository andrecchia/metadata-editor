import { render, screen } from "@testing-library/react";
import { MaterialOneOfEnumControlWithCheck , materialOneOfEnumControlWithCheckTester
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps, OwnPropsOfEnum } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'
import { WithOptionLabel } from '@jsonforms/material-renderers/lib/mui-controls/MuiAutocomplete';
import { TranslateProps } from "@jsonforms/react";
import { theme } from "renderer/utils/theme";
import { ThemeProvider } from "@mui/material/styles";

const mockProps: ControlProps & OwnPropsOfEnum & WithOptionLabel & TranslateProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema:{ 
        oneOf: [
        { const: "one", title: "one" },
        { const: "two", title: "two" }
      ]
    },
    uischema: {type: 'Control', scope: '#/properties/some_field'},
    visible: true,
    options:[{label:"one",value:"one"},{label:"two",value:"two"}],
    locale: "en",
    t: (id: string, defaultMessage="default") => defaultMessage  
}

describe('MaterialOneOfEnumControlWithCheck', () => {
    it('MaterialOneOfEnumControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <ThemeProvider theme={theme}>
                <MaterialOneOfEnumControlWithCheck {...mockProps}/>
            </ThemeProvider>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryByRole('combobox');
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id)
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialOneOfEnumControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(105)
    });
})
