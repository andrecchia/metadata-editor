import { render, screen } from "@testing-library/react";
import { MaterialAnyOfStringOrEnumControlWithCheck, materialAnyOfStringOrEnumControlWithCheckTester 
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps, TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'

const mockProps: ControlProps = {
    data: undefined,
    uischema:{type:"Control",scope:"#/properties/firstName"},
    schema:{ anyOf:[{type:"string",enum:["one","two","three"]},{type:"string"}] },
    path:"firstName",
    handleChange: (path: string, value: any) => {},
    enabled:true,
    id:"#/properties/firstName2",
    errors:"",
    label:"First Name",
    visible:true,
    required:false,
    config:{restrict:false,trim:false,showUnfocusedDescription:false,hideRequiredAsterisk:false},
    rootSchema:{type:"object",properties:{firstName:{anyOf:[{type:"string",enum:["one","two","three"]},{type:"string"}]}}}
}

describe('MaterialAnyOfStringOrEnumControlWithCheck', () => {
    // It fails to render, no idea why. The actual usage of this element works
    // it('MaterialAnyOfStringOrEnumControlWithCheck renderer',()=>{
    //     render(
    //     <HandleFieldsCheckProvider>
    //         <MaterialAnyOfStringOrEnumControlWithCheck {...mockProps}/>
    //     </HandleFieldsCheckProvider>
    //     );

    //     const renderedField = screen.queryByRole('combobox');
    //     expect(renderedField).toBeInTheDocument()
    //     expect(renderedField!.id).toBe(mockProps.id)
    //     expect(screen.getByText('Some Field')).toBeInTheDocument()
    //     expect(screen.queryAllByRole('checkbox').length).toBe(1)
    // });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialAnyOfStringOrEnumControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(105)
    });
})
