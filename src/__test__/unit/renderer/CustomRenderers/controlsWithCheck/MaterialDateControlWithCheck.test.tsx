import { render, screen } from "@testing-library/react";
import { MaterialDateControlWithCheck, materialDateControlWithCheckTester 
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'
import { theme } from "renderer/utils/theme";
import { ThemeProvider } from '@mui/material/styles';

const mockProps: ControlProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "string", format: "date"},
    uischema: {type: 'Control', scope: '#/properties/some_field'},
    visible: true
}
describe('MaterialDateControlWithCheck', () => {
    it('MaterialDateControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <ThemeProvider theme={theme}>
                <MaterialDateControlWithCheck {...mockProps}/>
            </ThemeProvider>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryByRole('textbox');
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id+'-input')
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialDateControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(104)
    });
})
