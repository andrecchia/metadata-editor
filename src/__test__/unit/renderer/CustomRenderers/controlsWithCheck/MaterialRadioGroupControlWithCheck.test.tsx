import { render, screen } from "@testing-library/react";
import { MaterialRadioGroupControlWithCheck , materialRadioGroupControlWithCheckTester
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps, OwnPropsOfEnum } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'

const mockProps: ControlProps & OwnPropsOfEnum = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "string", enum:["one", "two"]},
    uischema: {type: 'Control', scope: '#/properties/some_field', options: { format: "radio"}},
    visible: true,
    options:[{label:"one",value:"one"},{label:"two",value:"two"}]
}

describe('MaterialRadioGroupControlWithCheck', () => {
    it('MaterialRadioGroupControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <MaterialRadioGroupControlWithCheck {...mockProps}/>
        </HandleFieldsCheckProvider>
        );
        
        const renderedField = screen.queryByRole('radiogroup');
        expect(renderedField).toBeInTheDocument()
        expect(screen.getByText('one')).toBeInTheDocument()
        expect(screen.getByText('two')).toBeInTheDocument()
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(1)
    });
    it('MaterialBooleanControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialRadioGroupControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(120)
    });
})
