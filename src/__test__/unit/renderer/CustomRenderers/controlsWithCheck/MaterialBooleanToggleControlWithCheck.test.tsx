import { render, screen } from "@testing-library/react";
import { MaterialBooleanToggleControlWithCheck, materialBooleanToggleControlWithCheckTester 
} from "renderer/components/CustomRenderers/controlsWithCheck";
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import { ControlProps } from "@jsonforms/core";
import { TesterContext } from "@jsonforms/core";
import '__test__/test-utils/matchMedia.mock'

const mockProps: ControlProps = {
    data: undefined,
    enabled: true,
    errors: "",
    handleChange: (path: string, value: any) => {},
    id: "some_id",
    label: "some_label",
    path: "some_path",
    rootSchema: {},
    schema: {type: "boolean"},
    uischema: {type: 'Control', scope: '#/properties/some_field', options: {"toggle": true}},
    visible: true
}
describe('MaterialBooleanToggleControlWithCheck', () => {
    it('MaterialBooleanToggleControlWithCheck renderer',()=>{
        render(
        <HandleFieldsCheckProvider>
            <MaterialBooleanToggleControlWithCheck {...mockProps}/>
        </HandleFieldsCheckProvider>
        );

        const renderedField = screen.queryAllByRole('checkbox')[1];
        expect(renderedField).toBeInTheDocument()
        expect(renderedField!.id).toBe(mockProps.id+'-input')
        expect(screen.getByText('Some Field')).toBeInTheDocument()
        expect(screen.queryAllByRole('checkbox').length).toBe(2)
    });
    it('MaterialBooleanToggleControlWithCheck tester',()=>{
        const context: TesterContext = {
            rootSchema: {},
            config: ""
        }
        const tester = materialBooleanToggleControlWithCheckTester(mockProps.uischema, mockProps.schema, context);
        expect(tester).toBe(103)
    });
})
