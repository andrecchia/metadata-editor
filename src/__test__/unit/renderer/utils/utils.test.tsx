import { waitFor } from "@testing-library/react";
import { getJsonSchemaPaths, saveFile, loadFile } from "renderer/utils/utils"

describe('Utils functions tests', () => {
    it('getJsonPaths test', () => {
        const schema = {
            "$id":"https://example.com/schemas/customer",
            "type":"object",
            "properties":{
                "first_name":{"type":"string"},
                "last_name":{"type":"string"},
                "entryID":{
                    "type":"object",
                    "properties":{
                        "revisonID":{
                            "type":"object",
                            "properties":{
                                "identifierValue":{"type":"string"},
                                "identifierType":{"type":"string"}}
                            },
                            "revisionComment":{"type":"string"}
                        }
                    }
                },
                "required":["first_name","last_name","shipping_address","billing_address"],
                "$defs":{
                    "name":{"type":"string"},
                    "revision":{
                        "type":"object",
                        "properties":{
                            "revisonID":{
                                "type":"object",
                                "properties":{
                                    "identifierValue":{"type":"string"},
                                    "identifierType":{"type":"string"}
                                }
                            },
                            "revisionComment":{"type":"string"}
                        }
                    },
                    "identifier":{
                        "type":"object",
                        "properties":{
                            "identifierValue":{"type":"string"},
                            "identifierType":{"type":"string"}
                        }
                    }
                }
            }
        const schemaPaths = {
            "first_name":false,
            "last_name":false,
            "entryID.revisonID.identifierValue":false,
            "entryID.revisonID.identifierType":false,
            "entryID.revisionComment":false
        }
        const schemaPathsParent = {
            "parent.first_name":false,
            "parent.last_name":false,
            "parent.entryID.revisonID.identifierValue":false,
            "parent.entryID.revisonID.identifierType":false,
            "parent.entryID.revisionComment":false
        }
        const schemaWrong = {
            "$id": "https://example.com/person.schema.json",
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "title": "Person",
            "type": "object",
            "wrongProperties": {
              "firstName": {
                "type": "string",
                "description": "The person's first name."
              },
              "lastName": {
                "type": "string",
                "description": "The person's last name."
              },
              "age": {
                "description": "Age in years which must be equal to or greater than zero.",
                "type": "integer",
                "minimum": 0
              }
            }
          }
        expect(getJsonSchemaPaths(schema)).toEqual(schemaPaths)
        expect(getJsonSchemaPaths(schema, 'parent')).toEqual(schemaPathsParent)
        expect(getJsonSchemaPaths({})).toEqual({})
        expect(getJsonSchemaPaths(schemaWrong)).toEqual({})
    });

    it('saveFile test', () => {

        global.URL.createObjectURL = jest.fn(URL.createObjectURL);
        const mLink = {
             href: '', 
             click: jest.fn(), 
             download: '', 
             setAttribute: jest.fn() } as any;
        const spyCreateElement = jest.spyOn(document, 'createElement').mockReturnValueOnce(mLink);

        const fileContent = { name: "John", age: 30 };


        saveFile(fileContent);

        const blob = new Blob([JSON.stringify(fileContent)],{ type: "text/plain" })
        const url = URL.createObjectURL(blob);

        expect(window.URL.createObjectURL)
        .toHaveBeenCalledWith(blob);
        expect(spyCreateElement).toHaveBeenCalledWith("a");
        expect(mLink.setAttribute.mock.calls.length).toBe(2);
        expect(mLink.setAttribute.mock.calls[0]).toEqual(['download', 'metadata_document.json']);
        expect(mLink.setAttribute.mock.calls[1]).toEqual(['href', url]);
        expect(mLink.click).toBeCalled();
      });

      it('loadFile test', async () => {

        const mockHandleLoadFile = jest.fn();        
        const fileContent = { name: "John", age: 30 };
        const blob = new Blob([JSON.stringify(fileContent)],{ type: "text/plain" })
        const mockChangeEvent  = {
                target: {
                    files: [blob]
                }
            }
        global.FileReader.prototype.readAsText = jest.fn(FileReader.prototype.readAsText)

        loadFile(mockChangeEvent as any, mockHandleLoadFile);
            
        expect(window.FileReader.prototype.readAsText)
        .toHaveBeenCalledWith(blob);
        await waitFor(()=>expect(mockHandleLoadFile).toHaveBeenCalled());
        expect(mockHandleLoadFile.mock.calls[0][0]).toEqual(mockChangeEvent);
        expect(mockHandleLoadFile.mock.calls[0][1]).toEqual(fileContent);
      });

})