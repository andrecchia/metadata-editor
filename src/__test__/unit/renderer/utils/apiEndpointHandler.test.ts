import { apiEndpointMessagesHandler } from "renderer/utils/apiEndpointHandler";
const schemaPaths = {
    "study.studyID": false,
    "study.studyTitle": false,
    "study.studyDateTime": false,
    "study.program": false,
    "study.user.name": false,
    "study.user.role": false,
    "study.user.affiliation.institutionName": false,
    "study.user.affiliation.institutionAcronym": false,
    "study.user.affiliation.institutionDepartment": false,
    "study.user.affiliation.institutionID": false,
    "study.user.email": false,
    "study.sample.sampleName": false,
    "study.sample.sampleID": false,
    "study.sample.sampleSize.value": false,
    "study.sample.sampleSize.unit": false,
    "study.sample.sampleWeight.value": false,
    "study.sample.sampleWeight.unit": false,
    "study.sample.measurementConditions.value": false,
    "study.sample.measurementConditions.unit": false,
    "study.instrument.instrumentName": false,
    "study.instrument.instrumentID": false,
    "study.instrument.instrumentManufacturer.manufacturerName": false,
    "study.instrument.instrumentManufacturer.modelName": false,
    "study.instrument.instrumentManufacturer.manufacturerID": false,
    "study.series": false
}

/** Mock requestListener */
let mockValue = {}
const mockSend = jest.fn()
const mockRequestListener = jest.fn(callback => {
    const event = {
        sender: {
            send: mockSend
        }
    }
    callback(event, mockValue)
})

global.electron = {
    API: {
        requestListener: mockRequestListener,
        removeRequestListener: jest.fn(),
    }
}

// Mock setData
const mockSetData = jest.fn()

// Mock electronStore
let mockedSchema = {}
jest.mock('renderer/components/CustomRenderers/util/lockedUtils',()=>{
    return{
        electronStore:{
            get: jest.fn().mockImplementation((schemaMaskId)=>{
                return mockedSchema
            })
        }
    }
})

describe('apiEndpointMessagesHandler tests', () => {
    afterEach(()=>{
        jest.clearAllMocks();
    })
    it('Compile - Correct payload, checkFields false',() => {
        mockValue = JSON.stringify({
            path: '/api/v1/compile',
            content: {
                "study": {
                    "studyID": "Study ID test",
                    "studyDateTime": "2024-04-16T00:00:00+02:00",
                    "user": {
                        "name": "Joe"
                    }
                }
            }
          })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, false)
        const respBack = {requestName: "RESP_COMPILE", resp:[]}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).toHaveBeenCalledTimes(3)
        expect(mockSetData).toHaveBeenCalledWith(expect.any(Function))
    })
    it('Compile - Correct payload with array, checkFields false',() => {
        mockValue = JSON.stringify({
            path: '/api/v1/compile',
            content: {
                "study": {
                    "studyID": "Study ID test",
                    "studyDateTime": "2024-04-16T00:00:00+02:00",
                    "user": {
                        "name": "Joe"
                    },
                    "series": [
                        {
                            "seriesID": "Test",
                            "sequenceProtocol": {
                                "repetitionTime": {
                                    "value": 0.2,
                                    "unit": "ms"
                                }
                            },
                            "images": {
                                "allImages": {
                                    "pixelSpacing": {
                                        "value": [
                                            0.1
                                        ]
                                    }
                                }
                            }
                        }
                    ]

                }
            }
          })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, false)
        const respBack = {requestName: "RESP_COMPILE", resp:[]}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).toHaveBeenCalledTimes(7)
        expect(mockSetData).toHaveBeenCalledWith(expect.any(Function))
    })
    it('Compile - Payload with not existent schema paths, checkFields false',() => {
        mockValue = JSON.stringify({
            path: '/api/v1/compile',
            content: {
                "study": {
                    "studyID": "Study ID test",
                    "fakePath1": "so fake",
                    "fakePath2": {
                        "notOnlyFake": "butEvenNested"
                    }
                }
            }
          })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, false)
        const respBack = {requestName: "RESP_COMPILE", resp:["study.fakePath1","study.fakePath2.notOnlyFake"]}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).toHaveBeenCalledTimes(1)
        expect(mockSetData).toHaveBeenCalledWith(expect.any(Function))
    })
    it('Compile - Correct payload, checkFields true',() => {
        mockedSchema = {
            "study.studyID": true,
            "study.studyDateTime": true,
            "study.user.name": false
        }
        mockValue = JSON.stringify({
            path: '/api/v1/compile',
            content: {
                "study": {
                    "studyID": "Study ID test",
                    "studyDateTime": "2024-04-16T00:00:00+02:00",
                    "user": {
                        "name": "Joe"
                    }
                }
            }
          })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, true)
        const respBack = {requestName: "RESP_COMPILE", resp:[]}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).toHaveBeenCalledTimes(2)
        expect(mockSetData).toHaveBeenCalledWith(expect.any(Function))
    })
    it('Compile - Payload with not existent schema paths, checkFields true',() => {
        mockValue = JSON.stringify({
            path: '/api/v1/compile',
            content: {
                "study": {
                    "studyID": "Study ID test",
                    "fakePath1": "so fake",
                    "fakePath2": {
                        "notOnlyFake": "butEvenNested"
                    },
                    "user": {
                        "name": "Joe"
                    }
                }
            }
            })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, true)
        const respBack = {requestName: "RESP_COMPILE", resp:["study.fakePath1","study.fakePath2.notOnlyFake"]}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).toHaveBeenCalledTimes(1)
        expect(mockSetData).toHaveBeenCalledWith(expect.any(Function))
    })
    it('Read',()=>{
        const compiledData = {
            "study": {
                "studyID": "Study ID test",
                "studyDateTime": "2024-04-16T00:00:00+02:00",
                "user": {
                    "name": "Joe"
                }
            }
        }
        mockValue = JSON.stringify({
            path: '/api/v1/read',
          })
        apiEndpointMessagesHandler(mockSetData, schemaPaths, compiledData, false)
        const respBack = {requestName: "RESP_READ", resp:compiledData}
        expect(mockSend).toHaveBeenCalledWith("API:response",JSON.stringify(respBack))
        expect(mockSetData).not.toHaveBeenCalled()
    })
    it('Wrong value passed from endpoint',()=>{
        mockValue = 'bad json'
        apiEndpointMessagesHandler(mockSetData, schemaPaths, {}, false)
        expect(mockSend).toHaveBeenCalledWith('API:response',"ERROR at apiEndpointMessagesHandler")
        expect(mockSetData).not.toHaveBeenCalled()
    })
})