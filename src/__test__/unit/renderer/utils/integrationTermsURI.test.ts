import { resolveTermURIs } from "renderer/utils/integrationTermsURI";

const properties = {
    aai: {
      $ref: "#/$defs/aai"
    },
    phaseOfMatter: {
      $ref: "#/$defs/phaseOfMatter"
    },
    accelerationVoltage: {
      $ref: "#/$defs/accelerationVoltage"
    }
  }

const schemaToResolve = {
    $defs: {
        aai: {
            type: "object",
            properties: {
                aaiValues: {
                    termURI: "https://matwerk.datamanager.kit.edu/evoks/rest/v1/MatWerkAcronyms-1/data?uri=https://purls.helmholtz-metadaten.de/evoks/MatWerkAcronyms/aai&format=application%2Fjson",
                    type: "string",
                    title: "",
                    description: ""
                }
            }
        },
        phaseOfMatter: {
            type: "object",
            properties: {
                phaseOfMatterValues: {
                    termURI : "https://matwerk.datamanager.kit.edu/evoks/rest/v1/sampleDescription-1/data?uri=https://purls.helmholtz-metadaten.de/evoks/sdv/phaseOfMatter&format=application%2Fjson",
                    type: "string",
                    title : "",
                    description : "",
                    enum: []
                }
            }
        },
        accelerationVoltage:{
               termURI : "https://service.tib.eu/ts4tib/api/ontologies/emg/terms?iri=https%3A%2F%2Fpurls.helmholtz-metadaten.de%2Femg%2FEMG_00000004",
               type: "string",
               title : "",
               description:""
            }
    },
    type: "object",
    properties: {...properties}
  }

const resolvedSchema = {
    $defs: {
        aai: {
            type: "object",
            properties: {
                aaiValues: {
                    termURI: "https://matwerk.datamanager.kit.edu/evoks/rest/v1/MatWerkAcronyms-1/data?uri=https://purls.helmholtz-metadaten.de/evoks/MatWerkAcronyms/aai&format=application%2Fjson",
                    type: "string",
                    title: "AAI",
                    description: "Authentication & Authorization Infrastructure"
                }
            }
        },
        phaseOfMatter: {
            type: "object",
            properties: {
                phaseOfMatterValues: {
                    termURI: "https://matwerk.datamanager.kit.edu/evoks/rest/v1/sampleDescription-1/data?uri=https://purls.helmholtz-metadaten.de/evoks/sdv/phaseOfMatter&format=application%2Fjson",
                    type: "string",
                    title: "Phase of Matter",
                    description: "",
                    enum: [
                        "Gas",
                        "Liquid",
                        "Plasma",
                        "Solid"
                    ]
                }
            }
        },
        accelerationVoltage: {
            termURI: "https://service.tib.eu/ts4tib/api/ontologies/emg/terms?iri=https%3A%2F%2Fpurls.helmholtz-metadaten.de%2Femg%2FEMG_00000004",
            type: "string",
            title: "Acceleration Voltage",
            description: "The potential difference between anode and cathode."
        }
    },
    type: "object",
    properties: {...properties}
}

describe('resolveTermURIs tests', () => {
    it('Should resolve correctly the schema', async () => {
        await resolveTermURIs(schemaToResolve)
        expect(schemaToResolve).toEqual(resolvedSchema)
    })
})