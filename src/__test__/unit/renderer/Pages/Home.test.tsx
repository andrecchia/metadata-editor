import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import { Home } from 'renderer/components/Pages/Home'
import {BrowserRouter} from 'react-router-dom'


describe('Home page', () => {
    
  it('Home page rendering',() => {
    render(<Home />, {wrapper: BrowserRouter})
  
    // verify page content for default route
    const logo = screen.getByRole('img');
    expect(logo).toHaveAttribute('src', 'test-file-stub');
    // expect(logo).toHaveAttribute('src', 'metadataEditorLogo.png');
    expect(screen.getByText('MetaStore schemas')).toBeInTheDocument()
    expect(screen.getByText('Local documents')).toBeInTheDocument()
  });

})
