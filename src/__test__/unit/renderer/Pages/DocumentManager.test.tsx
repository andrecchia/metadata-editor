import {fireEvent, render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import { DocumentManager } from 'renderer/components/Pages/DocumentManager'
import {BrowserRouter, NavigateFunction} from 'react-router-dom'
import { useAuth } from 'renderer/AuthContext'
import * as utils from 'renderer/utils/utils'

jest.mock('renderer/AuthContext');
const mockUseAuth= useAuth as jest.MockedFunction<typeof useAuth>;

// mock useNavigate
const mockUseNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
   useNavigate: () => mockUseNavigate, // Return an empty jest function to test whether it was called or not...I'm not depending on the results so no need to put in a return value
 }));

describe('DocumentManager page (user not authenticated)', () => {
  beforeEach(()=>{
      // Mock useAuth with user not authenticated
      mockUseAuth.mockImplementation(() => {
        const auth = {
          authenticated: false,
          token: null,
          userInfo: null
        };
        return auth;
      })
    }) 
  afterEach(() => window.sessionStorage.clear())

  it('Page rendering',() => {
    render(<DocumentManager />, {wrapper: BrowserRouter})
  
    // verify page content
    expect(screen.getByText('Upload to MetaStore')).toBeInTheDocument()
    expect(screen.getByText('Export')).toBeInTheDocument()
    expect(screen.getByText('Save')).toBeInTheDocument()
    expect(screen.getByText('Back')).toBeInTheDocument()
    expect(screen.queryByText('Logout')).not.toBeInTheDocument()
  });

  it('calls saveFile when Export json document button is clicked', () => {
    // Mock saveFile
    const mockSaveFile = jest.fn()
    jest.spyOn(utils, 'saveFile').mockImplementation(mockSaveFile)
    // Put a mock compiled document in sessionStorage
    const mockCompiledDocument = { name: "John", age: 30 }
    window.sessionStorage.setItem('compiledDocument',JSON.stringify(mockCompiledDocument))

    render(<DocumentManager />, { wrapper: BrowserRouter });
    const downloadButton = screen.getByLabelText('export');
    fireEvent.click(downloadButton);

    // Assertions after the download button is called
    expect(mockSaveFile).toHaveBeenCalled();
    expect(mockSaveFile).toHaveBeenCalledWith(mockCompiledDocument)
  });

  it('calls navigate(-1) when Back button is clicked', () => {

    render(<DocumentManager />, { wrapper: BrowserRouter });
    const backButton = screen.getByText('Back');
    fireEvent.click(backButton);

    expect(mockUseNavigate).toHaveBeenCalled()
    expect(mockUseNavigate).toHaveBeenLastCalledWith(-1)
  });


  //Cannot make this work
  // it('Login',() => {
  //   render(<DocumentManager />, {wrapper: BrowserRouter})

  //   const loginButton = screen.getByLabelText('login');
  //   fireEvent.click(loginButton);

  //   // just to see that pushing the button login is called
  //   expect(window.electron.auth.signIn()).toHaveBeenCalled()
  // });

})

describe('DocumentManager page (user authenticated)', () => {   
  beforeEach(()=>{
    // Mock useAuth with user not authenticated
    mockUseAuth.mockImplementation(() => {
      const auth = {
        authenticated: true,
        token: 'mockToken',
        userInfo: {preferred_username:'username'}
      };
      return auth;
    })
  }) 
  it('Page rendering',() => {
    render(<DocumentManager />, {wrapper: BrowserRouter})

    // verify page content
    expect(screen.getByText('Upload to MetaStore')).toBeInTheDocument()
    expect(screen.getByText('Export')).toBeInTheDocument()
    expect(screen.getByText('Save')).toBeInTheDocument()
    expect(screen.getByText('Back')).toBeInTheDocument()
    expect(screen.queryByText('Logout (username)')).toBeInTheDocument()
  });

  //Cannot make this work
  // it('Logout',() => {
  //   render(<DocumentManager />, {wrapper: BrowserRouter})

  //   const logoutButton = screen.getByText('Logout (username)');
  //   fireEvent.click(logoutButton);

  //   // just to see that pushing the button logout is called
  //    expect(window.electron.auth.signOut()).toHaveBeenCalled()
  // });
})