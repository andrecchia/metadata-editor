// Mock saveFile
const mockSaveFile = jest.fn()
import { Provenance, jsonFromProvenance, getProvenance } from "renderer/components/Pages/Provenance";
import {act, fireEvent, render, screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import { metadataDocumentIdSortedByDate } from "renderer/components/widgets/Parents";
import { arrProvenanceTree1, arrProvenanceTree2, forProvenanceDocModel, forProvenanceRecord, jsonProvenanceResultTree1, jsonProvenanceResultTree2 } from "./provenanceTestData";

// Mock zustand
jest.mock('renderer/components/Pages/SelectMetaStoreInstance', () => ({
    useMetaStoreInstanceState: {
        getState: jest.fn().mockReturnValue({
            MetaStoreInstance: "MetaRepo"
        })
    }
}));

// Mock getSchemaFromMetaStore and saveFile, while keeping the other utils functions
jest.mock('renderer/utils/utils', () => {
    const original = jest.requireActual('renderer/utils/utils');
    return {
      ...original,
      scanSchemasFromMetaStore: jest.fn().mockResolvedValue({
        schemaLabel: [
          "measurement",
          "input",
          "analysis",
          "precursor",
          "sample"
        ],
        schemaIdLabel: {
            "measurement": [
                "SEM_schema",
                "datacite",
                "dummy_schema",
                "mri_schema"
            ],
            "input": [
                "input_schema",
                "nep_proposal"
            ],
            "analysis": [
                "mldata_basic_schema",
                "mri_ml"
            ],
            "precursor": [
                "precursor_schema"
            ],
            "sample": [
                "sample_schema"
            ]
        },
        schemaLastVersion: {
          "SEM_schema": 1,
          "datacite": 1,
          "dummy_schema": 1,
          "input_schema": 1,
          "mldata_basic_schema": 2,
          "mri_ml": 1,
          "mri_schema": 8,
          "nep_proposal": 2,
          "precursor_schema": 1,
          "sample_schema": 1
      }
      }),
      saveFile: mockSaveFile
    };
  });
jest.mock('renderer/components/widgets/Parents')

describe('Provenance page', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render without crash',async ()=>{
        render(<Provenance />, {wrapper: MemoryRouter})

        // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
        // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
        await screen.findByText('Select metadata document')

        // Assertions
        expect(screen.getByText('Select metadata document')).toBeInTheDocument()
        expect(screen.getByText('Get Provenance')).toBeInTheDocument()
        expect((screen.getByText('Get Provenance') as HTMLButtonElement).disabled).toBe(true)
        expect(screen.getByText('Related schema ID')).toBeInTheDocument()
    })

    it('select a schema id with no document registered', async()=>{
        // Mock metadataDocumentIdSortedByDate resolving with an empty array (no doc registered)
        (metadataDocumentIdSortedByDate as jest.Mock).mockResolvedValueOnce([])
        render(<Provenance />, {wrapper: MemoryRouter})

        // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
        // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
        await screen.findByText('Select metadata document')

        const dropdown = screen.getByLabelText('Related schema ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdown);

        const options = screen.getAllByRole('option');
        act(() => { options[0].click(); });

        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        // Assert that selected schema is visible
        expect(screen.getByDisplayValue('SEM_schema')).toBeInTheDocument()
        // Assert that a 'no document registered' message is rendered
        expect(screen.getByText('No metadada documents registered on MetaStore for schemaId "SEM_schema"')).toBeInTheDocument()
        // Get provenance should be disabled
        expect((screen.getByText('Get Provenance') as HTMLButtonElement).disabled).toBe(true)
    })

    it('select a schema id with some document registered', async()=>{
        // Mock metadataDocumentIdSortedByDate resolving with an some elements in the array
        (metadataDocumentIdSortedByDate as jest.Mock).mockResolvedValueOnce([
            'id: id1, last update: date1',
            'id: id2, last update: date2',
            'id: id3, last update: date3',
        ])
        render(<Provenance />, {wrapper: MemoryRouter})

        // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
        // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
        await screen.findByText('Select metadata document')

        const dropdown = screen.getByLabelText('Related schema ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdown);

        const options = screen.getAllByRole('option');
        act(() => { options[0].click(); });

        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        // Assert that selected schema is visible
        expect(screen.getByDisplayValue('SEM_schema')).toBeInTheDocument()

        // Select now the doocument for the provenance
        const dropdownDoc = screen.getByLabelText('Metadata document ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdownDoc);

        // Select the 1st entry
        const optionsDoc = screen.getAllByRole('option');
        act(() => { optionsDoc[0].click(); });

        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        // Assert that selected document is visible
        expect(screen.getByDisplayValue('id: id1, last update: date1')).toBeInTheDocument()
        // Get provenance should be enabled
        expect((screen.getByText('Get Provenance') as HTMLButtonElement).disabled).toBe(false)
    })

    it('get provenance - found some provenance', async () => {
        // Mock metadataDocumentIdSortedByDate resolving with an some elements in the array
        (metadataDocumentIdSortedByDate as jest.Mock).mockResolvedValueOnce([
            'id: id1, last update: date1',
            'id: id2, last update: date2',
            'id: id3, last update: date3',
        ])
        render(<Provenance />, {wrapper: MemoryRouter})

        // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
        // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
        await screen.findByText('Select metadata document')

        // Select a document to make 'Get Provenance' button be enabled
        const dropdown = screen.getByLabelText('Related schema ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdown);
        const options = screen.getAllByRole('option');
        fireEvent.click(options[0])
        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        // Select now the doocument for the provenance
        const dropdownDoc = screen.getByLabelText('Metadata document ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdownDoc);
        // Select the 1st entry
        const optionsDoc = screen.getAllByRole('option');
        fireEvent.click(optionsDoc[0])

        // Await that the async functions are resolved
        await screen.findByText('id: id1, last update: date1')

        
        const mockFetchResponse = value => ({ json: async() => value })
        global.fetch = jest.fn()
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing', 1, 1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        global.alert = jest.fn();
        
        // click 'get provenance' button
        const getProvenanceButton = screen.getByText('Get Provenance')
        fireEvent.click(getProvenanceButton)

        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        expect(mockSaveFile).not.toHaveBeenCalled();
        expect(global.alert).toHaveBeenCalled();
        expect(global.alert).toHaveBeenCalledWith("Missing provenance information in metadata document with id \"id1\"")
    })

    it('get provenance - no provenance found', async () => {
        // Mock metadataDocumentIdSortedByDate resolving with an some elements in the array
        (metadataDocumentIdSortedByDate as jest.Mock).mockResolvedValueOnce([
            'id: id1, last update: date1',
            'id: id2, last update: date2',
            'id: id3, last update: date3',
        ])
        render(<Provenance />, {wrapper: MemoryRouter})

        // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
        // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
        await screen.findByText('Select metadata document')

        // Select a document to make 'Get Provenance' button be enabled
        const dropdown = screen.getByLabelText('Related schema ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdown);
        const options = screen.getAllByRole('option');
        fireEvent.click(options[0])
        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        // Select now the doocument for the provenance
        const dropdownDoc = screen.getByLabelText('Metadata document ID');
        // open the dropdown menu
        fireEvent.mouseDown(dropdownDoc);
        // Select the 1st entry
        const optionsDoc = screen.getAllByRole('option');
        fireEvent.click(optionsDoc[0])

        // Await that the async functions are resolved
        await screen.findByText('id: id1, last update: date1')

        
        const mockFetchResponse = value => ({ json: async() => value })
        global.fetch = jest.fn()
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('precursor', 1, 1, true, true)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing', 1, 1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))

        // click 'get provenance' button
        const getProvenanceButton = screen.getByText('Get Provenance')
        fireEvent.click(getProvenanceButton)

        // Await that the async functions are resolved
        await screen.findByText('Select metadata document')

        expect(mockSaveFile).toHaveBeenCalled();
        expect(mockSaveFile).toHaveBeenCalledWith(
            {
            "id1":{
                "referenceType":"MetaStore URI",
                "reference":"https://metarepo.nffa.eu/api/v1/metadata/id1",
                "type":"measurement",
                "parents":[{
                    "precursor1":{
                        "referenceType":"MetaStore URI",
                        "reference":"https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                        "type":"precursor","parents":[]
                    }}]
                },"proposalIds":[]
            }, 
                false, 
                'id1_provenance.json'
        )
    })
})

describe('Provenance functions test', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('jsonFromProvenance - array with metarepo url, tree 1', () => {
        const provenanceJson = jsonFromProvenance(arrProvenanceTree1, 'analysed data')
        expect(JSON.stringify(provenanceJson)).toBe(JSON.stringify(jsonProvenanceResultTree1))
    })
    it('jsonFromProvenance - array with metarepo url, tree 2', () => {
        const provenanceJson = jsonFromProvenance(arrProvenanceTree2, 'analysed data')
        expect(JSON.stringify(provenanceJson)).toBe(JSON.stringify(jsonProvenanceResultTree2))
    })
    it('jsonFromProvenance - a non metarepo url', () => {
        const expectedRes = {
            "analysed_data2":{
                "referenceType":"MetaStore URI",
                "reference":"https://metarepo.nffa.eu/api/v1/metadata/analysed_data2",
                "type":"analysed data",
                "parents":[{
                    "6e34f0a14996400f0836f878b3ad117f9e166fa5f43ef639b3227b05c0c00a9c":{
                        "referenceType":"Internal",
                        "reference":"another_kind_of_ref",
                        "type":"analysed data","parents":[]
                    }}]
                },"proposalIds":[]
            }
        const arrProvenanceNonMR = [
            [
                {
                    "documentId": "analysed_data2",
                    "parentType": "analysed data",
                    "parentReferenceType": "Internal",
                    "parentReference": "another_kind_of_ref"
                }
            ]
        ]
        const provenanceJson = jsonFromProvenance(arrProvenanceNonMR, 'analysed data')
        expect(JSON.stringify(provenanceJson)).toBe(JSON.stringify(expectedRes))
    })
    it('getProvenance - provenance tree 1', async () => {
        const mockFetchResponse = value => ({ json: async() => value })
        /* Mock a multiple fetch responses to have this provenance structure:

                input1  input2  input3  input4  input5
                  |       |       |       |       |
                  |        -------         -------
                  |           |               |
                precursor1  precursor2    precursor3
                  |           |               |
                   -----------                |
                        |                     |
                    sample1                 sample2
                        |                     |
                         ---------------------
                                   |
                                  raw1
                                   |
                                analysis1
                                   |
                                analysis2
        */
        global.fetch = jest.fn()
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('raw data',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('sample',1,2)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('precursor',1,2)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('precursor',3,3)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('input',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('input',2,3)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('input',4,5)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        const result: any[] = [];
        getProvenance('analysed_data2',result, 'fakeToken').then(()=>{
            expect(result).toEqual(arrProvenanceTree1)
        })
    })
    it('getProvenance - tree 2', async () => {
        const mockFetchResponse = value => ({ json: async() => value })
        /* Mock multiple fetch responses to have this provenance structure:
                       input1
                           |
                   -----------------
                   |               |
               precursor1     precursor2
                   |_______________|
                           |
                       sample1
                           |
                       raw data1
                           |
                   -----------------
                   |               |
               analysed1       analysed2
                   |_______________|
                           |
                        analysed3
                           |
                        analysed4
                           |
             -------------------------------
             |             |               |
           analysed5    analysed6     analysed7
             |             |               |
             -------------------------------
                           |
                       analysed8
        */
        global.fetch = jest.fn()
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',5,7)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',4,4)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',4,4)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',4,4)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',3,3)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data',1,2)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('raw data',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('raw data',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('sample',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('precursor',1,2)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('input',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('input',1,1)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing',1,1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))

        const result: any[] = [];
        getProvenance('analysed_data8',result, 'fakeToken').then(()=>{
            expect(result).toEqual(arrProvenanceTree2)
        })
    })
    it('getProvenance - nested parents', async () => {
        const mockFetchResponse = value => ({ json: async() => value })
        global.fetch = jest.fn()
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('analysed data', 1, 1, true, true)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceDocModel('nothing', 1, 1, false)))
        .mockResolvedValueOnce(mockFetchResponse(forProvenanceRecord()))
        const result: any[] = [];
        getProvenance('analysed_data2',result, 'fakeToken').then(()=>{
            expect(result).toEqual([arrProvenanceTree1[0]])
        })
    })
})