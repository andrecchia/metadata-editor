/**
 * Generates a fake document with `parents` array field containing N entries for provenance test.
 * N is (parentsEnd-parentsStart)
 * @param parentType label for parent
 * @param parentsStart 
 * @param parentsEnd 
 * @param hasParents if false the generated document will not have `parents` field
 * @param isNested if true insert the` parents` field  at one nested level
 * @returns 
 */
export const forProvenanceDocModel = (parentType: string, parentsStart: number, parentsEnd: number, hasParents: boolean = true, isNested: boolean = false) =>  {
    if(hasParents){
        const parents: any[] = []
        for(let i=parentsStart; i<=parentsEnd; ++i){
            parents.push({
                "parentType": parentType,
                "parentReferenceType": "MetaStore URI",
                "parentReference": `https://metarepo.nffa.eu/api/v1/metadata/${parentType.replaceAll(' ', '_')}${i}`
            })
        }
        const parentsObj = isNested ? {"nested": {"parents": parents}} : {"parents": parents}    
        return { aField: { aKey: "aValue" }, ...parentsObj }
    } else {
        return { aField: { aKey: "aValue" } }
    }
}

/**
 * Generates a fake metadata record
 * @param hasProposaId 
 * @returns 
 */
export const forProvenanceRecord = (hasProposaId: boolean = false) => {
    return{
    "id": "someID",
    "relatedResource": {
        "identifier": "someIdentifier",
        "identifierType": "INTERNAL"
    },
    "createdAt": "2024-04-05T06:52:20Z",
    "lastUpdate": "2024-04-05T06:52:20.506Z",
    "schema": {
        "identifier": "someIdentifier",
        "identifierType": "URL"
    },
    "schemaVersion": 1,
    "recordVersion": 1,
    "acl": [
        {
            "id": 38,
            // random in [1, 500] if hasId is true
            "sid": hasProposaId ? Math. floor(Math. random()*500) + 1: "SELF",
            "permission": "ADMINISTRATE"
        }
    ],
    "metadataDocumentUri": "someMetadataDocumentUri",
    "documentHash": "sha1:dc5140bdc469a62c5d3cd83f553ec35b77d507f3"
}}

export const arrProvenanceTree1=[
    [
        {
            "documentId": "analysed_data2",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1"
        }
    ],
    [
        {
            "documentId": "analysed_data1",
            "parentType": "raw data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1"
        }
    ],
    [
        {
            "documentId": "raw_data1",
            "parentType": "sample",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/sample1"
        },
        {
            "documentId": "raw_data1",
            "parentType": "sample",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/sample2"
        }
    ],
    [
        {
            "documentId": "sample1",
            "parentType": "precursor",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1"
        },
        {
            "documentId": "sample1",
            "parentType": "precursor",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2"
        }
    ],
    [
        {
            "documentId": "sample2",
            "parentType": "precursor",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/precursor3"
        }
    ],
    [
        {
            "documentId": "precursor1",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input1"
        }
    ],
    [
        {
            "documentId": "precursor2",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input2"
        },
        {
            "documentId": "precursor2",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input3"
        }
    ],
    [
        {
            "documentId": "precursor3",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input4"
        },
        {
            "documentId": "precursor3",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input5"
        }
    ]
]

export const arrProvenanceTree2 = [
    [
        {
            "documentId": "analysed_data8",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data5"
        },
        {
            "documentId": "analysed_data8",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data6"
        },
        {
            "documentId": "analysed_data8",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data7"
        }
    ],
    [
        {
            "documentId": "analysed_data5",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4"
        }
    ],
    [
        {
            "documentId": "analysed_data6",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4"
        }
    ],
    [
        {
            "documentId": "analysed_data7",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4"
        }
    ],
    [
        {
            "documentId": "analysed_data4",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data3"
        }
    ],
    [
        {
            "documentId": "analysed_data3",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1"
        },
        {
            "documentId": "analysed_data3",
            "parentType": "analysed data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data2"
        }
    ],
    [
        {
            "documentId": "analysed_data1",
            "parentType": "raw data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1"
        }
    ],
    [
        {
            "documentId": "analysed_data2",
            "parentType": "raw data",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1"
        }
    ],
    [
        {
            "documentId": "raw_data1",
            "parentType": "sample",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/sample1"
        }
    ],
    [
        {
            "documentId": "sample1",
            "parentType": "precursor",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1"
        },
        {
            "documentId": "sample1",
            "parentType": "precursor",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2"
        }
    ],
    [
        {
            "documentId": "precursor1",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input1"
        }
    ],
    [
        {
            "documentId": "precursor2",
            "parentType": "input",
            "parentReferenceType": "MetaStore URI",
            "parentReference": "https://metarepo.nffa.eu/api/v1/metadata/input1"
        }
    ]
]

export const jsonProvenanceResultTree1 = {
    "analysed_data2": {
        "referenceType": "MetaStore URI",
        "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data2",
        "type": "analysed data",
        "parents": [
            {
                "analysed_data1": {
                    "referenceType": "MetaStore URI",
                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1",
                    "type": "analysed data",
                    "parents": [
                        {
                            "raw_data1": {
                                "referenceType": "MetaStore URI",
                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                "type": "raw data",
                                "parents": [
                                    {
                                        "sample1": {
                                            "referenceType": "MetaStore URI",
                                            "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                            "type": "sample",
                                            "parents": [
                                                {
                                                    "precursor1": {
                                                        "referenceType": "MetaStore URI",
                                                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                        "type": "precursor",
                                                        "parents": [
                                                            {
                                                                "input1": {
                                                                    "referenceType": "MetaStore URI",
                                                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                                    "type": "input"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    "precursor2": {
                                                        "referenceType": "MetaStore URI",
                                                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                        "type": "precursor",
                                                        "parents": [
                                                            {
                                                                "input2": {
                                                                    "referenceType": "MetaStore URI",
                                                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/input2",
                                                                    "type": "input"
                                                                }
                                                            },
                                                            {
                                                                "input3": {
                                                                    "referenceType": "MetaStore URI",
                                                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/input3",
                                                                    "type": "input"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        "sample2": {
                                            "referenceType": "MetaStore URI",
                                            "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample2",
                                            "type": "sample",
                                            "parents": [
                                                {
                                                    "precursor3": {
                                                        "referenceType": "MetaStore URI",
                                                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor3",
                                                        "type": "precursor",
                                                        "parents": [
                                                            {
                                                                "input4": {
                                                                    "referenceType": "MetaStore URI",
                                                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/input4",
                                                                    "type": "input"
                                                                }
                                                            },
                                                            {
                                                                "input5": {
                                                                    "referenceType": "MetaStore URI",
                                                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/input5",
                                                                    "type": "input"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]
    },
    "proposalIds": []
}

export const jsonProvenanceResultTree2 = {
    "analysed_data8": {
      "referenceType": "MetaStore URI",
      "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data8",
      "type": "analysed data",
      "parents": [
        {
          "analysed_data5": {
            "referenceType": "MetaStore URI",
            "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data5",
            "type": "analysed data",
            "parents": [
              {
                "analysed_data4": {
                  "referenceType": "MetaStore URI",
                  "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4",
                  "type": "analysed data",
                  "parents": [
                    {
                      "analysed_data3": {
                        "referenceType": "MetaStore URI",
                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data3",
                        "type": "analysed data",
                        "parents": [
                          {
                            "analysed_data1": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          },
                          {
                            "analysed_data2": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data2",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          "analysed_data6": {
            "referenceType": "MetaStore URI",
            "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data6",
            "type": "analysed data",
            "parents": [
              {
                "analysed_data4": {
                  "referenceType": "MetaStore URI",
                  "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4",
                  "type": "analysed data",
                  "parents": [
                    {
                      "analysed_data3": {
                        "referenceType": "MetaStore URI",
                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data3",
                        "type": "analysed data",
                        "parents": [
                          {
                            "analysed_data1": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          },
                          {
                            "analysed_data2": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data2",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          "analysed_data7": {
            "referenceType": "MetaStore URI",
            "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data7",
            "type": "analysed data",
            "parents": [
              {
                "analysed_data4": {
                  "referenceType": "MetaStore URI",
                  "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data4",
                  "type": "analysed data",
                  "parents": [
                    {
                      "analysed_data3": {
                        "referenceType": "MetaStore URI",
                        "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data3",
                        "type": "analysed data",
                        "parents": [
                          {
                            "analysed_data1": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data1",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          },
                          {
                            "analysed_data2": {
                              "referenceType": "MetaStore URI",
                              "reference": "https://metarepo.nffa.eu/api/v1/metadata/analysed_data2",
                              "type": "analysed data",
                              "parents": [
                                {
                                  "raw_data1": {
                                    "referenceType": "MetaStore URI",
                                    "reference": "https://metarepo.nffa.eu/api/v1/metadata/raw_data1",
                                    "type": "raw data",
                                    "parents": [
                                      {
                                        "sample1": {
                                          "referenceType": "MetaStore URI",
                                          "reference": "https://metarepo.nffa.eu/api/v1/metadata/sample1",
                                          "type": "sample",
                                          "parents": [
                                            {
                                              "precursor1": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor1",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            },
                                            {
                                              "precursor2": {
                                                "referenceType": "MetaStore URI",
                                                "reference": "https://metarepo.nffa.eu/api/v1/metadata/precursor2",
                                                "type": "precursor",
                                                "parents": [
                                                  {
                                                    "input1": {
                                                      "referenceType": "MetaStore URI",
                                                      "reference": "https://metarepo.nffa.eu/api/v1/metadata/input1",
                                                      "type": "input"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              ]
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      ]
    },
    "proposalIds": []
  }