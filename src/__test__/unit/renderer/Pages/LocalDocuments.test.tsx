
// Define it here to avoid: `ReferenceError: Cannot access 'dummySchema' before initialization`
// An alternative could be to define it in an external file and import it
const dummySchema = {
    "type": "object", 
    "properties": {
        "study": {
            "type": "object", 
            "properties": {
                "studyID": {"type": "string"}, 
                "studyTitle": {"type": "string"}, 
                "series": {
                    "type": "array", 
                    "items": {
                        "type": "object", 
                        "properties": {
                            "seriesID": {"type": "string"}, 
                            "seriesTitle": {"type": "string"}, 
                            "sequenceProtocol": {
                                "type": "object", 
                                "properties": {"sequenceProtocolName": {"type": "string"} }
                            }
                        }
                    }
                }
            }
        }
    }
}
import LocalDocuments from "renderer/components/Pages/LocalDocuments";
import {fireEvent, render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import {MemoryRouter} from 'react-router-dom'
import userEvent from "@testing-library/user-event";
import '__test__/test-utils/matchMedia.mock'


// Mock getSchemaFromMetaStore while keeping the other utils functions
jest.mock('renderer/utils/utils', () => {
    const original = jest.requireActual('renderer/utils/utils');
    return {
      ...original,
      getSchemaFromMetaStore: jest.fn().mockResolvedValue(dummySchema),
    };
  });


describe('Render LocalDocuments page', () => {
    afterEach(() => window.sessionStorage.clear())

    it('should render without crash',()=>{
        render(<LocalDocuments />, {wrapper: MemoryRouter})

        expect(screen.getByText('Load .jme file')).toBeInTheDocument()
    })

    it('load a .jme file', async ()=>{
        render(<LocalDocuments />, {wrapper: MemoryRouter})

        // At rendering useEffect triggers the input element used to load files
        const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
        expect(fileUpload.name).toBe('jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // .jme uploading
        const jmeFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"},"_metaStoreSchema":{"schemaLabel":"no label","schemaId":"dummy_schema","schemaVersion":"2"}}
        const file = new File([JSON.stringify(jmeFile)], 'jmetest.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,file)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(file)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest.jme');
        expect(fileUpload.files![0].type).toBe('application/json');
        // The upload file should be rendered correctly
        expect(await screen.findByText('Study ID')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
        expect(await screen.findByText('Study Title')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();
    }, 10000)

    it('load a not valid .jme file', async ()=>{
        // Mock alert window
        window.alert = jest.fn()

        render(<LocalDocuments />, {wrapper: MemoryRouter})

        // At rendering useEffect triggers the input element used to load files
        const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
        expect(fileUpload.name).toBe('jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // wrong formed .jme uploading
        const jmeFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"}}
        const file = new File([JSON.stringify(jmeFile)], 'jmetest.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,file)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(file)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest.jme');
        expect(fileUpload.files![0].type).toBe('application/json');
        // An alert should be rendered displaying an error
        expect(window.alert).toHaveBeenCalledTimes(1)
        expect(window.alert).toHaveBeenCalledWith('Invalid .jme file (missing informations on schema Label, ID and Version)')
    }, 10000)

    it('load a .json document after .jme', async ()=>{
        render(<LocalDocuments />, {wrapper: MemoryRouter})

        // At rendering useEffect triggers the input element used to load files
        const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
        expect(fileUpload.name).toBe('jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // .jme uploading
        const jmeFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"},"_metaStoreSchema":{"schemaLabel":"no label","schemaId":"dummy_schema","schemaVersion":"2"}}
        const file = new File([JSON.stringify(jmeFile)], 'jmetest.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,file)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(file)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest.jme');
        expect(fileUpload.files![0].type).toBe('application/json');
        // The upload file should be rendered correctly
        expect(await screen.findByText('Study ID')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
        expect(await screen.findByText('Study Title')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();


        // Json load button click
        const jsonButton = screen.getByRole('button', {name: 'Load JSON document'})
        fireEvent.click(jsonButton)
        expect(fileUpload).toHaveAttribute('name', 'document')
        expect(fileUpload.accept).toBe('.json')

        // .json uploading
        const jsonFile = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"}}
        const jfile = new File([JSON.stringify(jsonFile)], 'jsontest.json', {type: 'application/json'})
        await userEvent.upload(fileUpload,jfile)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(jfile)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jsontest.json');
        expect(fileUpload.files![0].type).toBe('application/json');

        // Assert that the new json is displayed
        expect(await screen.findByDisplayValue('fake_id')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('fake_study_title')).toBeInTheDocument();
        // Assert that the old file is not displayed anymore
        expect(screen.queryAllByDisplayValue('test_id')).toHaveLength(0)
        expect(screen.queryAllByDisplayValue('test_study_title')).toHaveLength(0)
    }, 10000)

    it('merge a .json document after .jme', async ()=>{
        // Here I do not make assertion on rendering after merge since it requires to do an api request to /compile endpoint
        // I make only assertion on the correct file loading and input properties
        // Mock fetch to avoid ERRCONNECTION error
        const mockResponse = {
            text: jest.fn().mockResolvedValue('Mocked response data'),
            // You can add other properties like `json`, `status`, etc., if your function uses them
        };
        global.fetch = jest.fn().mockResolvedValue(mockResponse);

        render(<LocalDocuments />, {wrapper: MemoryRouter})

        // At rendering useEffect triggers the input element used to load files
        const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
        expect(fileUpload.name).toBe('jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // .jme uploading
        const jmeFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"},"_metaStoreSchema":{"schemaLabel":"no label","schemaId":"dummy_schema","schemaVersion":"2"}}
        const file = new File([JSON.stringify(jmeFile)], 'jmetest.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,file)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(file)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest.jme');
        expect(fileUpload.files![0].type).toBe('application/json');
        // The upload file should be rendered correctly
        expect(await screen.findByText('Study ID')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
        expect(await screen.findByText('Study Title')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();


        // Enter in lock mode
        const lockButton = screen.getByRole('button', {name: 'Enable fields locking'})
        fireEvent.click(lockButton)

        // Merge button click
        const jsonButton = screen.getByRole('button', {name: 'Merge JSON document'})
        fireEvent.click(jsonButton)
        expect(fileUpload).toHaveAttribute('name', 'merge')
        expect(fileUpload.accept).toBe('.json')

        // .json uploading
        const jsonFile = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"}}
        const jfile = new File([JSON.stringify(jsonFile)], 'jsontest.json', {type: 'application/json'})
        await userEvent.upload(fileUpload,jfile)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(jfile)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jsontest.json');
        expect(fileUpload.files![0].type).toBe('application/json');


    }, 10000)

    it('load 2 consecutive .jme files', async ()=>{
        render(<LocalDocuments />, {wrapper: MemoryRouter})

        // At rendering useEffect triggers the input element used to load files
        const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
        expect(fileUpload.name).toBe('jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // .jme uploading
        const jmeFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"},"_metaStoreSchema":{"schemaLabel":"no label","schemaId":"dummy_schema","schemaVersion":"2"}}
        const file = new File([JSON.stringify(jmeFile)], 'jmetest.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,file)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(file)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest.jme');
        expect(fileUpload.files![0].type).toBe('application/json');
        // The upload file should be rendered correctly
        expect(await screen.findByText('Study ID')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
        expect(await screen.findByText('Study Title')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();


        // Schema upload button click
        const jsonButton = screen.getByRole('button', {name: 'Load .jme file'})
        fireEvent.click(jsonButton)
        expect(fileUpload).toHaveAttribute('name', 'jmeDocument')
        expect(fileUpload.accept).toBe('.jme')

        // .json uploading
        const jmeFile1 = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"},"_metaStoreSchema":{"schemaLabel":"no label","schemaId":"dummy_schema","schemaVersion":"2"}}
        const jfile = new File([JSON.stringify(jmeFile1)], 'jmetest2.jme', {type: 'application/json'})
        await userEvent.upload(fileUpload,jfile)

        // Assertion on file uploading
        expect(fileUpload.files![0]).toBe(jfile)
        expect(fileUpload.files!.length).toBe(1);
        expect(fileUpload.files![0].name).toBe('jmetest2.jme');
        expect(fileUpload.files![0].type).toBe('application/json');

        // Assert that the new json is displayed
        expect(await screen.findByDisplayValue('fake_id')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('fake_study_title')).toBeInTheDocument();
        // Assert that the old file is not displayed anymore
        expect(screen.queryAllByDisplayValue('test_id')).toHaveLength(0)
        expect(screen.queryAllByDisplayValue('test_study_title')).toHaveLength(0)
    }, 10000)

    it('Schema and document already present in sessionStorage',async ()=>{
        sessionStorage.setItem('schemaMode', 'localDocument')
        sessionStorage.setItem('schemaId','dummy_schema')
        sessionStorage.setItem('schemaVersion','2')
        const jsonFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"}}
        sessionStorage.setItem('compiledDocument', JSON.stringify(jsonFile))
        sessionStorage.setItem('schema', JSON.stringify(dummySchema))
        render(<LocalDocuments />, {wrapper: MemoryRouter})
        expect(await screen.findByText('Study ID')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
        expect(await screen.findByText('Study Title')).toBeInTheDocument();
        expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();
    })
})