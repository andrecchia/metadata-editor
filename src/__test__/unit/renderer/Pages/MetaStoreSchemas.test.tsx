const dummySchema = {
  "type": "object", 
  "properties": {
      "study": {
          "type": "object", 
          "properties": {
              "studyID": {"type": "string"}, 
              "studyTitle": {"type": "string"}, 
              "series": {
                  "type": "array", 
                  "items": {
                      "type": "object", 
                      "properties": {
                          "seriesID": {"type": "string"}, 
                          "seriesTitle": {"type": "string"}, 
                          "sequenceProtocol": {
                              "type": "object", 
                              "properties": {"sequenceProtocolName": {"type": "string"} }
                          }
                      }
                  }
              }
          }
      }
  }
}
const schemaLabel = ["label1", "label2", "label3"]
const schemaIdLabel = {
  "label1": ["schema11", "schema12"],
  "label2": ["schema21", "schema22", "schema23"],
  "label3": ["schema31"]
}
const schemaLastVersion = {
  "schema11": 1,
  "schema12": 4,
  "schema21": 6,
  "schema22": 2,
  "schema23": 1,
  "schema31": 5
}
import { fireEvent, render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import { MetaStoreSchemas } from 'renderer/components/Pages/MetaStoreSchemas'
import {BrowserRouter, MemoryRouter} from 'react-router-dom'
import '__test__/test-utils/matchMedia.mock'
import userEvent from '@testing-library/user-event'

jest.mock('renderer/utils/utils', () => {
  const original = jest.requireActual('renderer/utils/utils');
    return {
      ...original,
      getSchemaFromMetaStore: jest.fn().mockResolvedValue(dummySchema),
      scanSchemasFromMetaStore: jest.fn().mockResolvedValue({
        schemaLabel: schemaLabel,
        schemaIdLabel: schemaIdLabel,
        schemaLastVersion: schemaLastVersion
      })
    };
});

const selectDropdown = async (button:HTMLElement, toSelect: number) => {
  // Open dropdown menu
  fireEvent.mouseDown(button)
  // These are the options in the dropdown
  const options = screen.getAllByRole('option');
  fireEvent.click(options[toSelect]);
};

describe('Render /MetaStoreSchemas', () => {

  afterEach(() => window.sessionStorage.clear())

  it('renders loading message and then the schema selection',async ()=>{
    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})

    const logo = screen.getByRole('img');
    expect(logo).toHaveAttribute('src', 'test-file-stub');
    // expect(logo).toHaveAttribute('src', 'metadataEditorLogo.png');
    expect(screen.getByText('Loading...')).toBeInTheDocument()
    
    // Wait for the scanSchemasFromMetaStore function to complete and update the state
    await screen.findByText('Load Schema');
    // Check that the schema selection is displayed
    expect(screen.queryByText('Loading...')).not.toBeInTheDocument();
    expect(screen.getAllByText('Schema ID')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Version')[0]).toBeInTheDocument();
    expect(screen.getByText('Load Schema')).toBeInTheDocument();
    expect(screen.queryByText('Load JSON document')).not.toBeInTheDocument();
    expect(screen.queryByText('Merge JSON document')).not.toBeInTheDocument();
  });

  it('alert if try to load schema but no id/version are selected', async ()=>{
    global.alert = jest.fn();
    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})
    // Wait for the scanSchemasFromMetaStore function to complete and update the state
    await screen.findByText('Load Schema');

    // Load schema without having selected any label, id, version
    const loadButton = screen.getByRole('button',{name: 'Load Schema'})
    fireEvent.click(loadButton)

    expect(global.alert).toHaveBeenCalledWith('Choose a schema ID and version')
  })

  it('select and render a schema', async () =>{
    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})

    // Wait for the scanSchemasFromMetaStore function to complete and update the state
    await screen.findByText('Load Schema');

    const labelButton = screen.getByRole('button',{name: /Label/i})
    const schemaIDButton = screen.getByRole('button',{name: /Schema ID/i})
    // Select 2nd label and 3rd schema id
    selectDropdown(labelButton,1)
    selectDropdown(schemaIDButton,2)
    expect(screen.getByRole('button',{name: `Label ${schemaLabel[1]}`}).innerHTML).toBe(schemaLabel[1])

    // Load schema
    const loadButton = screen.getByRole('button',{name: 'Load Schema'})
    fireEvent.click(loadButton)

    // Wait for the getSchemaFromMetaStore function to complete 
    await screen.findByText('Load JSON document');
    expect(await screen.findByText('Study ID')).toBeInTheDocument();
    expect(await screen.findByText('Study Title')).toBeInTheDocument();
    expect(sessionStorage.getItem("schemaLabel")).toBe(schemaLabel[1])
    expect(sessionStorage.getItem("schemaId")).toBe(schemaIdLabel[schemaLabel[1]][2])
  })

  it('render a schema, then load a document', async () =>{
    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})

    // Wait for the scanSchemasFromMetaStore function to complete and update the state
    await screen.findByText('Load Schema');

    const labelButton = screen.getByRole('button',{name: /Label/i})
    const schemaIDButton = screen.getByRole('button',{name: /Schema ID/i})
    // Select 2nd label and 3rd schema id
    selectDropdown(labelButton,1)
    selectDropdown(schemaIDButton,2)
    expect(screen.getByRole('button',{name: `Label ${schemaLabel[1]}`}).innerHTML).toBe(schemaLabel[1])

    // Load schema
    const loadButton = screen.getByRole('button',{name: 'Load Schema'})
    fireEvent.click(loadButton)

    // Wait for the getSchemaFromMetaStore function to complete 
    await screen.findByText('Load JSON document');

    // Json load button click
    const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
    const jsonButton = screen.getByRole('button', {name: 'Load JSON document'})
    fireEvent.click(jsonButton)
    expect(fileUpload).toHaveAttribute('name', 'document')
    expect(fileUpload.accept).toBe('.json')

    // .json uploading
    const jsonFile = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"}}
    const jfile = new File([JSON.stringify(jsonFile)], 'jsontest.json', {type: 'application/json'})
    await userEvent.upload(fileUpload,jfile)

    // Assert that the new json is displayed
    expect(await screen.findByDisplayValue('fake_id')).toBeInTheDocument();
    expect(await screen.findByDisplayValue('fake_study_title')).toBeInTheDocument();
  })

  it('render a schema, then merge a document', async () =>{
    // Here I do not make assertion on rendering after merge since it requires to do an api request to /compile endpoint
    // I make only assertion on the correct file loading and input properties
    // Mock fetch to avoid ERRCONNECTION error
    const mockResponse = {
      text: jest.fn().mockResolvedValue('Mocked response data'),
    };
    global.fetch = jest.fn().mockResolvedValue(mockResponse);

    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})

    // Wait for the scanSchemasFromMetaStore function to complete and update the state
    await screen.findByText('Load Schema');

    const labelButton = screen.getByRole('button',{name: /Label/i})
    const schemaIDButton = screen.getByRole('button',{name: /Schema ID/i})
    // Select 2nd label and 3rd schema id
    selectDropdown(labelButton,1)
    selectDropdown(schemaIDButton,2)
    expect(screen.getByRole('button',{name: `Label ${schemaLabel[1]}`}).innerHTML).toBe(schemaLabel[1])

    // Load schema
    const loadButton = screen.getByRole('button',{name: 'Load Schema'})
    fireEvent.click(loadButton)

    // Wait for the getSchemaFromMetaStore function to complete 
    await screen.findByText('Load JSON document');

    // Enter in lock mode
    const lockButton = screen.getByRole('button', {name: 'Enable fields locking'})
    fireEvent.click(lockButton)

    // Merge button click
    const jsonButton = screen.getByRole('button', {name: 'Merge JSON document'})
    fireEvent.click(jsonButton)
    const fileUpload = (screen.getByTestId('fileUpload') as HTMLInputElement)
    expect(fileUpload).toHaveAttribute('name', 'merge')
    expect(fileUpload.accept).toBe('.json')

    // .json uploading
    const jsonFile = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"}}
    const jfile = new File([JSON.stringify(jsonFile)], 'jsontest.json', {type: 'application/json'})
    await userEvent.upload(fileUpload,jfile)

    // Assertion on file uploading
    expect(fileUpload.files![0]).toBe(jfile)
    expect(fileUpload.files!.length).toBe(1);
    expect(fileUpload.files![0].name).toBe('jsontest.json');
    expect(fileUpload.files![0].type).toBe('application/json');
  })

  it('Schema and document already present in sessionStorage',async ()=>{
    sessionStorage.setItem('schemaMode', 'MetaStore')
    sessionStorage.setItem('schemaId','dummy_schema')
    sessionStorage.setItem('schemaLabel','some_label')
    sessionStorage.setItem('schemaVersion','2')
    const jsonFile = {"study":{"studyID":"test_id","series":[{"seriesID":"test_series_id"}],"studyTitle":"test_study_title"}}
    sessionStorage.setItem('compiledDocument', JSON.stringify(jsonFile))
    sessionStorage.setItem('schema', JSON.stringify(dummySchema))
    render(<MetaStoreSchemas />, {wrapper: BrowserRouter})
    expect(await screen.findByText('Study ID')).toBeInTheDocument();
    expect(await screen.findByDisplayValue('test_id')).toBeInTheDocument();
    expect(await screen.findByText('Study Title')).toBeInTheDocument();
    expect(await screen.findByDisplayValue('test_study_title')).toBeInTheDocument();
  })

})
