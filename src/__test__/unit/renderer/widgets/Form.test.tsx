import { render, screen } from '@testing-library/react';
import Form from 'renderer/components/widgets/Form';
import '__test__/test-utils/matchMedia.mock'

describe('Form', () => {
  it('renders LocalDocumentButton and ShowForm when schema is defined', async () => {
    const schema = {
        "type": "object", 
        "properties": {
            "study": {
                "type": "object", 
                "properties": {
                    "studyID": {"type": "string"}, 
                    "studyTitle": {"type": "string"}, 
                    "series": {
                        "type": "array", 
                        "items": {
                            "type": "object", 
                            "properties": {
                                "seriesID": {"type": "string"}, 
                                "seriesTitle": {"type": "string"}, 
                                "sequenceProtocol": {
                                    "type": "object", 
                                    "properties": {"sequenceProtocolName": {"type": "string"} }
                                }
                            }
                        }
                    }
                }
            }
        }
      };
    const data = {"study":{"studyID":"fake_id","series":[{"seriesID":"fake_series_id"}],"studyTitle":"fake_study_title"}};
    const inputElement = document.createElement('input');
    
    render(<Form schema={schema} inputElement={inputElement} data={data} />);

    // Assert that ShowForm and Load JSON are rendered
    expect(await screen.findByText('Load JSON document')).toBeInTheDocument();
    expect(await screen.findByText('Study ID')).toBeInTheDocument();
    expect(await screen.findByText('Study Title')).toBeInTheDocument();
    expect(await screen.findByDisplayValue('fake_id')).toBeInTheDocument();
    expect(await screen.findByDisplayValue('fake_study_title')).toBeInTheDocument();
  });
});
