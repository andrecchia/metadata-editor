import { render, fireEvent } from '@testing-library/react';
import { getJsonDocumentPaths } from 'renderer/utils/utils';
import { HandleFieldsCheckProvider } from 'renderer/components/CustomRenderers/util/lockedUtils';
import { LockFieldsButtonsToolbar } from 'renderer/components/widgets/LockFieldsButtonsToolbar';

jest.mock('renderer/utils/utils',()=>{
  const original = jest.requireActual('renderer/utils/utils')
  return {
    ...original,
    getJsonDocumentPaths: jest.fn().mockReturnValue({field1: true})
  }
})

const LockFieldsButtonsToolbarProvided = ({data, setData}) =>{
  return(
  <HandleFieldsCheckProvider>
    <LockFieldsButtonsToolbar data={data} setData={setData} />
  </HandleFieldsCheckProvider>
  );
}



describe('LockFieldsButtonsToolbar', () => {
  const setDataMock = jest.fn();

  beforeEach(() => {
    setDataMock.mockClear();
  });

  it('renders the component', () => {
    const { container } = render(<LockFieldsButtonsToolbarProvided data={{}} setData={setDataMock} />);
    expect(container.firstChild).toBeInTheDocument();
  });

  it('displays lock button', () => {
    const { getByRole } = render(<LockFieldsButtonsToolbarProvided data={{}} setData={setDataMock} />);
    const lockButton = getByRole('button', { name: 'Enable fields locking' });
    expect(lockButton).toBeInTheDocument();
  });

  it('toggles lock button on click', () => {
    const { getByRole } = render(<LockFieldsButtonsToolbarProvided data={{}} setData={setDataMock} />);
    const lockButton = getByRole('button', { name: 'Enable fields locking' });

    fireEvent.click(lockButton);

    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(lockButton).toHaveAttribute('aria-label', 'Disable fields locking');

    fireEvent.click(lockButton);

    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(lockButton).toHaveAttribute('aria-label', 'Enable fields locking');
  });

  it('displays clear button and hides fields button when lock button is clicked', () => {
    const { getByRole, queryByRole } = render(<LockFieldsButtonsToolbarProvided data={{}} setData={setDataMock} />);
    const lockButton = getByRole('button', { name: 'Enable fields locking' });

    fireEvent.click(lockButton);

    const clearButton = getByRole('button', { name: 'Clear checked fields' });
    const hideFieldsButton = getByRole('button', { name: 'Hide unchecked fields' });

    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(clearButton).toBeInTheDocument();
    expect(hideFieldsButton).toBeInTheDocument();
    expect(queryByRole('button', { name: 'Enable fields locking' })).toBeNull();

    fireEvent.click(lockButton);
    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(clearButton).not.toBeInTheDocument();
    expect(hideFieldsButton).not.toBeInTheDocument();
  });

  it('clears checked fields when clear button is clicked', () => {
    const { getByRole } = render(<LockFieldsButtonsToolbarProvided data={{field1: 'test'}} setData={setDataMock} />);
    const lockButton = getByRole('button', { name: 'Enable fields locking' });

    fireEvent.click(lockButton);

    const clearButton = getByRole('button', { name: 'Clear checked fields' });
    fireEvent.click(clearButton);

    expect(setDataMock).toHaveBeenCalledTimes(1);
    expect(setDataMock).toHaveBeenNthCalledWith(1, expect.any(Function));
  });

  it('toggle hide button on click', () => {
    const { getByRole } = render(<LockFieldsButtonsToolbarProvided data={{}} setData={setDataMock} />);
    const lockButton = getByRole('button', { name: 'Enable fields locking' });

    fireEvent.click(lockButton);

    const hideFieldsButton = getByRole('button', { name: 'Hide unchecked fields' });
    fireEvent.click(hideFieldsButton);

    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(hideFieldsButton).toHaveAttribute('aria-label', 'Show hidden fields');

    fireEvent.click(hideFieldsButton);

    expect(setDataMock).toHaveBeenCalledTimes(0);
    expect(hideFieldsButton).toHaveAttribute('aria-label', 'Hide unchecked fields');

  });
});