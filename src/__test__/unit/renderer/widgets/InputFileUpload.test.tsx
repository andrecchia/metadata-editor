import { render, fireEvent } from '@testing-library/react';
import { InputFileUpload } from 'renderer/components/widgets/InputFileUpload';


describe('InputFileUpload', () => {
  it('triggers handleOnChange when a file is selected', () => {
    const handleOnChange = jest.fn();
    const { getByTestId } = render(<InputFileUpload handleOnChange={handleOnChange} />);
    const input = getByTestId('fileUpload');

    const file = new File(['test file contents'], 'test.txt', { type: 'text/plain' });
    fireEvent.change(input, { target: { files: [file] } });

    expect(handleOnChange).toHaveBeenCalledTimes(1);
    expect(handleOnChange).toHaveBeenCalledWith(expect.objectContaining({
      target: expect.objectContaining({
        files: [file]
      })
    }));
  });
});
