import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SelectSchemaProperties, Dropdown, SpinButton } from 'renderer/components/widgets/selectSchemaProperties';
import { useState } from 'react';

const schemaLabel = [
    "measurement",
    "input",
    "analysis",
    "precursor",
    "sample"
]
const schemaIdLabel= {
    "measurement": [
        "SEM_schema",
        "datacite",
        "dummy_schema",
        "mri_schema"
    ],
    "input": [
        "input_schema",
        "nep_proposal"
    ],
    "analysis": [
        "mldata_basic_schema",
        "mri_ml"
    ],
    "precursor": [
        "precursor_schema"
    ],
    "sample": [
        "sample_schema"
    ]
}
const schemaLastVersion = {
    "SEM_schema": 1,
    "datacite": 1,
    "dummy_schema": 2,
    "input_schema": 1,
    "mldata_basic_schema": 2,
    "mri_ml": 1,
    "mri_schema": 8,
    "nep_proposal": 2,
    "precursor_schema": 1,
    "sample_schema": 1
}

describe('SelectSchemaProperties', () => {


    it('should render without crashing', () => {
        render(<SelectSchemaProperties 
                    schemaLabel={schemaLabel}
                    schemaIdLabel={schemaIdLabel}
                    schemaLastVersion={schemaLastVersion}
                    value={undefined}
                    setValue={jest.fn()}
                />);
    });


    it('passing undefined in value the selections should be empty and/or deactivated', () => {
        render(<SelectSchemaProperties 
                    schemaLabel={schemaLabel}
                    schemaIdLabel={schemaIdLabel}
                    schemaLastVersion={schemaLastVersion}
                    value={undefined}
                    setValue={jest.fn()}
                />);
        const dropdownLabel = screen.getByLabelText('Label');
        const dropdownId = screen.getByLabelText('Schema ID');
        const spinButton = screen.getByLabelText('Version') as HTMLInputElement;

        // Ensure Label element, Schema ID and Version has nothing selected
        //This regular expression pattern checks if there is an empty string (^$) or a zero-width space character (\u200B).
        expect(dropdownLabel.textContent).toMatch(/^$|\u200B/);
        expect(dropdownId.textContent).toMatch(/^$|\u200B/);
        // Empty value menas 0 for Version
        expect(spinButton.value).toBe("0");
        
        // Ensure Schema ID and Version elements are disabled
        expect(dropdownId).toHaveAttribute('aria-disabled');
        expect(spinButton.disabled).toBe(true);
    });

    it('choose a label from dropdown menu', () => {
        const setValue = jest.fn();
        render(<SelectSchemaProperties 
                    schemaLabel={schemaLabel}
                    schemaIdLabel={schemaIdLabel}
                    schemaLastVersion={schemaLastVersion}
                    value={undefined}
                    setValue={setValue}
                />);
        const dropdownLabel = screen.getByLabelText('Label');

        // open the dropdown menu
        fireEvent.mouseDown(dropdownLabel);

        //click the elements and see if setValue is triggered
        const options = screen.getAllByRole('option');
        for (const label of schemaLabel){
            expect(screen.getByText(label)).toBeInTheDocument();
        }
        for (let i=0; i<options.length; ++i ){
            act(() => {
                options[i].click();
                });
            expect(dropdownLabel).toHaveFocus();
            // Check that id and version are reset, and that the label is set to the expected value
            expect(setValue).toHaveBeenCalledWith({id: "", label: schemaLabel[i], version: -1});
        }
        expect(setValue).toHaveBeenCalledTimes(schemaLabel.length)
    });

    it('choose an ID from dropdown menu', () => {
        const setValue = jest.fn();
        const value = {label:'measurement', id:'', version:-1}
        render(<SelectSchemaProperties 
                    schemaLabel={schemaLabel}
                    schemaIdLabel={schemaIdLabel}
                    schemaLastVersion={schemaLastVersion}
                    value={value}
                    setValue={setValue}
                />);
        const dropdownId = screen.getByLabelText('Schema ID');

        // open the dropdown menu
        fireEvent.mouseDown(dropdownId);

        //click the elements and see if setValue is triggered
        const options = screen.getAllByRole('option');
        for (const id of schemaIdLabel[value.label]){
            expect(screen.getByText(id)).toBeInTheDocument();
        }
        for (let i=0; i<options.length; ++i ){
            act(() => {
                options[i].click();
                });
            expect(dropdownId).toHaveFocus();
            // Check that id is set to the expected value, version to the most recent, and that the label is not changed
            const newIdValue = schemaIdLabel[value.label][i]
            expect(setValue).toHaveBeenCalledWith({id: newIdValue, label: value.label, version: schemaLastVersion[newIdValue]});
        }
        expect(setValue).toHaveBeenCalledTimes(schemaIdLabel[value.label].length)
    });

    it('choose a version from spinbutton', () => {
        const setValue = jest.fn();
        const value = {label:'measurement', id:'mri_schema', version: 8}
        render(<SelectSchemaProperties 
                    schemaLabel={schemaLabel}
                    schemaIdLabel={schemaIdLabel}
                    schemaLastVersion={schemaLastVersion}
                    value={value}
                    setValue={setValue}
                />);
        const spinButton = screen.getByLabelText('Version') as HTMLInputElement;

        fireEvent.change(spinButton, { target: { value: 7 } });
        expect(setValue).toHaveBeenCalledWith({id: value.id, label: value.label, version: "7"});
    });
});

describe('Dropdown', () => {
    it('should render the correct number and elements of menu items', () => {
        const { getByRole, getByText, getAllByRole } = render(<Dropdown
            id={'selectSchemaLabel'}
            value={''}
            fieldName={'Label'}
            property={schemaLabel}
            handleChange={jest.fn()}
            disabled={false}
            />)

        //open the dropdown menu
        const trigger = getByRole('button');
        fireEvent.mouseDown(trigger);

        //check that has the correct lenght and elements
        const options = getAllByRole('option');
        expect(options).toHaveLength(schemaLabel.length);
        expect(options[0]).toHaveFocus();
        for (const label of schemaLabel){
            expect(getByText(label)).toBeInTheDocument();
        }
    })

    it('should call handleChange when a schema label is selected', () => {
        const handleChange = jest.fn()
        const { getByRole, getAllByRole } = render(<Dropdown
                id={'selectSchemaLabel'}
                value={''}
                fieldName={'Label'}
                property={schemaLabel}
                handleChange={handleChange}
                disabled={false}
            />)

        //open the dropdown menu
        const trigger = getByRole('button');
        fireEvent.mouseDown(trigger);
        
        // check handleChange calls on selection
        const options = getAllByRole('option');
        for (let i=0; i<options.length; ++i ){
            act(() => {
                options[i].click();
              });
            expect(handleChange).toHaveBeenCalled();  
            expect(trigger).toHaveFocus();
        }
        expect(handleChange).toHaveBeenCalledTimes(schemaLabel.length)
    });

    it('should render value', () => {
        render(<Dropdown
            id={'selectSchemaLabel'}
            value={'measurement'}
            fieldName={'Label'}
            property={schemaLabel}
            handleChange={jest.fn()}
            disabled={false}
            />)
        expect(screen.getByText('measurement')).toBeInTheDocument()
    })

    it('should not render value when it is not in the property list', () => {
        render(<Dropdown
            id={'selectSchemaLabel'}
            value={'TestLabel'}
            fieldName={'Label'}
            property={schemaLabel}
            handleChange={jest.fn()}
            disabled={false}
            />)

        // TestLabel should not be in the document because it is not in schemaLabel 
        expect(screen.queryByText('TestLabel')).not.toBeInTheDocument()
        // In this case the label should be empty
        const dropdownLabel = screen.getByLabelText('Label')
        //This regular expression pattern checks if there is an empty string (^$) or a zero-width space character (\u200B).
        expect(dropdownLabel.textContent).toMatch(/^$|\u200B/);
    })    
})

