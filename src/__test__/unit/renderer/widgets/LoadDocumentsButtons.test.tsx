import { fireEvent, render, screen } from '@testing-library/react';
import { LoadDocumentsButtons } from "renderer/components/widgets/LoadDocumentsButtons";
import { HandleFieldsCheckProvider, useHandleFieldsCheckState } from 'renderer/components/CustomRenderers/util/lockedUtils';

jest.mock('renderer/components/CustomRenderers/util/lockedUtils', () => {
    const original = jest.requireActual('renderer/components/CustomRenderers/util/lockedUtils');
      return {
        ...original,
        useHandleFieldsCheckState: jest.fn().mockReturnValue([
            {
              checkFields: true, // Mocked value for checkFields
              hideUncheckedFields: false, // Mocked value for hideUncheckedFields
            },
            jest.fn(), // Mocked setter function
          ])
      };
  });

describe('LoadDocumentsButtons', ()=>{
    it('should show both buttons', ()=>{
        const inputElement = document.createElement('input');
        render(
            <HandleFieldsCheckProvider>
                <LoadDocumentsButtons isDisabled={false} fileUpload={inputElement} />
            </HandleFieldsCheckProvider>
        )
        expect(screen.getByText('Load JSON document')).toBeInTheDocument();
        expect(screen.getByText('Merge JSON document')).toBeInTheDocument();
    })

    it('should show only load JSON button', ()=>{
        const inputElement = document.createElement('input');
        (useHandleFieldsCheckState as jest.Mock).mockReturnValueOnce([
                {
                  checkFields: false, // Mocked value for checkFields
                  hideUncheckedFields: false, // Mocked value for hideUncheckedFields
                },
                jest.fn(), // Mocked setter function
            ])
        render(
            <HandleFieldsCheckProvider>
                <LoadDocumentsButtons isDisabled={false} fileUpload={inputElement} />
            </HandleFieldsCheckProvider>
        )
        expect(screen.getByText('Load JSON document')).toBeInTheDocument();
        expect(screen.queryByText('Merge JSON document')).not.toBeInTheDocument();
    })

    it('click Load JSON document', ()=>{
        const inputElement = document.createElement('input');
        render(
            <HandleFieldsCheckProvider>
                <LoadDocumentsButtons isDisabled={false} fileUpload={inputElement} />
            </HandleFieldsCheckProvider>
        )
        const loadButton = screen.getByRole('button',{name: 'Load JSON document'})
        fireEvent.click(loadButton)

        expect(inputElement.getAttribute('name')).toBe('document')
        expect(inputElement.getAttribute('accept')).toBe('.json')
        expect(inputElement.value).toBe('')
    })
    it('click Merge JSON document', ()=>{
        const inputElement = document.createElement('input');
        render(
            <HandleFieldsCheckProvider>
                <LoadDocumentsButtons isDisabled={false} fileUpload={inputElement} />
            </HandleFieldsCheckProvider>
        )
        const loadButton = screen.getByRole('button',{name: 'Merge JSON document'})
        fireEvent.click(loadButton)

        expect(inputElement.getAttribute('name')).toBe('merge')
        expect(inputElement.getAttribute('accept')).toBe('.json')
        expect(inputElement.value).toBe('')
    })
})