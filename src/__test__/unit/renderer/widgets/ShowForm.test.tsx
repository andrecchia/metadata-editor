import {render, screen, fireEvent} from '@testing-library/react'
import { ShowForm } from 'renderer/components/widgets/ShowForm';
import { HandleFieldsCheckProvider } from "renderer/components/CustomRenderers/util/lockedUtils";
import MRIschema from '__test__/json_files/metadata_schemas/mri_schema.json'
import MRIdocument from '__test__/json_files/metadata_documents/mri_schema_document.json'
import '__test__/test-utils/matchMedia.mock'
import React from 'react';

const {$schema, ...schema} = MRIschema;

interface ShowFormProps {
  schema: any;
  initialData?: any;
}
const ShowFormProvided = (props: ShowFormProps) =>{
  return(
    <HandleFieldsCheckProvider>
      <ShowForm {...props} />
    </HandleFieldsCheckProvider>
    );
}

describe('ShowForm element test', ()=>{
  test('Render only schema', async () => {
     render(
      <ShowFormProvided schema={schema}/>
    );

    // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
   // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
    await screen.findByText('Study')

    expect(screen.getByText(/Study ID/i)).toBeInTheDocument()
    expect(screen.getByText(/Study Title/i)).toBeInTheDocument()
    expect(screen.getByRole('textbox', { name: 'Study ID' })).toHaveValue('');
    expect(screen.getByRole('textbox', { name: 'Study Title' })).toHaveValue('');
    const lockButton = screen.getByRole('button', { name: 'Enable fields locking' });
    expect(lockButton).toBeInTheDocument();
  });

  test('Render schema and initial data', async () => {
     render(
      <ShowFormProvided schema={schema} initialData={MRIdocument}/>
    );

    // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
   // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
    await screen.findByText('Study')

    expect(screen.getByText(/Study ID/i)).toBeInTheDocument()
    expect(screen.getByText(/Study Title/i)).toBeInTheDocument()
    expect(screen.getByRole('textbox', { name: 'Study ID' })).toHaveValue('provaID');
    expect(screen.getByRole('textbox', { name: 'Study Title' })).toHaveValue('prova Titolo');
    const lockButton = screen.getByRole('button', { name: 'Enable fields locking' });
    expect(lockButton).toBeInTheDocument();
  });

  test('Render only schema, then fill it', async () => {
    render(
     <ShowFormProvided schema={schema}/>
   );

   // Await that page is rendered, useEffect is called and promise inside useEffect is resolved
   // (this avoids the `Warning: An update to Provenance inside a test was not wrapped in act(...)` )
   await screen.findByText('Study')

   expect(screen.getByText(/Study ID/i)).toBeInTheDocument();
   expect(screen.getByText(/Study Title/i)).toBeInTheDocument();
   const studyID=screen.getByRole('textbox', { name: 'Study ID' });
   const studyTitle=screen.getByRole('textbox', { name: 'Study Title' });
   expect(studyID).toHaveValue('');
   expect(studyTitle).toHaveValue('');
   const lockButton = screen.getByRole('button', { name: 'Enable fields locking' });
   expect(lockButton).toBeInTheDocument();

   // Fill schema
   fireEvent.change(studyID, { target: { value: 'test id' } })
   fireEvent.change(studyTitle, { target: { value: 'test title' } })
   expect(studyID).toHaveValue('test id');
   expect(studyTitle).toHaveValue('test title');
 });

})
