import { readFile } from 'fs';

export function readFileAsync(path: string, encoding: BufferEncoding): Promise<string> {
    return new Promise((resolve, reject) => {
      readFile(path, encoding, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }