import { match } from "css-mediaquery";
/**
 * Implementation of matchMedia (see the material-ui testing [paragraph](https://mui.com/material-ui/react-use-media-query/#testing))
 * @param width 
 * @returns MediaQueryList object
 */
export const createMatchMedia = (width: number) => (query: string): MediaQueryList => ({
    matches: match(query, { width }),
    media: query,
    onchange: null,
    addListener: () => jest.fn(),
    removeListener: () => jest.fn(),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn()
  });
  
  window.matchMedia = createMatchMedia(window.innerWidth);