import { Dispatch, SetStateAction } from "react";
import { IAuthContextProps } from "renderer/AuthContext";
import { Auth } from "main/AppAuth-JS/auth";

/**
 * Mock for window.electron.store
 */
global.electron = {
  store: {
    get(key) {
      return ()=>{};
    },
    set(property, val) {
      ()=>{}
    },
    handleElectronStoreChange: (callback) => {},
    removeElectronStoreChangeHandler: () => {}
  },
  auth: {
      signIn: ()=>{
        return new Promise(async (resolve, reject) => {
          try{
            resolve({ authenticated: true, token: 'fakeToken' })
          }catch (e){
            reject({ authenticated: false, token: null })
          }
        })
      },
      signOut: ()=>{},
      authContextListener: (setAuth: Dispatch<SetStateAction<IAuthContextProps>>) => {},
      configOIDC: (config) => {}
    },
  API: {
    requestListener: (callback) => {},
    removeRequestListener: () => {}
  },
  maskHandler:{
    /**
     * Triggers the MaskHandler object initialization
     * @param mask 
     * @param schemaMaskId 
     */
    assignMaskHandler(mask, schemaMaskId:string) {
    },
    getVisibilityState: (path:string) => {
      return new Promise(async (resolve, reject) => {
        try{
          resolve(true)
        }catch (e){
          reject(undefined)
        }
      })
    },
    insertIntoVisibilityState: (mask, pathToInsert: string) => {},
    unsetMaskHandler() { },
  },
  MetaStoreInstance: {
    setInstanceListener: (navigate)=>{},
    removeListener: () => {}
  }
}
