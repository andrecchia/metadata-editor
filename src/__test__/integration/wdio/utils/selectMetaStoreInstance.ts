/**
 * Selects the choosen MetaStore instance, then goes to Home
 * @param MetaStoreName MetaStore instance name. It can also be only part of the name
 * provided that is is unique
 */
export const selectMetaStoreInstance = async (MetaStoreName: string) => {
    const MetaStoreInstanceDropwdown = await browser.$('#MetaStoreInstanceDropdown');
    await MetaStoreInstanceDropwdown.click()

    const localInstance = await browser.$(`li*=${MetaStoreName}`)
    await localInstance.click()

    const home = await browser.$('a=Home')
    await home.click()
}