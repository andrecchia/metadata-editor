import { MenuOptions } from "./config";

/**
 * From home page, it hovers over the element to click and then clicks it
 * @param selectedOption 
 */
export const selectOptionFromHome = async (selectedOption: string) => {
    // Wait for the page to show correctly
    const homeSubtitle = await browser.$('h4=easily edit metadata documents')
    await homeSubtitle.waitForDisplayed()
    // Select the element to hover over
    const elementToHover = await browser.$(`div=${selectedOption}`);
    // Hover over the element
    await elementToHover.moveTo();

    if(selectedOption === MenuOptions.METASTORE_SCHEMAS){
        const buttonToClick = await browser.$('aria/MetaStore_schemas');
        await buttonToClick.waitForClickable()
        // Click on the element to enter the page
        await buttonToClick.click();
    } 
    else if(selectedOption === MenuOptions.LOCAL_DOCUMENTS) {
        const buttonToClick = await browser.$('aria/local_documents');
        await buttonToClick.waitForClickable()
        // Click on the element to enter the page
        await buttonToClick.click();
    }
    else if(selectedOption === MenuOptions.PROVENANCE){
        const buttonToClick = await browser.$('aria/login');
        await buttonToClick.waitForClickable()
        // Click on the element to enter the page
        await buttonToClick.click();
        // Wait that log is written into file...
        await browser.pause(2000)
    }
}