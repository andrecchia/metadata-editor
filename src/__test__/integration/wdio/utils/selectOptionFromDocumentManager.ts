import { DocumentManagerOptions } from "./config";

/**
 * From document manager page, it hovers over the element to click and then clicks it
 * @param selectedOption 
 */
export const selectOptionFromDocumentManager = async (selectedOption: string) => {
    // Select the element to hover over
    const elementToHover = await browser.$(`div=${selectedOption}`);
    // Hover over the element
    await elementToHover.moveTo();
    // await browser.pause(2000)

    if(selectedOption === DocumentManagerOptions.UPLOAD_TO_METASTORE){
        const buttonToClick = await browser.$('aria/login');
        await buttonToClick.waitForClickable()
        // Click on the element to enter the page
        await buttonToClick.click();
        // Wait that log is written into file...
        await browser.pause(2000)
    } 
    else if(selectedOption === DocumentManagerOptions.EXPORT) {
        console.error("Not implemented yet")
    }
    else if(selectedOption === DocumentManagerOptions.SAVE){
        console.error("Not implemented yet")
    }
}