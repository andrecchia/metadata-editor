export class MenuOptions {
    static METASTORE_SCHEMAS = "MetaStore schemas"
    static LOCAL_DOCUMENTS = "Local documents"
    static PROVENANCE = "Provenance"
}

export class DocumentManagerOptions {
    static UPLOAD_TO_METASTORE = "Upload to MetaStore"
    static EXPORT = "Export"
    static SAVE = "Save"
}

export class TestConst {
    /** API ports used for parallel wdio drivers are 4440, 4441, ... */
    static API_PORT_PREFIX = "444"
}

// define bad defined schemas to be skipped in testing
export const BAD_SCHEMAS = [
    'mldata_basic_schema', // Doesn't have 'properties' key
    'sem_fib_tomography_dataset', // Ref to non existent URL
    'sem_fib_tomography_acquisition', // Ref to non existent URL
    // For these schemas JSONSchemaFaker fails to generate good documents 
    'lab_ct', // Strange date/time pattern
    'sem', // Strange date/time pattern
    'sem_fib_tomography_image',
    'tem'
  ]