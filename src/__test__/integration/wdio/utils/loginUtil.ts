import fs from 'fs'

/**
 *
 * Function that performs a login for an authorization flow with PKCE protocol.
 * The workflow in summary is:
 * 1) Authorization flow is started by redirecting the user to the authorization endpoint 
 *    (/realms/{REALM}/protocol/openid-connect/auth) with the required parameters (redirect_uri, client_id, response_type ...).
 *    This step is perfermed before to call this function
 * 2) The authorization server presents the login form to the user.
 * 3) The user enters their credentials and submits the form, the authentication request is sent 
 *    to the /login-actions/authenticate endpoint of the Identity Provider. This endpoint handles the authentication process.
 * 4) Upon successful authentication, the authorization server generates an authorization code and redirects the user back 
 *    to the redirect URI specified in the initial authorization request.
 * 5) The local application running on `redirect_uri` receives the redirect request with the authorization code.
 * 6) The local application extracts the authorization code from the query parameters and may subsequently exchange it for a token
 * 
 * Inspired by https://medium.com/@rishabhsvats/understanding-authorization-code-flow-3946d746407
 * @param wdioWorkerID 
 * @param assertionCallback 
 */
export const doLogin = async (wdioWorkerID: string, assertionCallback?: () => void) => {
    // Step #1 performed before this function

    // In testing code challenge, code verifier and 
    // authorization URL (realms/NEP/protocol/openid-connect/auth?redirect_uri..) are logged out into a file
    const output = fs.readFileSync(`${__dirname}/../logs/wdio-${wdioWorkerID}-chromedriver.log`, 'utf8');
    // Get the URL from the file content
    const urlRegex = process.env.DEBUG_PROD ? 
        /http:\/\/test-keycloak:8080\/.*?S256/ :
        /https:\/\/auth\.nffa\.eu\/.*?S256/
    const loginURL = output.match(urlRegex)![0]
    
    let cookie = ''
    let authenticateURL = ''

    const response = await fetch(loginURL, {
        method: 'GET',
        headers: {
            'Cookie': cookie
        }
    });

    // Extracting and storing the cookie from the response headers
    cookie = response.headers.get('set-cookie') as string

    // The response is an html login form ( step #2 )
    const loginForm = await response.text()
    // Get the authentication URL from the html
    const formIdRegex = /<form id="kc-form-login".*?method="post">/
    const formString = loginForm.match(formIdRegex)![0]
    authenticateURL = formString.match(/action="(.*?)"/)![1].replaceAll('&amp;', '&')

    // Define username and password to be passed in the authentication URL
    const formData = new URLSearchParams()
    formData.append('username', process.env.WDIO_KC_TEST_USERNAME || 'test')
    formData.append('password', process.env.WDIO_KC_TEST_PASSWORD || 'test')

    // Step #3
    const authenticateResponse = await fetch(authenticateURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        body: formData
    });

    // Step #4,5,6 happens from here
    if (authenticateResponse.ok) {
        const redirectUrl = await authenticateResponse.text()
        expect(redirectUrl).toBe('Close your browser to continue')
    } else {
        throw new Error('Network response was not ok.')
    }

}