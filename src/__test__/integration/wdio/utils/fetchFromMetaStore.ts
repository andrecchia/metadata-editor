import $RefParser from "@apidevtools/json-schema-ref-parser";
import { LOCAL_METASTORE, MetaStoreInstancesURL } from "../../../../renderer/constants";

/**
 * Performs GET requests to MetaStore taking all the results avaiable among all the pages
 * @param endpoint where to perform the GET request, query parameters included
 * @param headers the header request, if needed
 * @returns array with the fetched results
 */
export async function getAllMetaStorePages(endpoint: string, headers?: HeadersInit){
    const MetaStoreInstance = LOCAL_METASTORE
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
    // Define the URL
    const URL = BASE_URL + '/' + endpoint + (endpoint.includes('?') ? '&' : '?')
    let data: any[] = [];
    // The size of the page to be returned (i.e. how many schemas)
    const size: number = 10;
    // Here will be stored the total schemas present on MetaStore
    let avaiableDocumentsNumber: number|undefined = undefined;
    let page: number = 0;
    // Loop until all the pages are taken
    while(avaiableDocumentsNumber === undefined || page < Math.ceil(avaiableDocumentsNumber/size) ){
      const response = await fetch(URL +'page=' + page.toString() + '&size=' + size.toString(),{
        headers: headers
      });
      // At the 1st iteration define get the information on how many schemas there are from response header
      if(avaiableDocumentsNumber === undefined){
        avaiableDocumentsNumber = parseInt(response.headers.get('content-range')?.split('/')[1] as string);
      }
      const response_json = await response.json();
      // Store all the schemas here
      data = [...data, ...response_json]
      page++;
    }
    return data;
  }
  
  /**
   * Retrieve all the schemas from current MetaStore instance
   * @returns schemaLabel: array of strings containing the labels for all the avaiable schemas
   * schemaIdLabel: as object with labels as keys and correspondent schema ids (arrays of strings) as values. 
   * @example {"input":["input_schema"], "measurement": ["SEM_schema", "STM_schema"], ... }
   *  schemaLastVersion: an object containing the schema id as a key and its last avaiable version
   *  as a value (@example: {'foo_schema': 3, 'bar_schema': 1, ...})
   */
  export async function scanSchemasFromMetaStore(){
    let schemaLastVersion = {};
    let schemaIdLabel = {};
    const data = await getAllMetaStorePages('schemas')
    //                                                     if label is undefined or empty string...                   ... set 'no label' , else set label
    const schemaLabel = [...new Set(data.map(item => item.label === undefined || (item.label as string).trim().length === 0 ? 'no label' : item.label))] as string[];
    for (const label of schemaLabel){
      schemaIdLabel[`${label}`] = []
    }
    for(const obj of data){
      //               if label is undefined or empty string...                   ... set 'no label' , else set label
      const label = obj.label === undefined || (obj.label as string).trim().length === 0 ? 'no label' : obj.label
      schemaIdLabel[label].push(obj.schemaId) 
      schemaLastVersion[obj.schemaId] = obj.schemaVersion;
    }
    return {schemaLabel, schemaIdLabel, schemaLastVersion};
  }
  
  /**
   * Makes a call to MetaStore APIs to get the selected JSON schema.
   * @param schemaId 
   * @param schemaVersion
   */
  export async function getSchemaFromMetaStore(schemaId, schemaVersion){
    const MetaStoreInstance = LOCAL_METASTORE
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
    const response = await fetch(BASE_URL + '/schemas/' + schemaId + '?version=' + schemaVersion);
    const schema = await response.json();
    if(400 <= response.status && response.status <= 599 ){
      throw new Error(JSON.stringify(schema));
    }
    delete schema.$schema;
    if('$defs' in schema){
      return await $RefParser.dereference(schema)
    }
    return schema;
  }