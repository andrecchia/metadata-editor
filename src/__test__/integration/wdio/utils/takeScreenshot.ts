
/**
 * Save a screenshot to logs folder, useful to debug
 * @param prefix 
 */
export const takeScreenshot = async (prefix:string) => {
    const wdioWorkerID = process.env.WDIO_WORKER_ID
    await browser.saveScreenshot(`${__dirname}/../logs/${prefix}-${wdioWorkerID}.png`)
}