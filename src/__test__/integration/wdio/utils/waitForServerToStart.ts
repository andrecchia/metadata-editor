import fs from 'fs'
/**
 * Waits for the API server to start by checking log file until 'Server listening on port ...' 
 * appears
 * @param port 
 * @param wdioWorkerID 
 * @param interval 
 * @returns 
 */
export const waitForServerToStart = (port: string, wdioWorkerID: string, interval: number = 500) => {

    return new Promise((resolve, reject) => {
        const intervalId = setInterval(() => {
            try {
                const output = fs.readFileSync(`${__dirname}/../logs/wdio-${wdioWorkerID}-chromedriver.log`, 'utf8');
                const serverStarted = output.includes(`Server listening on port ${port}`);
                
                if (serverStarted) {
                    clearInterval(intervalId);
                    resolve(true);
                }
            } catch (error) {
                clearInterval(intervalId);
                reject(error);
            }
        }, interval);
    });
}