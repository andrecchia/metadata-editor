import { browser, $, expect } from '@wdio/globals'
import { selectMetaStoreInstance } from './utils/selectMetaStoreInstance';
import { selectOptionFromHome } from './utils/selectOptionFromHome';
import { BAD_SCHEMAS, DocumentManagerOptions, MenuOptions, TestConst } from './utils/config';
import { getSchemaFromMetaStore, scanSchemasFromMetaStore } from './utils/fetchFromMetaStore';
import { JSONSchemaFaker } from "json-schema-faker";
import startCase from 'lodash/startCase';
import { selectOptionFromDocumentManager } from './utils/selectOptionFromDocumentManager';
import { doLogin } from './utils/loginUtil';
import { waitForServerToStart } from './utils/waitForServerToStart';


const findLabelBySchemaId = (schemaIdLabel, schemaId) => {
  for (const key in schemaIdLabel) {
    if (schemaIdLabel[key].includes(schemaId)) {
        return key;
    }
  }
  return null;
}

const takeFirstElement = (schema) => {
  const stack = [schema.properties]
  while(stack.length > 0){
    try{
      const subSchema = stack.pop()
      const [key, val] = Object.entries<any>(subSchema)[0]
      if(val.type === "object"){
        stack.push(val.properties)
      } else {
        const title = val.title || undefined
        const firstKey = key
        return {firstKey, title}
      }
    } catch(e){
      return undefined
    }
  }
}

describe('Metadata Editor Testing', () => {
    it('Render all schemad, fill via API, upload to MetaStore', async () =>{

      // select the instance and go to Home
      await selectMetaStoreInstance('Local')

      const {schemaIdLabel, schemaLastVersion} = await scanSchemasFromMetaStore()

      const seed = 0.09
      JSONSchemaFaker.option({
        failOnInvalidFormat: false,
        failOnInvalidTypes: false,
        minDateTime: "1889-12-31T00:00:00.000Z",
        maxDateTime: "1970-01-01T00:00:00.000Z",
        random: () => seed
      });
      // Used to log in only the 1st time
      let isFirstIteration = true
      // Used to check if server started
      let isServerUp = false
      
      for(const [schemaId, lastVersion] of Object.entries(schemaLastVersion)){
        
        // skip bad schemas
        if(BAD_SCHEMAS.includes(schemaId)){
          continue
        }

        await selectOptionFromHome(MenuOptions.METASTORE_SCHEMAS)

        const schema = await getSchemaFromMetaStore(schemaId, lastVersion)
        const label = findLabelBySchemaId(schemaIdLabel, schemaId)
                  
        const generatedDocument = JSONSchemaFaker.generate(schema)
        const firstElement = takeFirstElement(schema)

        // Trigger the dropdown 'schema label' to open by clicking on it
        const dropdownLabel = await browser.$('#selectSchemaLabel');
        await dropdownLabel.click();
        
        // Select the label from the dropdown menu
        const labelOption = await browser.$(`li*=${label}`)
        await labelOption.click()
        
        // Trigger the dropdown 'schema id' to open by clicking on it
        const dropdown = await browser.$('#selectSchemaId');
        await dropdown.click();
        
        // Select the id from the dropdown menu
        const schemaIdOption = await browser.$(`li*=${schemaId}`)
        await schemaIdOption.click()
        
        // Click on Load button
        const ls = await browser.$('button=Load Schema');
        await ls.click();

        // Assertion before compiling form
        if(firstElement){
          if(firstElement.title){
            // Assertion on document rendered
            console.debug(schemaId)
            const firstKey = await browser.$(`label*=${firstElement.title}`)
            await firstKey.waitForExist()
            expect(await firstKey.isExisting()).toBe(true)
          } else {
            // Assertion on document rendered
            console.debug(schemaId)
            const firstKey = await browser.$(`label*=${startCase(firstElement.firstKey)}`)
            await firstKey.waitForExist()
            expect(await firstKey.isExisting()).toBe(true)
          }
        }

        // Fill the form via API
        const wdioWorkerID = process.env.WDIO_WORKER_ID
        const lastDigit = wdioWorkerID!.slice(-1)
        if(!isServerUp){
          // Wait for server to start, if started set the variable to true
          await waitForServerToStart(`${TestConst.API_PORT_PREFIX}${lastDigit}`, wdioWorkerID!)
          isServerUp = true
        }
        const response = await fetch(`http://localhost:${TestConst.API_PORT_PREFIX}${lastDigit}/api/v1/compile`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json' // Indicating that the request body is JSON
              },
              body: JSON.stringify(generatedDocument) 
        })
        // Wait for correctly processing request
        await response.text()

        // Click on Done
        const done = await browser.$('a=Done')
        await done.click()

        // Go to upload page
        await selectOptionFromDocumentManager(DocumentManagerOptions.UPLOAD_TO_METASTORE)
        
        if(isFirstIteration){
          // Log in only the 1st time, then the user will be already logged in
          await doLogin(wdioWorkerID as string)
            isFirstIteration = false
          }

        // Wait that the page is correctly shown
        const metadataRecordLabel = await browser.$('div*=Metadata record')
        await metadataRecordLabel.waitForExist()
        expect(await metadataRecordLabel.isExisting()).toBe(true)
        
        // Set related resource
        const openDropdown = await browser.$('aria/Open')
        await openDropdown.click()
        // Choose 'INTERNAL'
        const internalOption = await browser.$('li=INTERNAL')
        await internalOption.click()
        // Fill in related resource
        const relatedResourceId = await browser.$('[aria-invalid=true]')
        const timestamp = Date.now().toString()
        await relatedResourceId.setValue(`${schemaId}-uploading-${timestamp}`)
        expect(await relatedResourceId.getValue()).toBe(`${schemaId}-uploading-${timestamp}`)

        // Upload
        const uploadButton = await browser.$('button=Upload')
        await uploadButton.waitForClickable()
        await browser.waitUntil(async ()=>{
          const isChanged = (await uploadButton.getCSSProperty('background-color')).value 
            === 'rgba(25,118,210,1)'
          return isChanged
        }, {timeout: 5000})
        await uploadButton.click()

        // Success assertion
        const success = await browser.$('strong=Success')
        await success.waitForExist()
        expect(await success.isExisting()).toBe(true)

        // Go to Home
        const home = await browser.$('a=Home')
        await home.click()
      }

    })
})