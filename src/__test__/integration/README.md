# Reproduce CI/CD integration test

## Prerequisites

- Having removed the [patch](/patches/@openid+appauth+1.3.1.patch) to `@openid`,
because code challenge and verifier must be accessed from tests. (To check if the patch is correctly 
removed there must be log messages when login is attempted on metadata editor). \
**This step is also needed for ordinary (i.e. not CI/CD) integration tests**.
- Having packaged with `DEBUG_PROD=true npm run package`
- Having set some domains to `/etc/hosts`:
    ```shell
    127.0.0.1       test-keycloak
    127.0.0.1       metastore4docker
    ```

## Testing

There is an [integration-test](/.erb/scripts/integration-test.js) script that prepares and runs test environment
equivalent to the CI/CD pipeline, just launch it:
```shell
npm run integration-test
```

**Note**: define an `.env` file in the root forlder of the project containing the needed environment variables, e.g.
```
DEBUG_PROD=true
CHROMEDRIVER_PATH=/path/to/the/chromedriver
```