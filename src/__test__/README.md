This directory contains everything related to testing for the metadata editor app.\
The folder structure is as follows:
- json_files: metadata schemas, documents and uischema that are useful to test
the files upload and metadata schemas compilations via uploads.
- test-utils: useful modules for testing purposes
- under ./unit/main and ./unit/renderer there are tests for the corresponding files under src/main and src/renderer
- integration: contains integration tests files