// Disable no-unused-vars, broken for spread args
/* eslint no-unused-vars: off */
import { contextBridge, ipcRenderer, IpcRendererEvent } from 'electron';
import { Dispatch, SetStateAction } from 'react';
import { NavigateFunction } from 'react-router-dom';
import { IAuthContextProps } from 'renderer/AuthContext';
import { ElectronChannel } from './electronChannels';

const isDebug =
  process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true'

const electronHandler = {
  store: {
    get(key) {
      return ipcRenderer.sendSync(ElectronChannel.ELECTRON_STORE_GET, key);
    },
    set(property, val, setMask=true) {
      ipcRenderer.send(ElectronChannel.ELECTRON_STORE_SET, property, val, setMask);
    },
    /**
     *  Register an event listener for the `electronStoreDidChange` message (sent by
     *  electron-store `onDidAnyChange` method via `mainWindow.webContents.send`)
     * @param {*} callback event handler for the `electronStoreDidChange` event
    */
   handleElectronStoreChange: (callback, subChannel:string) => {
     ipcRenderer.on(
      `${ElectronChannel.ELECTRON_STORE_DID_CHANGE}${subChannel}`, 
      (event,newVisibilityState)=>callback(event,newVisibilityState)); 
    },
    /**
     *  Remove the event listener for the `electronStoreDidChange` event.
    */
    removeElectronStoreChangeHandler: (subChannel) => {
      // Remove all the listeners for the subChannel
      ipcRenderer.removeAllListeners(`${ElectronChannel.ELECTRON_STORE_DID_CHANGE}${subChannel}`)
     }
  },
  maskHandler:{
    /**
     * Triggers the MaskHandler object initialization
     * @param mask 
     * @param schemaMaskId 
     */
    assignMaskHandler(mask, schemaMaskId:string) {
      ipcRenderer.send(ElectronChannel.MASK_HANDLER_ASSIGN, mask, schemaMaskId)
    },
    /**
     * To get the visibility state from renderer process
     * @param path 
     * @returns 
     */
    getVisibilityState: (path:string) => 
      ipcRenderer.invoke(ElectronChannel.MASK_HANDLER_GET_STATE, path),
    /**
     * To insert a visibility state group from renderer process after the first schema rendering
     * @param mask 
     * @param pathToInsert 
     * @returns 
     */
    insertIntoVisibilityState: (mask, pathToInsert: string) => 
      ipcRenderer.send(ElectronChannel.MASK_HANDLER_INSERT_INTO_STATE, mask, pathToInsert),
    /**
     * Unsets the MaskHandler object, when a new schema is rendered
     */
    unsetMaskHandler() { ipcRenderer.send(ElectronChannel.MASK_HANDLER_UNSET) },
  },
  auth: {
    /**
     * IPC for 2 ways communication. Performs login.
     * @returns Promise that resolves with the response from the main process.
     */
    signIn: () => ipcRenderer.invoke(ElectronChannel.AUTH_SIGN_IN),
    signOut: () => ipcRenderer.send(ElectronChannel.AUTH_SIGN_OUT),
    /**
     * IPC listener for `auth:authContext` events, that are events that changes the AuthContext values
     * @param setAuth set the auth state
     */
    authContextListener: (setAuth: Dispatch<SetStateAction<IAuthContextProps>>) =>{
      ipcRenderer.on(ElectronChannel.AUTH_AUTH_CONTEXT, (_event, value)=>{
        setAuth({
          authenticated: value.authenticated, 
          token: value.token,
          userInfo: value.userInfo
        })
      })
    },
    removeAuthContextListener:() => {
      ipcRenderer.removeAllListeners(ElectronChannel.AUTH_AUTH_CONTEXT)
    },
    /** Cancels a sign in process without complete it */
    abortSignIn: () => ipcRenderer.send(ElectronChannel.AUTH_ABORT_SIGN_IN),
    /** Send keycloak configuration parameters based on selected MetaStore instance */
    configOIDC: (config) => ipcRenderer.send(ElectronChannel.AUTH_CONFIG_OIDC, config)
  },
  API: {
    /**
     * Register an `API:request` event ipc listener
     * @param callback event handler
     */
    requestListener: (callback) => {
      ipcRenderer.on(ElectronChannel.API_REQUEST, callback)
    },
    /**
     * Removes an `API:request` event ipc listener
     */
    removeRequestListener:() => {
      ipcRenderer.removeAllListeners(ElectronChannel.API_REQUEST)
    }
  },
  MetaStoreInstance: {
    /**
     * Listens from menù Settings->Set MetaStore instance event, then
     * goes to MetaStore_instance page
     * @param navigate 
     */
    setInstanceListener: (navigate: NavigateFunction)=>{
      ipcRenderer.on(ElectronChannel.METASTORE_INSTANCE_SET, ()=>{
        navigate('/MetaStore_instance')
      })
    },
    removeListener: () => {
      ipcRenderer.removeAllListeners(ElectronChannel.METASTORE_INSTANCE_SET)
    }
  },
  JSONschema: {
    /**
     * Takes a JSON schema with `$ref`s and resolves it
     * @param schema the schema to resolve
     * @returns the resolved schema
     */
    resolve: (schema) => ipcRenderer.invoke(ElectronChannel.RESOLVE_SCHEMA, schema)
  },
  infos: {
    /** One time listener to get some useful constants variables */
    getConstants: (callback) => ipcRenderer.on(ElectronChannel.INFO_CONSTANTS, callback)
  }
};

if(isDebug){
  const electronDebug = {
      /** List all channels with a listener */
      listChannelListener: () => console.log(ipcRenderer.eventNames()),
      forEachChannelListener: () => 
        ipcRenderer
        .eventNames()
        .forEach(channel => console.log(channel, ipcRenderer.rawListeners(channel).length))
  };

  contextBridge.exposeInMainWorld('electronDebug', electronDebug);
}

contextBridge.exposeInMainWorld('electron', electronHandler);

export type ElectronHandler = typeof electronHandler;
