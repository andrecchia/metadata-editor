import detectPort from 'detect-port';
import { app, Notification } from 'electron';

/**
 * Create and show a notification message
 * @param notificationMessage the message in the notification
 */
function showNotification ( notificationMessage: string) {
  new Notification({ title: 'Metadata editor error!', body: notificationMessage }).show()
}

/**
 * Show the notification message when the electron `app` object is initialized
 * @param notificationMessage the message in the notification
 */
async function errNotification( notificationMessage: string){
  try {
    await app.whenReady();
    showNotification(notificationMessage);
  } catch (error) {
    // Handle any errors that occur during whenReady() or showNotification()
    console.error(error);
  }
}

/**
 * Check that a given port is a valid port and it is not occupied
 * @private
 * @param port the port to check
 * @param portDescription a brief description for the service that will use that port (e.g. 'rest API')
 * @param rangeMin lower boundary for acceptable port range
 * @param rangeMax upper boundary for acceptable port range
 */
export const checkPort = async (port: string|number, portDescription:string, rangeMin:number = 1025, rangeMax:number = 65536) => {
  const portEnvName = portDescription === 'auth redirection' ? 'ME_AUTH_PORT' : 'ME_API_PORT'
  const helpMessage = ` To define a port for ${portDescription} set the '${portEnvName}' environment variable.`
  // check if port is correctly defined
  if(port === undefined ){ 
    const errMessage = `Please define a port for ${portDescription}.` + helpMessage
    console.error(errMessage)
    await errNotification(errMessage)
    process.exit(1)
  }

  // if is a string check that it contains only numbers
  if(typeof port === 'string'){ 
    if(!/^\d+$/.test(port)){
      //.. if it doesn't exit
      const errMessage = `Port should be an integer, received '${port}', please set a valid port.` + helpMessage
      console.error(errMessage)
      await errNotification(errMessage)
      process.exit(1)
    }
  }

  // Check that the port number is in a valid range
  if(port as number < rangeMin || port as number > rangeMax){ 
    const errMessage = `Port '${port}' for ${portDescription} is not valid, please select a port in the range ${rangeMin}-${rangeMax}.` + helpMessage 
    console.error(errMessage)
    await errNotification(errMessage)
    process.exit(1)
  } 
  
  // If port it is a well defined and reasonable number, check if it is free
  try {
    const _port = await detectPort(port as number);
    if (port != _port) {
      const errMessage = `Port '${port}' for ${portDescription} is occupied, try another port in the range ${rangeMin}-${rangeMax}.` + helpMessage;
      console.error(errMessage);
      await errNotification(errMessage);
      process.exit(1);
    }
  } catch (err) {
    const errMessage = 'Unexpected error in checking valid ports ' + err + '.' + helpMessage;
    console.error(errMessage);
    await errNotification(errMessage);
    process.exit(1);
  }
}

/**
 * Check auth and API ports
 * @param redirectPort port for auth redirect
 * @param APIport port for rest API endpoints
 */
export const checkPorts = async (redirectPort:string|number, APIport:string|number) =>{
  await checkPort(redirectPort,'auth redirection', 57640, 57649)
  await checkPort(APIport,'rest API')
}
