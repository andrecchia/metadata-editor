/* eslint global-require: off, no-console: off, promise/always-return: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `npm run build` or `npm run build:main`, this file is compiled to
 * `./src/main.js` using webpack. This gives us some performance wins.
 */
import path from 'path';
import { app, BrowserWindow, shell, ipcMain } from 'electron';
import { autoUpdater } from 'electron-updater';
import log from 'electron-log';
import MenuBuilder from './menu';
import { resolveHtmlPath } from './util';
import Store from 'electron-store';
import { Auth } from './AppAuth-JS/auth';
import { createServer } from 'http';
import { signInTimeout, waitServerShutdown } from './AppAuth-JS/constants';
import { checkPorts } from './checkPorts';
import { port as redirectPort, useOpenIDConnectState } from './AppAuth-JS/config'
import { MaskHandler } from './MaskHandler/MaskHandler';
import { ServerAPI } from './server/ServerAPI';
import express, { Express } from 'express';
import { ElectronChannel } from './electronChannels';
import { testMeApiPort } from './utils/testPortsForWdio';
import $RefParser from '@apidevtools/json-schema-ref-parser';


const APIport = process.env.DEBUG_PROD ? 
  (testMeApiPort || process.env.ME_API_PORT || 3333) : 
  process.env.ME_API_PORT || 3333
// Check whether ports for auth redirection and API endpoints are valid
checkPorts(redirectPort, APIport)


class AppUpdater {
  constructor() {
    log.transports.file.level = 'info';
    autoUpdater.logger = log;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

let auth: Auth | null = null;
let mainWindow: BrowserWindow | null = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

const isDebug =
  process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true';

if (isDebug) {
  require('electron-debug')();
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS'];

  return installer
    .default(
      extensions.map((name) => installer[name]),
      forceDownload
    )
    .catch(console.log);
};

const createWindow = async () => {
  if (isDebug) {
    // await installExtensions();
  }

  const RESOURCES_PATH = app.isPackaged
    ? path.join(process.resourcesPath, 'assets')
    : path.join(__dirname, '../../assets');

  const getAssetPath = (...paths: string[]): string => {
    return path.join(RESOURCES_PATH, ...paths);
  };

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    icon: getAssetPath('icon.png'),
    webPreferences: {
      preload: app.isPackaged
        ? path.join(__dirname, 'preload.js')
        : path.join(__dirname, '../../.erb/dll/preload.js'),
    },
  });

  mainWindow.loadURL(resolveHtmlPath('index.html'));

  mainWindow.on('ready-to-show', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  // Define listener for AppAuth
  auth = new Auth(mainWindow);

  // Open urls in the user's browser
  mainWindow.webContents.setWindowOpenHandler((edata) => {
    shell.openExternal(edata.url);
    return { action: 'deny' };
  });

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  // new AppUpdater();

  /**Send some useful constants to renderer */
  mainWindow.webContents.on('did-finish-load', () => {
    // The main window has finished loading
    // Now I can send the constant variables to renderer
    mainWindow!.webContents.send(
      ElectronChannel.INFO_CONSTANTS,
      {signInTimeout, waitServerShutdown, APIport}
    )
  });
};


/**
 * Electron-store
*/
// Create an instance of electron-store
const store = new Store({name: 'schemaMasks'});

// Declare a MaskHandler object
let maskHandler:MaskHandler|undefined=undefined

// IPC listener
ipcMain.on(ElectronChannel.ELECTRON_STORE_GET, async (event, val) => {
    event.returnValue = store.get(val);
  });
ipcMain.on(ElectronChannel.ELECTRON_STORE_SET, async (event, key, val, setMask) => {
    store.set(key, val);

    if(maskHandler && setMask){
      // Set visibility for group containing 'key' and its group parents
      maskHandler.setGroupsVisibilityState(key,val)
    }
})

/**
 * Maks handler
 */
ipcMain.on(
  ElectronChannel.MASK_HANDLER_ASSIGN, 
  async (event, mask, schemaMaskId)=>{
    //  Create and initialize MaskHandler object 
    maskHandler = new MaskHandler(mainWindow!, mask, schemaMaskId)
  }
)
ipcMain.handle(
  ElectronChannel.MASK_HANDLER_GET_STATE, 
  (event, path) => maskHandler?.getGroupVisibilityState(path)
)
ipcMain.on(
  ElectronChannel.MASK_HANDLER_INSERT_INTO_STATE, 
  async (event, mask, pathToInsert) => { 
    maskHandler?.insertIntoGroupVisibilityState(mask, pathToInsert) 
  }
)
ipcMain.on(ElectronChannel.MASK_HANDLER_UNSET, async ()=>{
  //  Unset the object
  maskHandler = undefined
})

// Auth
ipcMain.handle(ElectronChannel.AUTH_SIGN_IN, async () => {
  if(auth == null){
    auth = new Auth(mainWindow as BrowserWindow);
  }
  const response = await auth?.signIn()
  return response
})
ipcMain.on(ElectronChannel.AUTH_SIGN_OUT, async ()=>{
  await auth?.signOut()
  auth = null
})
ipcMain.on(ElectronChannel.AUTH_ABORT_SIGN_IN, async ()=>{
  await auth?.abortSignIn()
  auth = null
})
ipcMain.on(ElectronChannel.AUTH_CONFIG_OIDC, async (event, config) => {
  const {setOpenIDConnectParams}=useOpenIDConnectState.getState()
  setOpenIDConnectParams(config)
})

// Schema
ipcMain.handle(
  ElectronChannel.RESOLVE_SCHEMA, 
  async (event, schema) =>{
    return await $RefParser.dereference(schema)
})

/**
 * Add event listeners...
 */
app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app
  .whenReady()
  .then(() => {
    createWindow();
    app.on('activate', () => {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (mainWindow === null) createWindow();
    });
    
    const expressApp: Express = express()
    const serverAPI = new ServerAPI(mainWindow!,expressApp, APIport)
  })
  .catch(console.log);

  /**
   * Electron main window getter
   * @returns electron main window
   */
export function getMainWindow(){
  return mainWindow
}