/** This is to define a customized port based on WDIO_WORKER_ID environment variable,
 *  useful when multiple wdio tests are run in parallel
 */

/** */
const lastDigit = process.env.WDIO_WORKER_ID ? process.env.WDIO_WORKER_ID.slice(-1) : undefined
export const _API_PORT_PEEFIX = "444"
export const testMeApiPort = process.env.WDIO_WORKER_ID ? `444${lastDigit}` : undefined
export const testMeAuthPort = process.env.WDIO_WORKER_ID ? `5764${lastDigit}` : undefined