import { BrowserWindow } from "electron"
import { ElectronChannel } from "../electronChannels"

export type MaskAsArray = {
  [key: string]: string[]
}

/**
 * Considering the mask as a tree, this function takes the mask and defines an entry
 * in a hashmap with the path-to-node as keys and (empty) array as values, for each node 
 * that is not a leaf. Furthermore, it insert all the `true` leafs (actually the path-to-leaf)
 * into an array
 * @param mask the schema mask with true or false values for each field
 * @param schemaMaskId
 * @returns flattened mask with path as keys and empty arrays as values, and array containing mask true values 
 * @private
 */
export const flattenToArrays = (mask: any, schemaMaskId: string): { 
  flattenedToArrayMask: MaskAsArray, 
  initialTrueKeys: string[] 
} => {
  let flattenedToArrayMask: MaskAsArray = {}
  let initialTrueKeys: string[] = []
  // Helping stack
  const stack: { mask: any, path: string }[] = [{ mask, path: schemaMaskId }]
  while (stack.length > 0) {
    const { mask, path } = stack.pop()!
    for (const [k, v] of Object.entries(mask)) {
      const currentPath = `${path}.${k}`
      // When value is an object put it into stack and process it in the next 'while' iteration
      if (typeof v === 'object') {
        flattenedToArrayMask[currentPath] = []
        stack.push({ mask: v, path: currentPath })
      } else {
        // Enters here when value is not object
        /**
         * @todo is this `if` really needed?
         */
        if (!flattenedToArrayMask[path]) {
          flattenedToArrayMask[path] = []
        }
        // When value is true insert the path into array
        if (v) {
          initialTrueKeys.push(currentPath)
        }
      }
    }
  }

  return { flattenedToArrayMask, initialTrueKeys }
}

/**
 * Handles mask values to send messages to renderer process when a group changes state.
 * It does this storing groups into a {@link MaskAsArray} structure, and monitoring the 
 * changes from empty to populated (and viceversa) of the arrays into the structure.
 * (Groups=nested level, i.e. an entry with `type: object` in JSON schema)
 */
export class MaskHandler {
    private mainWindow: BrowserWindow
    /** Stores information about groups to show/hide,
     *  one array for each group
     */
    private groupsVisibilityState:MaskAsArray

    constructor(mainWindow: BrowserWindow, mask, schemaMaskId:string){
      this.mainWindow = mainWindow
      const {flattenedToArrayMask, initialTrueKeys} = flattenToArrays(mask, schemaMaskId)
      this.groupsVisibilityState = flattenedToArrayMask
      /**
       * Initialize the visibility state inserting true values into groupsVisibilityState
       * and sending messages to renderer
       */
      for(const key of initialTrueKeys){
        this.setGroupsVisibilityState(key,true)
      }
    }

    /**
     * Updates the groupsVisibilityState when a mask field is changed
     * @param key path to the field
     * @param val true or false
     */
    setGroupsVisibilityState(key:string, val:boolean){
      const lastDot = key.lastIndexOf('.')
      const k = key.substring(0,lastDot)
      if(!this.groupsVisibilityState[k]){
        this.groupsVisibilityState[k] = []
      }
      if(val){
        this.push(this.groupsVisibilityState[k],key.substring(lastDot+1),k)
      } else {
        this.remove(this.groupsVisibilityState[k],key.substring(lastDot+1),k)
      }
    }

    /**
     * Insert the name of a field that has been set as true in the mask into the
     * its group array. Insert the path-to-field into all the parent group arrays.
     * If the group arrays change state sends a message to renderer.
     * @param groupArray the array for the group
     * @param valueToInsert the name of the field that has been set to true
     * @param groupPath the path to the field
     */
    push(groupArray: string[], valueToInsert: string, groupPath: string) {
      if (!groupArray) {
        return;
      }

      const fixedPath = groupPath
      const fixedValue = valueToInsert
      const stack = [{ array: groupArray, valueToInsert, path: groupPath}];
    
      while (stack.length > 0) {
        const { array, valueToInsert, path } = stack.pop()!
    
        const wasEmpty = array.length === 0
        array.push(valueToInsert)
    
        if (wasEmpty && array.length > 0) {
          this.mainWindow.webContents.send(`${ElectronChannel.ELECTRON_STORE_DID_CHANGE}${path}`, true)
        }
    
        const lastDot = path.lastIndexOf('.')
        const p = path.substring(0, lastDot)
    
        if (p in this.groupsVisibilityState) {
          stack.push({
            array: this.groupsVisibilityState[p],
            valueToInsert: `${fixedPath}.${fixedValue}`,
            path: p
          });
        }
      }
    }

    /**
     * Remove the name of a field that has been set as false in the mask into the
     * its group array. Remove the path-to-field from all the parent group arrays.
     * If the group arrays change state sends a message to renderer.
     * @param groupArray the array for the group
     * @param valueToInsert the name of the field that has been set to false
     * @param groupPath the path to the field
     */
    remove(groupArray: string[], valueToRemove: string, groupPath: string) {
      if (!groupArray) {
        return;
      }
      
      const fixedKey = groupPath
      const fixedValue = valueToRemove
      const stack = [{ array: groupArray, valueToRemove, path: groupPath}];
    
      while (stack.length > 0) {
        const { array, valueToRemove, path } = stack.pop()!
    
        const wasPopulated = array.length > 0
        const index = array.indexOf(valueToRemove)
        if (index !== -1) {
          array.splice(index, 1);
        }
    
        if (wasPopulated && array.length === 0){
          this.mainWindow.webContents.send(`${ElectronChannel.ELECTRON_STORE_DID_CHANGE}${path}`, false)
        }
      
        const lastDot = path.lastIndexOf('.')
        const p = path.substring(0, lastDot)
    
        if (p in this.groupsVisibilityState) {
          stack.push({
            array: this.groupsVisibilityState[p],
            valueToRemove: `${fixedKey}.${fixedValue}`,
            path: p
          });
        }
      }
    }

    /**
     * Gets the visibility state of a group by its path
     * @param path 
     * @returns the visibility state (true/false) for the group, or undefined in case of errors.
     */
    getGroupVisibilityState(path:string): boolean | undefined{
      try{
        const state: boolean = this.groupsVisibilityState[path].length > 0
        return state
      }catch(e){
        console.log("EXCEPTION getGroupVisibilityState", e, path, this.groupsVisibilityState[path])
      }
    }

    /**
     * Inserts a group, via its mask, into the group visibility state object. Used in complex objects
     * (like arrays, oneOf, and so on) that are not rendered by default.
     * @param mask the (sub)mask for the group
     * @param pathToInsert the path name for the entry to insert
     */
    insertIntoGroupVisibilityState(mask, pathToInsert: string){
      try{
        const { flattenedToArrayMask } = flattenToArrays(mask, pathToInsert)
        this.groupsVisibilityState = {...this.groupsVisibilityState, ...flattenedToArrayMask}
      } catch(e){
        console.log("EXCEPTION insertIntoGroupVisibilityState", e, pathToInsert, mask)
      }
    }
}