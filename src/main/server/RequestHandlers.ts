import { BrowserWindow, ipcMain } from 'electron';
import { ElectronChannel } from '../electronChannels';

export class RequestHandlers{
    mainWindow: BrowserWindow

    constructor(mainWindow: BrowserWindow){
        this.mainWindow = mainWindow
        this.handleCompileRequest = this.handleCompileRequest.bind(this);
        this.handleReadRequest = this.handleReadRequest.bind(this);
    }
    /**
     * Sends a message to the frontend from inside to a REST endpoint,
     * the message content depends on the endpoint parameters and on the 
     * client (i.e. who called the endpoint) request.
     * @param {BrowserWindow} mainWindow electron main window
     * @param {string} path endpoint path
     * @param {*=} [ content ] request content
     */
    private sendEndpointMessage(mainWindow, path, content?){
      if (mainWindow) {
        let toSend = {
          'path': path,
          'content': content
        }
        mainWindow.webContents.send(ElectronChannel.API_REQUEST, JSON.stringify(toSend))
      }
    }
    
    /**
     * Sends the frontend message to the client who called the REST endpoint, and 
     * based on the frontend message it returns an appropriate HTTP response. 
     * @param {*} message the frontend message (response to a previous request)
     * @param {*} reqName a code that specify the frontend message 'type' (@example if the 
     * frontend message is a consequence of a previous `/compile` or `/read` request)
     * @param {*} onSuccess callback for succesfull operation
     * @param {*} onError callback for errors
     */
    private sendResponseMessage(message, reqName, onSuccess, onError ){
        try {
            const parsedMessage = JSON.parse(message);
            const requestName = parsedMessage['requestName'];
            if(requestName === reqName){
              onSuccess(parsedMessage['resp']);
            }
          } catch (e) {
              onError(e);
          } 
    }
    
    /**
     * Handle the requests to `/compile` endpoint.
     * The request is processed via electron IPC messages:
     * - A message is sent to the frontend, specifying the source (i.e. the endpoint)
     * and the request content
     * @example
     * ```
     * {
     *   'path':'/api/v1/compile',
     *   'content': {
     *     'field0':'content0',
     *     'field1': {
     *       'field2':'content2'
     *     }
     *   }
     * }
     * ```
     * - An ipcMain listen for frontend messages (as a response, containing errors for instance) and processes it returning 
     * an HTTP response
     * @param {*} req request
     * @param {*} res response
     */
    handleCompileRequest(req, res) {
      this.sendEndpointMessage(this.mainWindow, req.path, req.body);
      ipcMain.once('API:response', (event,message) => {
        this.sendResponseMessage(
          message,
          "RESP_COMPILE",
          (parsedResp) => {
            if(parsedResp.length === 0){
              res.status(200).send();
            } else {
              res.status(422).json(
                  {
                    error: "Invalid fields",
                    message : `Cannot find [${parsedResp}] keys in the schema`
                  }
              );
            }
          },
          (error) => {res.status(500); console.log(error)}
        );
      });
    }
    
    /**
     * Handle the requests to `/read` endpoint.
     * The request is processed via electron IPC messages:
     * - A message is sent to the frontend, specifying the source (i.e. the endpoint)
     * and the request content
     * - An ipcMain listen for frontend messages (as a response) and processes it returning 
     * an HTTP response togheter with its content
     * @param {*} req request
     * @param {*} res response
     */
    handleReadRequest(req, res) {
      this.sendEndpointMessage(this.mainWindow, req.path);
      ipcMain.once('API:response', (event,message) => {
        this.sendResponseMessage(
          message,
          "RESP_READ",
          (parsedResp) => {res.status(200).json(parsedResp)},
          (error) => {res.status(500); console.log(error)}
        );
      });
    }
}
