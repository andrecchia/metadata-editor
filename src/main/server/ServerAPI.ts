import { json, static as staticExpress, Express, Request, Response } from 'express';
import { join } from 'path';
import { BrowserWindow } from 'electron';
import cors from 'cors';
import { serve, setup } from 'swagger-ui-express';
import swaggerDocument from './swagger/swagger.json';
import { createServer } from 'http';
import { RequestHandlers } from './RequestHandlers';

/**
 * @private
 */
export const handleNotValidJSON = (err, req: Request, res: Response, next) => {
    if (err instanceof SyntaxError && (err as any).status === 400 && 'body' in err) {
        console.debug(err);
        return res.status(400).json({
            error: 'Bad Request',
            message: 'The body of the request is not valid JSON'
        }); // Bad request
    }
    next();
}

/**
 * @private
 */
export const acceptOnlyJSON = (req: Request, res: Response, next) => {
    if (req.method === 'POST' && req.headers['content-type'] !== 'application/json') {
        return res.status(415).json({
        error: 'Unsupported Media Type',
        message: 'Content-Type must be application/json'
        });
    }
    next();
}

export class ServerAPI {
    private expressApp: Express
    private mainWindow: BrowserWindow
    private APIport
    private requestHandler: RequestHandlers

    constructor(mainWindow: BrowserWindow, expressApp: Express, APIport){
        this.mainWindow = mainWindow
        this.APIport = APIport
        this.requestHandler = new RequestHandlers(this.mainWindow)

        this.expressApp = expressApp

        /** Wait 5s that everything is load correctly, then start server to expose API endpoints */
        setTimeout(()=>this.startAPIServer(this.APIport),5000)

        this.setupMiddlewares()
        this.serveSwagger()
        this.setupStaticFiles()
        this.setupEndpoints()
    } 

    /**
        * Starts a server in localhost:APIport to expose rest endpoints
        * @param APIport 
        */
    private startAPIServer(APIport){
        // Start the server to expose API
        const server = createServer(this.expressApp);
    
        server.listen(APIport, () => {  
        console.log(`Server listening on port ${APIport}`);
        });
    }

    private setupMiddlewares(){
        // enabling CORS for any unknown origin (https://xyz.example.com)
        this.expressApp.use(cors());

        // parses incoming requests with JSON payloads
        this.expressApp.use(json());
        // Handle not valid JSON in HTTP requests
        this.expressApp.use(handleNotValidJSON);
        // Accepts only application/json
        this.expressApp.use(acceptOnlyJSON)
    }
    
    private serveSwagger(){
        // Change server port when the relative env variable is defined
        if(process.env.ME_API_PORT){
            swaggerDocument.servers[0].url = `http://localhost:${process.env.ME_API_PORT}`
        }
        // Serve the Swagger documentation
        this.expressApp.use('/api-docs', serve, setup(swaggerDocument));
    }
    
    /** 
     * In production: serve static files from the 'build' directory
     * In development: the html file is served by webpack, that enables hot reloading and so on
     * (read some details here:
     *  https://www.newline.co/fullstack-react/articles/how-to-get-create-react-app-to-work-with-your-rails-api/#the-rub- )
     */
    private setupStaticFiles(){
        if(process.env.NODE_ENV==="development"){
            //do nothing
        }else{
            this.expressApp.use(staticExpress(join(__dirname, '../../build')));
        }
        
    }

    private setupEndpoints(){
        /**
         * Compile the schema via POST request
         * @example   
         * ```
         * curl -X POST "http://localhost:3333/api/v1/compile" \
         * -H "Content-Type: application/json" \
         * -d '{ "path.to.field0": "value0", "path.to.field1": "value1" }'
         * ```
         */
        this.expressApp.post('/api/v1/compile', this.requestHandler.handleCompileRequest);

        /**
         * Read the schema via GET request
         * @example   
         * ```curl "http://localhost:3333/api/v1/read"``` to read the whole document
         */
        this.expressApp.get('/api/v1/read', this.requestHandler.handleReadRequest);

        // Handle all other requests by returning the index.html file
        this.expressApp.get('/app*', (req: Request, res: Response) => {
            res.sendFile(join(__dirname, '../../build/index.html'));
        });
    }
}