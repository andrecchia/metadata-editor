export class ElectronChannel {
    static ELECTRON_STORE_GET: string = 'electronStore:get'
    static ELECTRON_STORE_SET: string = 'electronStore:set'
    static ELECTRON_STORE_DID_CHANGE: string = 'electronStoreDidChange:'
    static MASK_HANDLER_ASSIGN: string = 'maskHandler:assign'
    static MASK_HANDLER_GET_STATE: string = 'maskHandler:getState'
    static MASK_HANDLER_INSERT_INTO_STATE: string = 'maskHandler:insertIntoState'
    static MASK_HANDLER_UNSET: string = 'maskHandler:unset'
    static AUTH_SIGN_IN: string = 'auth:signIn'
    static AUTH_SIGN_OUT: string = 'auth:signOut'
    static AUTH_AUTH_CONTEXT: string = 'auth:authContext'
    static AUTH_ABORT_SIGN_IN: string = 'auth:abortSignIn'
    static AUTH_CONFIG_OIDC: string = 'auth:configOIDC'
    static API_REQUEST: string = 'API:request'
    static METASTORE_INSTANCE_SET: string = 'MetaStoreInstance:set'
    static INFO_CONSTANTS: string = 'info:constants'
    static RESOLVE_SCHEMA: string = 'schema:resolve'
}