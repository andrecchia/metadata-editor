import { testMeAuthPort } from "../utils/testPortsForWdio";
import { create } from "zustand";

type OpenIDConnectParams = {
    base_url: string
    realm: string
    clientId: string
}

type OpenIDConnectState = {
    openIDConnectParams: OpenIDConnectParams,
    setOpenIDConnectParams:(value: OpenIDConnectParams) => void
}

export const useOpenIDConnectState = create<OpenIDConnectState>((set) => ({
    openIDConnectParams: {
        base_url: '',
        realm: '',
        clientId: ''
    },
    setOpenIDConnectParams: (value: OpenIDConnectParams) =>
        set({ openIDConnectParams: value }),
}));

export const port = process.env.DEBUG_PROD ? 
    (testMeAuthPort || process.env.ME_AUTH_PORT || 57648) : 
    process.env.ME_AUTH_PORT || 57648
export const redirectUri = `http://127.0.0.1:${port}`;