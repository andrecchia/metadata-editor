/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import { AuthorizationRequest } from '@openid/appauth/built/authorization_request';
import {
  AuthorizationNotifier,
  AuthorizationRequestHandler,
} from '@openid/appauth/built/authorization_request_handler';
import { AuthorizationServiceConfiguration } from '@openid/appauth/built/authorization_service_configuration';
import { NodeCrypto } from '@openid/appauth/built/node_support/';
import { NodeBasedHandler } from '@openid/appauth/built/node_support/node_request_handler';
import { NodeRequestor } from '@openid/appauth/built/node_support/node_requestor';
import {
  GRANT_TYPE_AUTHORIZATION_CODE,
  GRANT_TYPE_REFRESH_TOKEN,
  TokenRequest,
} from '@openid/appauth/built/token_request';
import {
  BaseTokenRequestHandler,
  TokenRequestHandler,
} from '@openid/appauth/built/token_request_handler';
import {
  TokenResponse,
} from '@openid/appauth/built/token_response';
import { RevokeTokenRequest } from '@openid/appauth';
import EventEmitter from 'events';

import { StringMap } from '@openid/appauth/built/types';

export class AuthStateEmitter extends EventEmitter {
  static ON_TOKEN_RESPONSE = 'on_token_response';
}
import { redirectUri, port, useOpenIDConnectState } from './config';
import { signInTimeout, waitServerShutdown } from './constants';

/* the Node.js based HTTP client. */
const requestor = new NodeRequestor();

const scope = 'openid';

export class AuthFlow {
  private notifier: AuthorizationNotifier;

  private authorizationHandler: AuthorizationRequestHandler;

  private tokenHandler: TokenRequestHandler;

  readonly authStateEmitter: AuthStateEmitter;

  // state
  private configuration: AuthorizationServiceConfiguration | undefined;

  private refreshToken: string | undefined;

  private accessTokenResponse: TokenResponse | undefined;

  // Cancel the authorization request (and shut down the server) if not completed within signInTimeout
  private signInTimer: NodeJS.Timeout | undefined;
  // to store the tokenExpired timeout 
  private onTokenExpiredTimeout: NodeJS.Timeout | undefined;

  // Define the behaviour when access token expires
  onTokenExpired: (() => void) | undefined;

  constructor() {
    this.notifier = new AuthorizationNotifier();
    this.authStateEmitter = new AuthStateEmitter();
    this.authorizationHandler = new NodeBasedHandler(port as number);
    this.tokenHandler = new BaseTokenRequestHandler(requestor);
    // set notifier to deliver responses
    this.authorizationHandler.setAuthorizationNotifier(this.notifier);
    // set a listener to listen for authorization responses
    // make refresh and access token requests.
    this.notifier.setAuthorizationListener((request, response, error) => {
      if (response) {
        let codeVerifier: string | undefined;
        if (request.internal && request.internal.code_verifier) {
          codeVerifier = request.internal.code_verifier;
        }

        this.makeRefreshTokenRequest(response.code, codeVerifier)
          .then(() => {
            this.authStateEmitter.emit(AuthStateEmitter.ON_TOKEN_RESPONSE);
          });
      }
    });
  }

  fetchServiceConfiguration(): Promise<void> {
    const {openIDConnectParams} = useOpenIDConnectState.getState()
    return AuthorizationServiceConfiguration.fetchFromIssuer(
      `${openIDConnectParams.base_url}/realms/${openIDConnectParams.realm}`,
      requestor
    ).then((response) => {
      this.configuration = response;
    });
  }

  makeAuthorizationRequest(username?: string) {
    const {openIDConnectParams} = useOpenIDConnectState.getState()
    // If authorization request is not completed within signInTimeout abort it
    this.signInTimer = setTimeout(()=>{
      this.abortExistingRequest();
    }, signInTimeout);

    if (!this.configuration) {
      return;
    }

    const extras: StringMap = { prompt: 'consent', access_type: 'offline' };
    if (username) {
      extras.login_hint = username;
    }

    // create a request
    const request = new AuthorizationRequest(
      {
        client_id: openIDConnectParams.clientId,
        redirect_uri: redirectUri,
        scope,
        response_type: AuthorizationRequest.RESPONSE_TYPE_CODE,
        state: undefined,
        extras,
      },
      new NodeCrypto()
    );

    this.authorizationHandler.performAuthorizationRequest(
      this.configuration,
      request
    );
  }

  /**
   * Performs cleanup of the AuthFlow instance, clearing timers and removing listeners.
   */
    private destroy() {
      // Clear signInTimer if it's set
      if (this.signInTimer) {
        clearTimeout(this.signInTimer);
        this.signInTimer = undefined;
      }

      if(this.onTokenExpiredTimeout){
        clearTimeout(this.onTokenExpiredTimeout)
        this.onTokenExpiredTimeout = undefined
      }
  
      // Remove onTokenExpired handler
      this.onTokenExpired = undefined;
  
      // Remove any event listeners from authStateEmitter
      this.authStateEmitter.removeAllListeners(AuthStateEmitter.ON_TOKEN_RESPONSE);
    }
  

  /**
   * Cancel the authorization request and shut down the server
   */
  async abortExistingRequest(): Promise<void> {
    try {
      // Cleanup
      this.destroy()
      // Dummy call to server on redirectUri to make it stop listening for a response
      await fetch(new Request(`${redirectUri}/?error=aborted`));
      // Wait server shutdown
      await new Promise((resolve) => setTimeout(resolve, waitServerShutdown));
    } catch (error) {
      // We should receive connection refused if the server is already down
      const connectionRefused = (error as any).cause?.code === 'ECONNREFUSED';
      if (!connectionRefused) {
        console.error('unexpected error', error);
      }
    }
  }

  private makeRefreshTokenRequest(
    code: string,
    codeVerifier: string | undefined
  ): Promise<void> {
    if (!this.configuration) {
      return Promise.resolve();
    }

    const extras: StringMap = {};

    if (codeVerifier) {
      extras.code_verifier = codeVerifier;
    }

    return this.updateToken('authorization',code, extras).then(()=>{})
  }

  loggedIn(): boolean {
    return !!this.accessTokenResponse && this.accessTokenResponse.isValid(0);
  }

  async signOut() {
    const {openIDConnectParams} = useOpenIDConnectState.getState()
    
    // Cleanup
    this.destroy()  
    // Revoke tokens
    var revokeRequests: RevokeTokenRequest[] = [];
    revokeRequests.push(
      new RevokeTokenRequest({
        client_id: openIDConnectParams.clientId,
        token_type_hint: "refresh_token",
        token: this.accessTokenResponse?.refreshToken as string
      })
    )
    revokeRequests.push(
      new RevokeTokenRequest({
        client_id: openIDConnectParams.clientId,
        token_type_hint: "access_token",
        token: this.accessTokenResponse?.accessToken as string
      })
    )
    for(const request of revokeRequests){
      this.tokenHandler.performRevokeTokenRequest(this.configuration as AuthorizationServiceConfiguration, request)
      .catch(e=>console.log('Token revoke request error', e))
    }
    // Call logout endpoint
    await fetch(
      `${openIDConnectParams.base_url}/realms/${openIDConnectParams.realm}/protocol/openid-connect/logout\
      ?client_id=${openIDConnectParams.clientId}\
      &id_token_hint=${this.accessTokenResponse?.idToken}`
    )
    // forget all cached token state
    this.accessTokenResponse = undefined;
  }

  performWithFreshTokens(): Promise<string> {
    if (!this.configuration) {
      return Promise.reject('Unknown service configuration');
    }
    if (!this.refreshToken) {
      return Promise.resolve('Missing refreshToken.');
    }
    if (this.accessTokenResponse && this.accessTokenResponse.isValid(0)) {
      // do nothing
      return Promise.resolve(this.accessTokenResponse.accessToken);
    }
    return this.updateToken('refresh') as Promise<string>
  }

  // The following methods and logic are inspired by keycloak-js 22.0.0 implementation:
  // https://github.com/keycloak/keycloak/blob/b825ba97b489d715f7ca1984c19bd95afb355a38/js/libs/keycloak-js/src/keycloak.js

  /**
   * Handles onTokenExpired event
   * @param accessToken 
   * @param timeLocal token request time
   */
  handleOnTokenExpired(accessToken: string, timeLocal: number){
    if(accessToken){
      const tokenParsed = this.decodeToken(accessToken)
      // Estimated delay between server and client in seconds
      const timeSkew = Math.floor(timeLocal / 1000) - tokenParsed.iat;
      if (this.onTokenExpired) {
        // Access token expiration time in seconds
        var expiresIn = (tokenParsed['exp'] - (new Date().getTime() / 1000) + timeSkew) * 1000;
        if (expiresIn <= 0) {
            this.onTokenExpired();
        } else {
          this.onTokenExpiredTimeout = setTimeout(this.onTokenExpired, expiresIn);
        }
    }
    }
  }

  /**
   * Refresh the token
   * @param action the action to perform 'refresh' | 'authorization'
   * @param code 
   * @param extras 
   * @returns 
   */
  updateToken(action: 'refresh' | 'authorization', code?: string, extras?: StringMap){
    const {openIDConnectParams} = useOpenIDConnectState.getState()
    let timeLocal = new Date().getTime()
    const request = action === 'refresh' ?
    new TokenRequest({
      client_id: openIDConnectParams.clientId,
      redirect_uri: redirectUri,
      grant_type: GRANT_TYPE_REFRESH_TOKEN,
      code: undefined,
      refresh_token: this.refreshToken,
      extras: undefined,
    }) : action === 'authorization' ?
    new TokenRequest({
      client_id: openIDConnectParams.clientId,
      redirect_uri: redirectUri,
      grant_type: GRANT_TYPE_AUTHORIZATION_CODE,
      code,
      refresh_token: undefined,
      extras,
    }) : null 

    if(request){
      return this.tokenHandler
      .performTokenRequest(this.configuration as AuthorizationServiceConfiguration, request)
      .then((response) => {
        timeLocal = (timeLocal + new Date().getTime()) / 2
        this.accessTokenResponse = response;
        this.refreshToken = response.refreshToken;
        this.handleOnTokenExpired(response.accessToken, timeLocal)
        if(action==='refresh'){
          return response.accessToken;
        } else if (action==='authorization'){
          return response
        } else { 
        return Promise.reject(`Failed to refresh. updateToken 'action' property must be "refresh" or "authorization", found ${action}`);
        }
      })
      .catch((error)=>{
        return Promise.reject(`Failed to '${action}' with error ${error}`);
      })
    } else {
      return Promise.reject(`Failed to refresh, updateToken 'request' is null. This happens because 'action' property must be "refresh" or "authorization", found ${action}`);
    }
  }

  /**
   * Token decoder and parser
   * @param str token
   * @returns parsed token
   */
  private decodeToken(str) {
    str = str.split('.')[1];

    str = str.replace(/-/g, '+');
    str = str.replace(/_/g, '/');
    switch (str.length % 4) {
        case 0:
            break;
        case 2:
            str += '==';
            break;
        case 3:
            str += '=';
            break;
        default:
            throw 'Invalid token';
    }

    str = decodeURIComponent(encodeURIComponent(atob(str)));

    str = JSON.parse(str);
    return str;
  }
}
