import { BrowserWindow } from 'electron';
import { AuthFlow, AuthStateEmitter } from './flow';
import { signInTimeout } from './constants';
import { useOpenIDConnectState } from './config';
import { ElectronChannel } from '../electronChannels';

type UserInfo = {
  sub: string;
  preferred_username: string;
  email_verified: boolean;
  given_name: string;
  family_name: string;
};

class Auth {
  private mainWindow: BrowserWindow;
  private authFlow: AuthFlow = new AuthFlow();
  private userInfo: string | null = null;
  // Checks if the authentication procedure is already started, to avoid having multiple auth requests
  private authAlreadyStarted: boolean = false;
  // To resolve login promise if not completed within signInTimeout
  private signInTimer : NodeJS.Timeout | undefined;

  constructor(mainWindow: BrowserWindow) {
    this.mainWindow = mainWindow;
    this.authFlow.onTokenExpired = () => {
      this.authFlow.updateToken('refresh')
      .then((accessToken)=>{
        this.mainWindow.webContents.send(
          ElectronChannel.AUTH_AUTH_CONTEXT, 
          {authenticated: true, token: accessToken, userInfo: this.userInfo}
        )
      })
      .catch(()=>{
        this.mainWindow.webContents.send(
          ElectronChannel.AUTH_AUTH_CONTEXT, 
          {authenticated: false, token: null, userInfo:null}
        )
      })
    }
  }

  /**
   * Performs authorization request, retrieve the access token and emits `auth:authContext` events
   * @returns promise with auth state and token
   */
  async signIn() {
    const {openIDConnectParams} = useOpenIDConnectState.getState()
    return new Promise(async (resolve, reject) => {
      try {
        // When sign-in does not finish within signInTimeout resolve promise
        this.signInTimer = setTimeout(()=>{
          // Remove pending state for Sign-in
          this.authAlreadyStarted = false;
          resolve({ authenticated: false, token: null });
        }, signInTimeout);
        // Login if not already logged in and if no sign-in process is pending
        if (!this.authFlow.loggedIn() && !this.authAlreadyStarted) {
          await this.authFlow
            .fetchServiceConfiguration()
            .then(() => {
              this.authFlow.makeAuthorizationRequest()
              // Mark Sign-in procedure as pending
              this.authAlreadyStarted = true;
            })
        }
        // After login
        this.authFlow.authStateEmitter.on(
          AuthStateEmitter.ON_TOKEN_RESPONSE,
          async () => {
            try {
              // Get the access token
              const accessToken = await this.authFlow.performWithFreshTokens();
              // Get user informations
              const request = new Request(
                `${openIDConnectParams.base_url}/realms/${openIDConnectParams.realm}/protocol/openid-connect/userinfo`,
                {
                  headers: new Headers({ Authorization: `Bearer ${accessToken}` }),
                  method: 'GET',
                  cache: 'no-cache',
                }
              );
        
              const result = await fetch(request);
              const userInfo = await result.json();
              this.userInfo = userInfo;
              // Clear timeout if sign-in is completed
              clearTimeout(this.signInTimer);
              resolve({ authenticated: true, token: accessToken });
              this.mainWindow.webContents.send(
                ElectronChannel.AUTH_AUTH_CONTEXT, 
                {authenticated: true, token: accessToken, userInfo: userInfo }
              )
            } catch (error) {
              this.mainWindow.webContents.send(
                ElectronChannel.AUTH_AUTH_CONTEXT, 
                {authenticated: false, token: null, userInfo: null }
              )
              reject(error);
            }
          }
        );
      } catch (error) {
          this.mainWindow.webContents.send(
            ElectronChannel.AUTH_AUTH_CONTEXT, 
            {authenticated: false, token: null, userInfo: null }
          )
          resolve({ authenticated: false, token: null });
      }
    });
  }

  /**
   * Performs cleanup of the Auth instance, clearing timers and removing listeners.
   */
  private destroy() {
    // Clear signInTimer if it's set
    if (this.signInTimer) {
      clearTimeout(this.signInTimer);
      this.signInTimer = undefined;
    }

    // Reset authAlreadyStarted flag
    this.authAlreadyStarted = false;

    // Clean up other state variables if necessary
    this.userInfo = null;
  }

  async signOut(){
    try{
      await this.authFlow.signOut()
      this.mainWindow.webContents.send(
        ElectronChannel.AUTH_AUTH_CONTEXT, 
        {authenticated: false, token: null, userInfo: null }
      )
      // Cleanup
      this.destroy()
    } catch (error){
      console.error("Failed to sign out", error)
    }
  }

  async abortSignIn(){
    try{
      await this.authFlow.abortExistingRequest()
      // Cleanup
      this.destroy()
    } catch (error){
      console.error("Failed to abort sign in", error)
    }
  }

}

export { Auth, UserInfo };
