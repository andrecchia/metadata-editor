// Sign in timeout in milliseconds
export const signInTimeout = 60000
// Wait server to properly shut down in milliseconds
export const waitServerShutdown = 3000