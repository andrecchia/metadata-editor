export const LOCAL_METASTORE = 'Local MetaStore instance'
export const METAREPO = 'MetaRepo'
export const MATWERK = 'NFDI MatWerk'

export const MetaStoreInstancesAvailable = 
  process.env.NODE_ENV  === 'development' || process.env.DEBUG_PROD ?
  [
    LOCAL_METASTORE,
    METAREPO,
    MATWERK
  ] : 
  [
    METAREPO,
    MATWERK
  ]

export const MetaStoreInstancesURL = 
  process.env.NODE_ENV === 'development' ?
  {
    [LOCAL_METASTORE]: '/MetaRepoAPILocal',
    [METAREPO]: '/MetaRepoAPI',
    [MATWERK]: '/NFDI-MatWerkAPI'
  } : 
   process.env.DEBUG_PROD ? 
   {
    [LOCAL_METASTORE]: 'http://metastore4docker:8040/api/v1',
    [METAREPO]: 'https://metarepo.nffa.eu/api/v1',
    [MATWERK]: 'https://matwerk.datamanager.kit.edu/api/v1'
  } :
  {
    [METAREPO]: 'https://metarepo.nffa.eu/api/v1',
    [MATWERK]: 'https://matwerk.datamanager.kit.edu/api/v1'
  }

export const MetaStoreInstanceDomainMap = {
    [LOCAL_METASTORE]: 'https://metarepo.nffa.eu',
    [METAREPO]: 'https://metarepo.nffa.eu',
    [MATWERK]: 'https://matwerk.datamanager.kit.edu'
}

export const KeycloakConfigs =  
  process.env.NODE_ENV === 'development' ?
  {
    [LOCAL_METASTORE]: {
      base_url: "http://localhost:8080/auth",
      realm: 'TEST',
      clientId: 'metastore'
    },
    [METAREPO]: {
      base_url: 'https://auth.nffa.eu/auth',
      realm: 'NEP',
      clientId: 'metastore'
    },
    [MATWERK]: {
      base_url: 'https://gateway.datamanager.kit.edu:8443',
      realm: 'nfdi4matwerk',
      clientId: 'kitdm-services'
    }
    // These are used in some cases for testing, I'll leave it here coomented out
    // [METAREPO]: {
    //   base_url: 'http://localhost:8080/auth',
    //   realm: 'NEP',
    //   clientId: 'metastore'
    // },
    // [MATWERK]: {
    //   base_url: 'http://localhost:8081/auth',
    //   realm: 'MW',
    //   clientId: 'metastore'
    // }
  } : 
  process.env.DEBUG_PROD ?
  {
    [LOCAL_METASTORE]: {
      base_url: "http://test-keycloak:8080/auth",
      realm: 'TEST',
      clientId: 'metastore'
    },
    [METAREPO]: {
      base_url: 'https://auth.nffa.eu/auth',
      realm: 'NEP',
      clientId: 'metastore'
    },
    [MATWERK]: {
      base_url: 'https://gateway.datamanager.kit.edu:8443',
      realm: 'nfdi4matwerk',
      clientId: 'kitdm-services'
    }
  } :
  {
    [METAREPO]: {
      base_url: 'https://auth.nffa.eu/auth',
      realm: 'NEP',
      clientId: 'metastore'
    },
    [MATWERK]: {
      base_url: 'https://gateway.datamanager.kit.edu:8443',
      realm: 'nfdi4matwerk',
      clientId: 'kitdm-services'
    }
  }
