import {
  and,
  ControlProps,
  isEnumControl,
  optionIs, OwnPropsOfEnum, RankedTester, rankWith
} from '@jsonforms/core';
import {  withJsonFormsEnumProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialRadioGroupControl } = Unwrapped;

export const MaterialRadioGroupControlWithCheck = (props: ControlProps & OwnPropsOfEnum) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialRadioGroupControl} />
);


export const materialRadioGroupControlWithCheckTester: RankedTester = rankWith(
  20+100,
  and(isEnumControl, optionIs('format', 'radio'))
);
export default withJsonFormsEnumProps(MaterialRadioGroupControlWithCheck);
