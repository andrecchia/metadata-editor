import {
  isBooleanControl,
  RankedTester,
  rankWith,
  ControlProps,
  optionIs,
  and
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialBooleanToggleControl } = Unwrapped;

export const MaterialBooleanToggleControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialBooleanToggleControl} />
);

export const materialBooleanToggleControlWithCheckTester: RankedTester = rankWith(
  3+100,
  and(isBooleanControl, optionIs('toggle', true))
);

export default withJsonFormsControlProps(MaterialBooleanToggleControlWithCheck);
