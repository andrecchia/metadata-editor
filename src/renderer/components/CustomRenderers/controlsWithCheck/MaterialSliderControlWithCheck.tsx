import {
  ControlProps,
  isRangeControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialSliderControl } = Unwrapped;

export const MaterialSliderControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialSliderControl} />
);

export const materialSliderControlWithCheckTester: RankedTester = rankWith(
  4+100,
  isRangeControl
);

export default withJsonFormsControlProps(MaterialSliderControlWithCheck);
