
import {
  and,
  ControlProps,
  ControlState,
  JsonSchema,
  RankedTester,
  rankWith,
  schemaMatches,
  uiTypeIs,
} from '@jsonforms/core';
import { Control, withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialAnyOfStringOrEnumControl } = Unwrapped;

// export const MaterialAnyOfStringOrEnumControlWithCheck = (props: ControlProps) => (
//   <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialAnyOfStringOrEnumControl} />
// );

export class MaterialAnyOfStringOrEnumControlWithCheck extends Control<
  ControlProps,
  ControlState
> {
  render() {
    return (
      <MaterialControlWithCheck {...this.props} MaterialTypeControl={MaterialAnyOfStringOrEnumControl} />

    );
  }
}

const findEnumSchema = (schemas: JsonSchema[]) =>
  schemas.find(
    s => s.enum !== undefined && (s.type === 'string' || s.type === undefined)
  );
const findTextSchema = (schemas: JsonSchema[]) =>
  schemas.find(s => s.type === 'string' && s.enum === undefined);

const hasEnumAndText = (schemas: JsonSchema[]) => {
  // idea: map to type,enum and check that all types are string and at least one item is of type enum,
  const enumSchema = findEnumSchema(schemas);
  const stringSchema = findTextSchema(schemas);
  const remainingSchemas = schemas.filter(
    s => s !== enumSchema || s !== stringSchema
  );
  const wrongType = remainingSchemas.find(s => s.type && s.type !== 'string');
  return enumSchema && stringSchema && !wrongType;
};
const simpleAnyOf = and(
  uiTypeIs('Control'),
  schemaMatches(
    schema => schema.hasOwnProperty('anyOf') && hasEnumAndText(schema.anyOf as JsonSchema[])!
  )
);

export const materialAnyOfStringOrEnumControlWithCheckTester: RankedTester = rankWith(
  5+100,
  simpleAnyOf
);
export default withJsonFormsControlProps(MaterialAnyOfStringOrEnumControlWithCheck);
