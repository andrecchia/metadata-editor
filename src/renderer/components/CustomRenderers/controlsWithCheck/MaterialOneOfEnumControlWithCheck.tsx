import React from 'react';
import {
  ControlProps,
  isOneOfEnumControl,
  OwnPropsOfEnum,
  RankedTester,
  rankWith,
} from '@jsonforms/core';
import { TranslateProps, withJsonFormsOneOfEnumProps, withTranslateProps } from '@jsonforms/react';
import { WithOptionLabel } from '@jsonforms/material-renderers/lib/mui-controls/MuiAutocomplete';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialOneOfEnumControl } = Unwrapped;

export const MaterialOneOfEnumControlWithCheck = (props: ControlProps & OwnPropsOfEnum & WithOptionLabel & TranslateProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialOneOfEnumControl} />
);


export const materialOneOfEnumControlWithCheckTester: RankedTester = rankWith(
  5+100,
  isOneOfEnumControl
);

// HOC order can be reversed with https://github.com/eclipsesource/jsonforms/issues/1987
export default withJsonFormsOneOfEnumProps(withTranslateProps(React.memo(MaterialOneOfEnumControlWithCheck)), false);
