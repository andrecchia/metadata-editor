import {
  ControlProps,
  isDateControl,
  isTimeControl,
  or,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialNativeControl } = Unwrapped;

export const MaterialNativeControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialNativeControl} />
);

export const materialNativeControlWithCheckTester: RankedTester = rankWith(
  2+100,
  or(isDateControl, isTimeControl)
);

export default withJsonFormsControlProps(MaterialNativeControlWithCheck);
