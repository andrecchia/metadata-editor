import React, { useState, useCallback } from 'react';
import { Grid } from '@mui/material';
import { electronStore, useHandleFieldsCheckState } from '../util/lockedUtils';
import get from 'lodash/get';
import { getSchemaMaskId } from '../../../utils/utils';
import { MaskCheckbox } from 'renderer/components/widgets/MaskCheckbox';

/**
 * This component renders a specific material control (e.g. MaterialTextControl or MateriaIntegerControl),
 * defined with memo so that doesn't re-renders when hidden and shown
 */
const MaterialTypeControlGridComponent = React.memo((
  props: any &  {checkFields: boolean} &
  { MaterialTypeControl: (typeControlProps: any) => JSX.Element }  ) =>{
    const {MaterialTypeControl,  checkFields ,...typeControlProps }=props;
  return(
    <Grid 
    item 
    // This column take the whole space (i.e. 12 ) if the user is in the 'normal' mode
    // (that is: only lock icon closed visible; checkboxes, clear and hide buttons hidden )
    xs={!checkFields ? 12 : 11}
    // disable pointer events if checked 
    // style={checkFields && !checked ? {pointerEvents: 'none'} : {}}
  >
      <MaterialTypeControl {...typeControlProps} />
  </Grid>
  )
})

interface InnerMaterialControlWithCheckProps {
  checkFields: boolean;
  checked: boolean|undefined;
  hideUncheckedFields: boolean;
  handleCheck: (event: React.ChangeEvent<HTMLInputElement>) => void;
  MaterialTypeControl: (typeControlProps: any) => JSX.Element;
}
const InnerMaterialControlWithCheck = React.memo((
  props: 
  any & InnerMaterialControlWithCheckProps
)=>{
  const {
    MaterialTypeControl, 
    checkFields, 
    checked, 
    hideUncheckedFields, 
    handleCheck, 
    ...typeControlProps 
  }: InnerMaterialControlWithCheckProps = props;
  return(
    <Grid 
    container 
    spacing={1}
    justifyContent="center"
    alignItems="center"
    sx={{ display: hideUncheckedFields && !checked ? { xl: 'none', xs: 'none' } : {}}}
    >
      <Grid 
        item 
        xs={1}
        // Specular to xs property in the above <Grid>
        display = {!checkFields ? {xs: "none", lg: "none"} : {}}
      >
        {/* One checkbox for each field */}
          <MaskCheckbox checked={checked === undefined ? false : checked} onChange={handleCheck} />
      </Grid>
      <MaterialTypeControlGridComponent checkFields={checkFields} MaterialTypeControl={MaterialTypeControl} {...typeControlProps}/>
    </Grid>
)
})

/**
 * This is a general element that takes a specific material control as argument
 * (e.g. MaterialTextControl or MateriaIntegerControl) and wrap it adding a Checkbox,
 * that is used to hide, clear and lock the fields
 * @param props union of two parameters: 1) the (unwrapped) material control for a specific type (e.g. MaterialTextControl) and 2) whatever props it takes as parameter
 * @returns the wrapped material control for a specific type
 */
export const MaterialControlWithCheck = React.memo((
  props: any & 
  { MaterialTypeControl: (typeControlProps: any) => JSX.Element } ) => {
    
    const {MaterialTypeControl ,...typeControlProps }=props;

    const schemaMaskId = getSchemaMaskId();
    // check and change array path, remove the '.i.' index for i-th entry
    const arrayPathPattern = /\.\d+\./g
    const pathNoArray = props.path.replace(arrayPathPattern, '.');
    // Get the mask value from electron store if exists (otherwise is undefined)
    const maskValue = get(electronStore.get(schemaMaskId), pathNoArray);
    // Stores and updates the checkbox status for the input fields. If undefined assign false as a defauls value
    const [checked, setChecked] = useState<boolean|undefined>( maskValue === undefined ? false : maskValue );
  
    /**
     * Update the lockedSchema object that stores the checkbox status for the iput fields
     * as key value pairs, where key is the input field path, value is the checkbox value. 
     * lockedSchema object example: 
     * { person.firstName: true,
     * address.zipCode: false }
     * 
     * @param event Click that set true/false the checkbox for the input fields
     */
    const handleCheck = useCallback(
      (event: React.ChangeEvent<HTMLInputElement>) => {
        const newCheckStatus = event.target.checked;
        setChecked(newCheckStatus);
        electronStore.set(schemaMaskId + '.' + pathNoArray, newCheckStatus);
      },
      [props.path, schemaMaskId, setChecked]
    );
    /**
     * Object storing checking, hiding and clearing variables.
     * These variables comes from the root of the project
     */
    const [fieldsCheckDict,_] =  useHandleFieldsCheckState();
    return <InnerMaterialControlWithCheck 
      checkFields={fieldsCheckDict.checkFields}
      hideUncheckedFields={fieldsCheckDict.hideUncheckedFields}
      checked={checked as boolean}
      handleCheck={handleCheck}
      MaterialTypeControl={MaterialTypeControl}
      {...typeControlProps}
    />
    
})