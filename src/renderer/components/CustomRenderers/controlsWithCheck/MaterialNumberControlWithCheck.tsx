import {
  ControlProps,
  isNumberControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialNumberControl } = Unwrapped;

export const MaterialNumberControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialNumberControl} />
);

export const materialNumberControlWithCheckTester: RankedTester = rankWith(
  2+100,
  isNumberControl
);

export default withJsonFormsControlProps(MaterialNumberControlWithCheck);
