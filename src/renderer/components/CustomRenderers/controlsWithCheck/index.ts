import MaterialBooleanControlWithCheck, {
    materialBooleanControlWithCheckTester
  } from './MaterialBooleanControlWithCheck';
  import MaterialBooleanToggleControlWithCheck, {
    materialBooleanToggleControlWithCheckTester
  } from './MaterialBooleanToggleControlWithCheck';
  import MaterialEnumControlWithCheck, {
    materialEnumControlWithCheckTester
  } from './MaterialEnumControlWithCheck';
  import MaterialNativeControlWithCheck, {
    materialNativeControlWithCheckTester
  } from './MaterialNativeControlWithCheck';
  import MaterialDateControlWithCheck, {
    materialDateControlWithCheckTester
  } from './MaterialDateControlWithCheck';
  import MaterialDateTimeControlWithCheck, {
    materialDateTimeControlWithCheckTester
  } from './MaterialDateTimeControlWithCheck';
  import MaterialTimeControlWithCheck, {
    materialTimeControlWithCheckTester
  } from './MaterialTimeControlWithCheck';
  import MaterialSliderControlWithCheck, {
    materialSliderControlWithCheckTester
  } from './MaterialSliderControlWithCheck';
  import MaterialRadioGroupControlWithCheck, {
    materialRadioGroupControlWithCheckTester
  } from './MaterialRadioGroupControlWithCheck';
  import MaterialIntegerControlWithCheck, {
    materialIntegerControlWithCheckTester
  } from './MaterialIntegerControlWithCheck';
  import MaterialNumberControlWithCheck, {
    materialNumberControlWithCheckTester
  } from './MaterialNumberControlWithCheck';
  import MaterialTextControlWithCheck, {
    materialTextControlWithCheckTester
  } from './MaterialTextControlWithCheck';
  
  import MaterialAnyOfStringOrEnumControlWithCheck, {
    materialAnyOfStringOrEnumControlWithCheckTester
  } from './MaterialAnyOfStringOrEnumControlWithCheck';
  
  import MaterialOneOfEnumControlWithCheck, {
    materialOneOfEnumControlWithCheckTester
  } from './MaterialOneOfEnumControlWithCheck';
  
  import MaterialOneOfRadioGroupControlWithCheck, {
    materialOneOfRadioGroupControlWithCheckTester
  } from './MaterialOneOfRadioGroupControlWithCheck';
import { JsonFormsRendererRegistryEntry } from '@jsonforms/core';
  
export {
    MaterialBooleanControlWithCheck,
    materialBooleanControlWithCheckTester,
    MaterialBooleanToggleControlWithCheck,
    materialBooleanToggleControlWithCheckTester,
    MaterialEnumControlWithCheck,
    materialEnumControlWithCheckTester,
    MaterialNativeControlWithCheck,
    materialNativeControlWithCheckTester,
    MaterialDateControlWithCheck,
    materialDateControlWithCheckTester,
    MaterialDateTimeControlWithCheck,
    materialDateTimeControlWithCheckTester,
    MaterialTimeControlWithCheck,
    materialTimeControlWithCheckTester,
    MaterialSliderControlWithCheck,
    materialSliderControlWithCheckTester,
    MaterialRadioGroupControlWithCheck,
    materialRadioGroupControlWithCheckTester,
    MaterialIntegerControlWithCheck,
    materialIntegerControlWithCheckTester,
    MaterialNumberControlWithCheck,
    materialNumberControlWithCheckTester,
    MaterialTextControlWithCheck,
    materialTextControlWithCheckTester,
    MaterialAnyOfStringOrEnumControlWithCheck,
    materialAnyOfStringOrEnumControlWithCheckTester,
    MaterialOneOfEnumControlWithCheck,
    materialOneOfEnumControlWithCheckTester,
    MaterialOneOfRadioGroupControlWithCheck,
    materialOneOfRadioGroupControlWithCheckTester
  };
  
export const materialRenderersWithCheck: JsonFormsRendererRegistryEntry[] = [
    // controls
    { tester: materialAnyOfStringOrEnumControlWithCheckTester, renderer: MaterialAnyOfStringOrEnumControlWithCheck },
    { tester: materialBooleanControlWithCheckTester, renderer: MaterialBooleanControlWithCheck },
    { tester: materialBooleanToggleControlWithCheckTester, renderer: MaterialBooleanToggleControlWithCheck },
    { tester: materialDateControlWithCheckTester, renderer: MaterialDateControlWithCheck },
    { tester: materialDateTimeControlWithCheckTester, renderer: MaterialDateTimeControlWithCheck },
    { tester: materialEnumControlWithCheckTester, renderer: MaterialEnumControlWithCheck },
    { tester: materialIntegerControlWithCheckTester, renderer: MaterialIntegerControlWithCheck },
    { tester: materialNativeControlWithCheckTester, renderer: MaterialNativeControlWithCheck },
    { tester: materialNumberControlWithCheckTester, renderer: MaterialNumberControlWithCheck },
    { tester: materialOneOfEnumControlWithCheckTester, renderer: MaterialOneOfEnumControlWithCheck },
    { tester: materialOneOfRadioGroupControlWithCheckTester,renderer: MaterialOneOfRadioGroupControlWithCheck },
    { tester: materialRadioGroupControlWithCheckTester, renderer: MaterialRadioGroupControlWithCheck},
    { tester: materialSliderControlWithCheckTester, renderer: MaterialSliderControlWithCheck },
    { tester: materialTextControlWithCheckTester, renderer: MaterialTextControlWithCheck },
    { tester: materialTimeControlWithCheckTester, renderer: MaterialTimeControlWithCheck }, 
]

export * from './MaterialControlWithCheck';