import {
  ControlProps,
  isDateControl,
  RankedTester,
  rankWith,
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialDateControl } = Unwrapped;

export const MaterialDateControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialDateControl} />
);

export const materialDateControlWithCheckTester: RankedTester = rankWith(
  4+100,
  isDateControl
);

export default withJsonFormsControlProps(MaterialDateControlWithCheck);
