import {
  ControlProps,
  isStringControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
import React from 'react';
const { MaterialTextControl } = Unwrapped;

export const MaterialTextControlWithCheck = React.memo((props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialTextControl} />
));
export const materialTextControlWithCheckTester: RankedTester = rankWith(
  1+100,
  isStringControl
);
export default withJsonFormsControlProps(MaterialTextControlWithCheck);
