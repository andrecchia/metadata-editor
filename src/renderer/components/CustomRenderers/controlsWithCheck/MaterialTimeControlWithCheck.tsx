import {
  ControlProps,
  isTimeControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialTimeControl } = Unwrapped;

export const MaterialTimeControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialTimeControl} />
);

export const materialTimeControlWithCheckTester: RankedTester = rankWith(
  4+100,
  isTimeControl
);

export default withJsonFormsControlProps(MaterialTimeControlWithCheck);
