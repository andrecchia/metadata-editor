import {
    and,
  ControlProps,
  isOneOfEnumControl,
  optionIs,
  OwnPropsOfEnum,
  RankedTester,
  rankWith,
} from '@jsonforms/core';
import { withJsonFormsOneOfEnumProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialOneOfRadioGroupControl } = Unwrapped;

export const MaterialOneOfRadioGroupControlWithCheck = (props: ControlProps & OwnPropsOfEnum) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialOneOfRadioGroupControl} />
);

export const materialOneOfRadioGroupControlWithCheckTester: RankedTester = rankWith(
  20+100,
  and(isOneOfEnumControl, optionIs('format', 'radio'))
);

export default withJsonFormsOneOfEnumProps(MaterialOneOfRadioGroupControlWithCheck);
