import {
  ControlProps,
  isDateTimeControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialDateTimeControl } = Unwrapped;

export const MaterialDateTimeControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialDateTimeControl} />
);
export const materialDateTimeControlWithCheckTester: RankedTester = rankWith(
  2+100,
  isDateTimeControl
);

export default withJsonFormsControlProps(MaterialDateTimeControlWithCheck);
