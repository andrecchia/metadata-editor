import {
  ControlProps,
  isIntegerControl,
  RankedTester,
  rankWith
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialIntegerControl } = Unwrapped;

export const MaterialIntegerControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialIntegerControl} />
);

export const materialIntegerControlWithCheckTester: RankedTester = rankWith(
  2+100,
  isIntegerControl
);
export default withJsonFormsControlProps(MaterialIntegerControlWithCheck);
