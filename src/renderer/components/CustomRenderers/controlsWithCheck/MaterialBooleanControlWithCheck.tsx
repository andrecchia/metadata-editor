
import {
  isBooleanControl,
  RankedTester,
  rankWith,
  ControlProps
} from '@jsonforms/core';
import { withJsonFormsControlProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from './MaterialControlWithCheck';
import { Unwrapped } from '@jsonforms/material-renderers';
const { MaterialBooleanControl } = Unwrapped;

export const MaterialBooleanControlWithCheck = (props: ControlProps) => (
  <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialBooleanControl} />
);

export const materialBooleanControlWithCheckTester: RankedTester = rankWith(
  2+100,
  isBooleanControl
);
export default withJsonFormsControlProps(MaterialBooleanControlWithCheck);
