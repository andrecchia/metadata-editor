import { createContext, useState, useContext, ReactNode } from 'react';

export const electronStore = window.electron.store;

const initialState={
    checkFields: false,
    hideUncheckedFields: false
  };

const useInitState = () => useState(initialState);

export const handleFieldsCheck = createContext<ReturnType<typeof useInitState> | null>(null);

/**
 * useContext wrapper for the checking status
 * @returns the current context (checking status in this case) value.
 */
export const useHandleFieldsCheckState = () => {
  const value = useContext(handleFieldsCheck);
  if (value === null) throw new Error('Please add SharedStateProvider');
  return value;
};

/** 
 * This Provider provides the checking status that will be catched
 * by its children, and in general to any descendant, with useHandleFieldsCheckState.  
 */
export const HandleFieldsCheckProvider = ({ children }: { children: ReactNode }) => {
    return(
        <handleFieldsCheck.Provider value={useInitState()}> 
            {children}
        </handleFieldsCheck.Provider>
  );
}