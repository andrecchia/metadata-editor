import {
    ControlProps,
    DispatchPropsOfMultiEnumControl,
    OwnPropsOfEnum,
    Paths
  } from '@jsonforms/core';
  import { MuiCheckbox } from '@jsonforms/material-renderers';
  import {
    FormControl,
    FormControlLabel,
    FormGroup,
    FormHelperText,
    Hidden,
  } from '@mui/material';
  import isEmpty from 'lodash/isEmpty';
  
  export const MaterialEnumArrayRenderer = ({
    schema,
    visible,
    errors,
    path,
    options,
    data,
    addItem,
    removeItem,
    handleChange: _handleChange,
    ...otherProps
  }: ControlProps & OwnPropsOfEnum & DispatchPropsOfMultiEnumControl) => {
    return (
      <Hidden xsUp={!visible}>
        <FormControl component='fieldset'>
          <FormHelperText error>{errors}</FormHelperText>
          <FormGroup row>
            {options?.map((option: any, index: number) => {
              const optionPath = Paths.compose(path, `${index}`);
              const checkboxValue = data?.includes(option.value)
                ? option.value
                : undefined;
              return (
                    <FormControlLabel
                      id={option.value}
                      key={option.value}
                      control={
                        <MuiCheckbox
                          key={'checkbox-' + option.value}
                          isValid={isEmpty(errors)}
                          path={optionPath}
                          handleChange={(_childPath, newValue) =>
                            newValue
                              ? addItem(path, option.value)
                              : removeItem!(path, option.value)
                          }
                          data={checkboxValue}
                          errors={errors}
                          schema={schema}
                          visible={visible}
                          {...otherProps}
                        />
                      }
                      label={option.label}
                      required={false}
                    />
              );
            })}
          </FormGroup>
        </FormControl>
      </Hidden>
    );
  };
  