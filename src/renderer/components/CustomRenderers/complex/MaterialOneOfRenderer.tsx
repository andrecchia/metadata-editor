/*
  The MIT License
  
  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import { useCallback, useEffect, useState } from 'react';
import isEmpty from 'lodash/isEmpty';

import {
  CombinatorRendererProps,
  createCombinatorRenderInfos,
  createDefaultValue,
  isOneOfControl,
  JsonSchema,
  OwnPropsOfControl,
  RankedTester,
  rankWith,
} from '@jsonforms/core';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Hidden
} from '@mui/material';
import { JsonFormsDispatch, withJsonFormsOneOfProps } from '@jsonforms/react';
import { CombinatorProperties } from '@jsonforms/material-renderers';
import { getSchemaMaskId } from 'renderer/utils/utils';
import getClosestMatchingOption from './matchingOptions';
import { Dropdown } from 'renderer/components/widgets/selectSchemaProperties';

export interface OwnOneOfProps extends OwnPropsOfControl {
  indexOfFittingSchema?: number;
}

export const MaterialOneOfRenderer = ({
  handleChange,
  schema,
  path,
  renderers,
  cells,
  rootSchema,
  id,
  visible,
  uischema,
  uischemas,
  data,
}: CombinatorRendererProps) => {
  const [open, setOpen] = useState(false);
  const oneOfRenderInfos = createCombinatorRenderInfos(
    (schema as JsonSchema).oneOf!,
    rootSchema,
    'oneOf',
    uischema,
    path,
    uischemas
  );
  const initialIndex = getClosestMatchingOption(schema,data,oneOfRenderInfos.map(item => item.schema))
  const [selectedIndex, setSelectedIndex] = useState<undefined|number>(initialIndex);
  const [newSelectedIndex, setNewSelectedIndex] = useState(0);
  const handleClose = useCallback(() => setOpen(false), [setOpen]);
  const cancel = useCallback(() => {
    setOpen(false);
  }, [setOpen]);
  const oneOfLabels = oneOfRenderInfos.map(item => item.label)
  const [selectedOneOfLabel, setSelectedOneOfLabel] = useState<string>(oneOfLabels[initialIndex || 0]);

  const openNewTab = (newIndex: number) => {
    handleChange(path, createDefaultValue(oneOfRenderInfos[newIndex].schema, rootSchema));
    setSelectedIndex(newIndex);
    setSelectedOneOfLabel(oneOfLabels[newIndex])
  };

  const confirm = useCallback(() => {
    openNewTab(newSelectedIndex);
    setOpen(false);
  }, [handleChange, createDefaultValue, newSelectedIndex]);

  const handleDropdownChange = useCallback(
    (_event: any) => {
      const selectedLabel: string = _event.target.value
      const newOneOfIndex = oneOfLabels.indexOf(selectedLabel)
      setNewSelectedIndex(newOneOfIndex);
      if (isEmpty(data)) {
        openNewTab(newOneOfIndex);
      } else {
        setOpen(true);
      }
    },
    [setOpen, setSelectedIndex, data]
  );

  // check and change array path, remove the '.i.' index for i-th entry
  const arrayPathPattern = /\.\d+\./g   //pattern matching .integer.
  const pathNoArray = path.replace(arrayPathPattern, '.');
  const schemaMaskId = getSchemaMaskId();
  useEffect(()=>{
    const lastDot = pathNoArray.lastIndexOf('.')
    const p = pathNoArray.substring(0, lastDot)
    const pf = pathNoArray.substring(lastDot+1)
    // Inserts a fake mask (see below) for `path.to.this.oneOf`
    // to trigger the insertion of `path.to.this` into the visibility group in the main process,
    // otherwise only `path.to.this.oneOf` is inserted.
    window.electron.maskHandler.insertIntoVisibilityState(
      // Fake mask
      { [pf]: {} },
      `${schemaMaskId}.${p as string}`
    )
    const index = getClosestMatchingOption(schema,data,oneOfRenderInfos.map(item => item.schema))

    if (isEmpty(data)) {
      openNewTab(index || 0);
    }
  },[])

  return (
    <Hidden xsUp={!visible}>
      <CombinatorProperties
        rootSchema={rootSchema}
        schema={schema}
        combinatorKeyword={'oneOf'}
        path={path}
      />
      <Dropdown
        id={path+'oneOf-dropdown'}
        value={selectedOneOfLabel}
        fieldName=''
        property={oneOfLabels}
        handleChange={handleDropdownChange}
        disabled={false}
        sx={{ width: '100%'}}
      />
      {oneOfRenderInfos.map(
        (oneOfRenderInfo, oneOfIndex) =>
          selectedIndex === oneOfIndex && (
            <JsonFormsDispatch
              key={oneOfIndex}
              schema={oneOfRenderInfo.schema}
              uischema={oneOfRenderInfo.uischema}
              path={path}
              renderers={renderers}
              cells={cells}
            />
          )
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>{'Clear form?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            Your data will be cleared if you navigate away from {selectedOneOfLabel}. Do you
            want to proceed?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={cancel} color='primary'>
            No
          </Button>
          <Button
            onClick={confirm}
            color='primary'
            autoFocus
            id={`oneOf-${id}-confirm-yes`}
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Hidden>
  );
};

export const materialOneOfControlTester: RankedTester = rankWith(
  100+3,
  isOneOfControl
);

export default withJsonFormsOneOfProps(MaterialOneOfRenderer);
