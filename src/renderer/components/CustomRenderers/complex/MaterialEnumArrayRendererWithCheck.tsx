import {
    ControlProps,
    DispatchPropsOfMultiEnumControl,
    JsonSchema,
    OwnPropsOfEnum,
    RankedTester,
    and,
    hasType,
    rankWith,
    resolveSchema,
    schemaMatches,
    schemaSubPathMatches,
    showAsRequired,
    uiTypeIs,
} from '@jsonforms/core';
import { withJsonFormsMultiEnumProps } from '@jsonforms/react';
import { MaterialControlWithCheck } from '../controlsWithCheck';
import InputLabel from '@mui/material/InputLabel';
import merge from 'lodash/merge';
import { MaterialEnumArrayRenderer } from './MaterialEnumArrayRenderer';

export const MaterialEnumArrayRendererWithCheck = 
    (props: ControlProps & OwnPropsOfEnum & DispatchPropsOfMultiEnumControl) => {
      const {
        config,
        uischema,
        errors,
        id,
        required,
        label
      } = props
      const appliedUiSchemaOptions = merge({}, config, uischema.options);
      const isValid = errors.length === 0;
        return (
        <>
          <InputLabel
            sx={{textAlign:'initial'}}
            htmlFor={id + '-material-enum-array-check'}
            error={!isValid}
            required={showAsRequired(
              required as boolean,
              appliedUiSchemaOptions.hideRequiredAsterisk
            )}
          >
            {label}
          </InputLabel>
          <MaterialControlWithCheck {...props} MaterialTypeControl={MaterialEnumArrayRenderer} />
        </>
        )
  }

const hasOneOfItems = (schema: JsonSchema): boolean =>
    schema.oneOf !== undefined &&
    schema.oneOf.length > 0 &&
    (schema.oneOf as JsonSchema[]).every((entry: JsonSchema) => {
        return entry.const !== undefined;
    });
    
const hasEnumItems = (schema: JsonSchema): boolean =>
    schema.type === 'string' && schema.enum !== undefined;

export const materialEnumArrayRendererWithCheckTester: RankedTester = rankWith(
    100 + 5,
    and(
      uiTypeIs('Control'),
      and(
        schemaMatches(
          (schema) =>
            hasType(schema, 'array') &&
            !Array.isArray(schema.items) &&
            schema.uniqueItems === true
        ),
        schemaSubPathMatches('items', (schema, rootSchema) => {
          const resolvedSchema = schema.$ref
            ? resolveSchema(rootSchema, schema.$ref, rootSchema)
            : schema;
          return hasOneOfItems(resolvedSchema) || hasEnumItems(resolvedSchema);
        })
      )
    )
  );

export default withJsonFormsMultiEnumProps(MaterialEnumArrayRendererWithCheck);