/*
  The MIT License

  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import React, { useCallback, useState } from 'react';
import {
  ArrayLayoutProps,
  RankedTester,
  isObjectArrayControl,
  isPrimitiveArrayControl,
  or,
  rankWith,
} from '@jsonforms/core';
import { withJsonFormsArrayLayoutProps } from '@jsonforms/react';
import { MaterialTableControl } from '@jsonforms/material-renderers';
import { Grid, Hidden } from '@mui/material';
import { DeleteDialog } from '@jsonforms/material-renderers';
import { getSchemaMaskId } from 'renderer/utils/utils';
import { electronStore, useHandleFieldsCheckState } from '../util/lockedUtils';
import get from 'lodash/get';
import { MaskCheckbox } from 'renderer/components/widgets/MaskCheckbox';

export const MaterialArrayControlRenderer = (props: ArrayLayoutProps) => {
  const [open, setOpen] = useState(false);
  const [path, setPath] = useState<string|undefined>(undefined);
  const [rowData, setRowData] = useState<number| undefined>(undefined);
  const { removeItems, visible } = props;

  const openDeleteDialog = useCallback(
    (p: string, rowIndex: number) => {
      setOpen(true);
      setPath(p);
      setRowData(rowIndex);
    },
    [setOpen, setPath, setRowData]
  );
  const deleteCancel = useCallback(() => setOpen(false), [setOpen]);
  const deleteConfirm = useCallback(() => {
    if(path){
        const p = path.substring(0, path.lastIndexOf('.'));
        removeItems!(p, [rowData as number])();
    }
    setOpen(false);
  }, [setOpen, path, rowData]);
  const deleteClose = useCallback(() => setOpen(false), [setOpen]);

  // Changes with respect to the original JSONForms renderer:
  // I am adding a checkbox and the functions to change the mask value based on the checkbox values

  const schemaMaskId = getSchemaMaskId();
  // check and change array path, remove the '.i.' index for i-th entry
  const arrayPathPattern = /\.\d+\./g   //pattern matching .integer.
  const pathNoArray = props.path.replace(arrayPathPattern, '.');
  const maskValue = get(electronStore.get(schemaMaskId), pathNoArray);
  // Stores and updates the checkbox status for the input fields. If undefined assign false as a defauls value
  const [checked, setChecked] = useState<boolean|undefined>( maskValue === undefined ? false : maskValue );

  /**
   * Update the lockedSchema object that stores the checkbox status for the iput fields
   * as key value pairs, where key is the input field path, value is the checkbox value. 
   * lockedSchema object example: 
   * { person.firstName: true,
   * address.zipCode: false }
   * 
   * @param event Click that set true/false the checkbox for the input fields
   */
  const handleCheck = useCallback(
      (event: React.ChangeEvent<HTMLInputElement>) => {
      const newCheckStatus = event.target.checked;
      setChecked(newCheckStatus);
      electronStore.set(schemaMaskId + '.' + pathNoArray, newCheckStatus);
      },
      [props.path, schemaMaskId, setChecked]
   );
  /**
     * Object storing checking, hiding and clearing variables.
     * These variables comes from the root of the project
     */
  const [fieldsCheckDict,_] =  useHandleFieldsCheckState();
  return (
    <Grid 
    container 
    spacing={1}
    justifyContent="center"
    alignItems="flex-end"
    sx={{ display: fieldsCheckDict.hideUncheckedFields && !checked ? { xl: 'none', xs: 'none' } : {}}}
    >
      <Grid 
        item 
        xs={1}
        // Specular to xs property in the above <Grid>
        display = {!fieldsCheckDict.checkFields ? {xs: "none", lg: "none"} : {}}
        style={{margin: 'auto'}}
      >
        {/* One checkbox for each field */}
        <MaskCheckbox checked={checked === undefined ? false : checked} onChange={handleCheck} />
      </Grid>
      <Grid 
        item 
        // This column take the whole space (i.e. 12 ) if the user is in the 'normal' mode
        // (that is: only lock icon closed visible; checkboxes, clear and hide buttons hidden )
        xs={!fieldsCheckDict.checkFields ? 12 : 11}
        // disable pointer events if checked 
        // style={checkFields && !checked ? {pointerEvents: 'none'} : {}}
       >
        <Hidden xsUp={!visible}>
        <MaterialTableControl {...props} openDeleteDialog={openDeleteDialog} />
        <DeleteDialog
            open={open}
            onCancel={deleteCancel}
            onConfirm={deleteConfirm}
            onClose={deleteClose}
            acceptText={props.translations.deleteDialogAccept as string}
            declineText={props.translations.deleteDialogDecline as string}
            title={props.translations.deleteDialogTitle as string}
            message={props.translations.deleteDialogMessage as string}
            />
        </Hidden>
       </Grid>
    </Grid>
  );
};

export const materialArrayControlTester: RankedTester = rankWith(
  100+3,
  or(isObjectArrayControl, isPrimitiveArrayControl)
);

export default withJsonFormsArrayLayoutProps(MaterialArrayControlRenderer);
