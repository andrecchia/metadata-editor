import { JsonSchema } from "@jsonforms/core";
import Ajv from "ajv";
import addFormats from 'ajv-formats';
import get from 'lodash/get';
import has from 'lodash/has';
import isObject from 'lodash/isObject';
import isString from 'lodash/isString';
import reduce from 'lodash/reduce';
import times from 'lodash/times';

const PROPERTIES_KEY = 'properties'
const ONE_OF_KEY = 'oneOf'
const ANY_OF_KEY = 'anyOf'


/** A junk option used to determine when the getFirstMatchingOption call really matches an option rather than returning
 * the first item
 */
const JUNK_OPTION = {
  type: 'object',
  properties: {
    __not_really_there__: {
      type: 'number',
    },
  },
};

/** Given the `formData` and list of `options`, attempts to find the index of the option that best matches the data.
 * 
 * Adapted from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/src/schema/getMatchingOption.ts
 *
 * @param formData - The current formData, if any, used to figure out a match
 * @param options - The list of options to find a matching options from
 * @returns - The index of the matched option or 0 if none is available
 */
export function getMatchingOption(
  formData: any,
  options: JsonSchema[]
): number {
  // For performance, skip validating subschemas if formData is undefined. We just
  // want to get the first option in that case.
  if (formData === undefined) {
    return 0;
  }

  // Validation variables:
  // Define ajv, used to validate
  const ajv = new Ajv({
    allErrors: true,
    verbose: true,
    strict: false,
  });
  // Add formats, like date-time and so on
  addFormats(ajv);

  for (let i = 0; i < options.length; i++) {
    const option = options[i];
    const validate = ajv.compile(option)
    if (option[PROPERTIES_KEY]) {
      // If the schema describes an object then we need to add slightly more
      // strict matching to the schema, because unless the schema uses the
      // "requires" keyword, an object will match the schema as long as it
      // doesn't have matching keys with a conflicting type. To do this we use an
      // "anyOf" with an array of requires. This augmentation expresses that the
      // schema should match if any of the keys in the schema are present on the
      // object and pass validation.
      //
      // Create an "anyOf" schema that requires at least one of the keys in the
      // "properties" object
      const requiresAnyOf = {
        anyOf: Object.keys(option[PROPERTIES_KEY]).map((key) => ({
          required: [key],
        })),
      };

      let augmentedSchema;

      // If the "anyOf" keyword already exists, wrap the augmentation in an "allOf"
      if (option.anyOf) {
        // Create a shallow clone of the option
        const { ...shallowClone } = option;

        if (!shallowClone.allOf) {
          shallowClone.allOf = [];
        } else {
          // If "allOf" already exists, shallow clone the array
          shallowClone.allOf = shallowClone.allOf.slice();
        }

        shallowClone.allOf.push(requiresAnyOf);

        augmentedSchema = shallowClone;
      } else {
        augmentedSchema = Object.assign({}, option, requiresAnyOf);
      }

      // Remove the "required" field as it's likely that not all fields have
      // been filled in yet, which will mean that the schema is not valid
      delete augmentedSchema.required;

      const validate = ajv.compile(augmentedSchema)
      if (validate(formData)) {
        return i;
      }
    } else if (validate(formData)) {
      return i;
    }
  }
  return 0;
}


/** Determines which of the given `options` provided most closely matches the `formData`. Using
 * `getFirstMatchingOption()` to match two schemas that differ only by the readOnly, default or const value of a field
 * based on the `formData` and returns 0 when there is no match. Rather than passing in all the `options` at once to
 * this utility, instead an array of valid option indexes is created by iterating over the list of options, call
 * `getFirstMatchingOptions` with a list of one junk option and one good option, seeing if the good option is considered
 * matched.
 *
 * Once the list of valid indexes is created, if there is only one valid index, just return it. Otherwise, if there are
 * no valid indexes, then fill the valid indexes array with the indexes of all the options. Next, the index of the
 * option with the highest score is determined by iterating over the list of valid options, calling
 * `calculateIndexScore()` on each, comparing it against the current best score, and returning the index of the one that
 * eventually has the best score.
 *
 * Adapted from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/src/schema/getClosestMatchingOption.ts#L136
 * 
 * @param rootSchema - The root JSON schema of the entire form (N.B. rootSchema can also be a piece of the actual root 
 * JSON schema, provided that has a JSON schema structure. Like for instance `object` type, or `oneOf`, and so on)
 * @param formData - The form data associated with the schema
 * @param options - The list of options that can be selected from
 * @param [selectedOption=undefined] - The index of the currently selected option, defaulted to undefined if not specified
 * @returns - The index of the option that is the closest match to the `formData` or the `selectedOption` if no match
 */
export default function getClosestMatchingOption(
  rootSchema: JsonSchema,
  formData: any,
  options: JsonSchema[],
  selectedOption: number | undefined = undefined
): number | undefined {

  // Reduce the array of options down to a list of the indexes that are considered matching options
  const allValidIndexes = options.reduce((validList: number[], option, index: number) => {
    const testOptions = [JUNK_OPTION , option];
    const match = getMatchingOption(formData, testOptions);
    // The match is the real option, so add its index to list of valid indexes
    if (match === 1) {
      validList.push(index);
    }
    return validList;
  }, []);

  // There is only one valid index, so return it!
  if (allValidIndexes.length === 1) {
    return allValidIndexes[0];
  }
  if (!allValidIndexes.length) {
    // No indexes were valid, so we'll score all the options, add all the indexes
    times(options.length, (i) => allValidIndexes.push(i));
  }
  type BestType = { bestIndex: number | undefined; bestScore: number };
  const scoreCount = new Set<number>();
  // Score all the options in the list of valid indexes and return the index with the best score
  const { bestIndex }: BestType = allValidIndexes.reduce(
    (scoreData: BestType, index: number) => {
      const { bestScore } = scoreData;
      const option = options[index];
      const score = calculateIndexScore(rootSchema, option, formData);
      scoreCount.add(score);
      if (score > bestScore) {
        return { bestIndex: index, bestScore: score };
      }
      return scoreData;
    },
    { bestIndex: selectedOption, bestScore: 0 }
  );
  // if all scores are the same go with selectedOption
  if (scoreCount.size === 1 && selectedOption && selectedOption >= 0) {
    return selectedOption;
  }

  return bestIndex;
}


/** Recursive function that calculates the score of a `formData` against the given `schema`. The computation is fairly
 * simple. Initially the total score is 0. When `schema.properties` object exists, then all the `key/value` pairs within
 * the object are processed as follows after obtaining the formValue from `formData` using the `key`:
 * - If the `value` contains a `oneOf` and there is a formValue, then score based on the index returned from calling
 *   `getClosestMatchingOption()` of that oneOf.
 * - If the type of the `value` is 'object', `calculateIndexScore()` is called recursively with the formValue and the
 *   `value` itself as the sub-schema, and the score is added to the total.
 * - If the type of the `value` matches the guessed-type of the `formValue`, the score is incremented by 1, UNLESS the
 *   value has a `default` or `const`. In those case, if the `default` or `const` and the `formValue` match, the score
 *   is incremented by another 1 otherwise it is decremented by 1.
 * 
 * Adapted from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/src/schema/getClosestMatchingOption.ts#L50
 *
 * @param rootSchema - The root JSON schema of the entire form (N.B. rootSchema can also be a piece of the actual root 
 * JSON schema, provided that has a JSON schema structure. Like for instance `object` type, or `oneOf`, and so on)
 * @param schema - The schema for which the score is being calculated
 * @param formData - The form data associated with the schema, used to calculate the score
 * @returns - The score a schema against the formData
 */
export function calculateIndexScore(
  rootSchema: JsonSchema,
  schema?: JsonSchema,
  formData: any = {}
): number {
  let totalScore = 0;
  if (schema) {
    if (isObject(schema.properties)) {
      totalScore += reduce(
        schema.properties,
        (score, value, key) => {
          const formValue = get(formData, key);
          if (typeof value === 'boolean') {
            return score;
          }
          if ((has(value, ONE_OF_KEY) || has(value, ANY_OF_KEY)) && formValue) {
            const key = has(value, ONE_OF_KEY) ? ONE_OF_KEY : ANY_OF_KEY;
            const scoreFromMatchingOption = getClosestMatchingOption(rootSchema,formValue,get(value, key) as JsonSchema[]) || -1
            return score + scoreFromMatchingOption
          }
          if (value.type === 'object') {
            return score + calculateIndexScore(rootSchema, value, formValue || {});
          }
          if (value.type === guessType(formValue)) {
            // If the types match, then we bump the score by one
            let newScore = score + 1;
            if (value.default) {
              // If the schema contains a readonly default value score the value that matches the default higher and
              // any non-matching value lower
              newScore += formValue === value.default ? 1 : -1;
            } else if (value.const) {
              // If the schema contains a const value score the value that matches the default higher and
              // any non-matching value lower
              newScore += formValue === value.const ? 1 : -1;
            }
            // TODO eventually, deal with enums/arrays
            return newScore;
          }
          return score;
        },
        0
      );
    } else if (isString(schema.type) && schema.type === guessType(formData)) {
      totalScore += 1;
    }
  }
  return totalScore;
}

/** Given a specific `value` attempts to guess the type of a schema element. In the case where we have to implicitly
 *  create a schema, it is useful to know what type to use based on the data we are defining.
 *
 * Taken from https://github.com/rjsf-team/react-jsonschema-form/blob/b8f700a1cd2b9b8d7f67040e334836d236b325a4/packages/utils/src/guessType.ts
 * 
 * @param value - The value from which to guess the type
 * @returns - The best guess for the object type
 */
export function guessType(value: any) {
  if (Array.isArray(value)) {
    return 'array';
  }
  if (typeof value === 'string') {
    return 'string';
  }
  if (value == null) {
    return 'null';
  }
  if (typeof value === 'boolean') {
    return 'boolean';
  }
  if (!isNaN(value)) {
    return 'number';
  }
  if (typeof value === 'object') {
    return 'object';
  }
  // Default to string if we can't figure it out
  return 'string';
}