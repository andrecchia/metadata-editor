import MaterialOneOfRenderer, { 
    materialOneOfControlTester 
} from './MaterialOneOfRenderer';
import MaterialAnyOfRenderer, { 
    materialAnyOfControlTester 
} from './MaterialAnyOfRenderer';
import MaterialArrayControlRenderer, {
    materialArrayControlTester
} from './MaterialArrayControlRenderer';
import MaterialEnumArrayRendererWithCheck, {
    materialEnumArrayRendererWithCheckTester 
} from './MaterialEnumArrayRendererWithCheck';
import { JsonFormsRendererRegistryEntry } from '@jsonforms/core';

export {
    MaterialOneOfRenderer,
    materialOneOfControlTester,
    MaterialAnyOfRenderer, 
    materialAnyOfControlTester, 
    MaterialArrayControlRenderer,
    materialArrayControlTester,
    MaterialEnumArrayRendererWithCheck,
    materialEnumArrayRendererWithCheckTester
}

export const materialCustomComplexRenderers: JsonFormsRendererRegistryEntry[] = [
    { tester: materialOneOfControlTester, renderer: MaterialOneOfRenderer},
    { tester: materialAnyOfControlTester, renderer: MaterialAnyOfRenderer},
    { tester: materialArrayControlTester, renderer: MaterialArrayControlRenderer},
    { tester: materialEnumArrayRendererWithCheckTester, renderer: MaterialEnumArrayRendererWithCheck}
]
