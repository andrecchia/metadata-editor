/*
  The MIT License

  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import {
    ArrayLayoutProps,
    composePaths,
    computeLabel,
    createDefaultValue,
    findUISchema,
    isObjectArray,
    isObjectArrayWithNesting,
    or,
    RankedTester,
    rankWith
  } from '@jsonforms/core';
  import {
    JsonFormsDispatch,
    withJsonFormsArrayLayoutProps
  } from '@jsonforms/react';
  import { Grid, Hidden, List, Typography } from '@mui/material';
  import map from 'lodash/map';
  import range from 'lodash/range';
  import React, { useCallback, useEffect, useMemo, useState} from 'react';
  import { ArrayLayoutToolbar } from './ArrayToolbar'; 
  import ListWithDetailMasterItem from './ListWithDetails';
  import merge from 'lodash/merge';
import { getSchemaMaskId } from 'renderer/utils/utils';
import { electronStore, useHandleFieldsCheckState } from '../util/lockedUtils';
import get from 'lodash/get';
  
  

  export const MaterialListWithDetailRenderer = ({
    uischemas,
    schema,
    uischema,
    path,
    enabled,
    errors,
    visible,
    label,
    required,
    removeItems,
    addItem,
    data,
    renderers,
    cells,
    config,
    rootSchema,
    translations
  }: ArrayLayoutProps) => {
    const [selectedIndex, setSelectedIndex] = useState<number|undefined>(undefined);
    const handleAddItem = useCallback(
      (p: string, value: any) => () => {
        addItem?.(p,value)();
        selectedIndex !== undefined ? 
        setSelectedIndex(selectedIndex + 1) : setSelectedIndex(0);
      },
      [removeItems, selectedIndex]
    );
    const handleRemoveItem = useCallback(
      (p: string, value: any) => () => {
        removeItems?.(p, [value])();
        if (selectedIndex === value) {
          setSelectedIndex(undefined);
        } else if (selectedIndex! > value) {
          setSelectedIndex(selectedIndex! - 1);
        }
      },
      [removeItems, selectedIndex]
    );
    const handleListItemClick = useCallback(
      (index: number) => () => setSelectedIndex(index),
      [setSelectedIndex]
    );
    const handleCreateDefaultValue = useCallback(
      () => createDefaultValue(schema, rootSchema),
      [createDefaultValue]
    );
    const foundUISchema = useMemo(
      () =>
        findUISchema(
          uischemas!,
          schema,
          uischema.scope,
          path,
          undefined,
          uischema,
          rootSchema
        ),
      [uischemas, schema, uischema.scope, path, uischema, rootSchema]
    );
    const appliedUiSchemaOptions = merge({}, config, uischema.options);
  
    React.useEffect(() => {
      setSelectedIndex(undefined);
    }, [schema]);
    
    // Used to trigger the hiding/show
    const [isVisible, setIsVisible] = useState(visible)
    // Used to store the visibility that could come from nested (sub)groups
    const [visibility, setVisibility] = useState(false)
    const [fieldsCheckDict,_] =  useHandleFieldsCheckState();
    const schemaMaskId = getSchemaMaskId();
    // check and change array path, remove the '.i.' index for i-th entry
    const arrayPathPattern = /\.\d+\./g   //pattern matching .integer.
    const pathNoArray = path.replace(arrayPathPattern, '.');
    const maskValue = get(electronStore.get(schemaMaskId), pathNoArray)
    useEffect(()=>{
      if(maskValue){
        window.electron.maskHandler.getVisibilityState(schemaMaskId+'.'+pathNoArray)
        .then( v =>  setVisibility(v) )
      } else {
        setVisibility(false)
      }
  
      //Register an event listener for electron-store at element mounting
      electronStore.handleElectronStoreChange((event, newVisibilityState)=>{ 
        setVisibility(newVisibilityState) 
      }, schemaMaskId+'.'+pathNoArray)
  
      //Remove an event listener for electron-store at element unmounting
      return () => electronStore.removeElectronStoreChangeHandler(schemaMaskId+'.'+pathNoArray)
    },[])

    useEffect(()=>{
      if(data > 0){
        /* When there is some entry in the array, it must be visible when at least one of these options are true:
         *  - some fields is checked here or in nested (sub)groups [visibility=true]
         *  - the `hide unchecked fields` button is not pushed [hideUncheckedFields=false]
         */
        setIsVisible(visibility || !fieldsCheckDict.hideUncheckedFields)
      } else {
        /* When there are no entries in the array, it must be visible only if
         * the `hide unchecked fields` button is not pushed [hideUncheckedFields=false]
         */
        setIsVisible(!fieldsCheckDict.hideUncheckedFields)
      }
    },[data, fieldsCheckDict.hideUncheckedFields, visibility])

    return (
    
    <div style={{ display: isVisible ? 'contents' : 'none' }}>
      <Hidden xsUp={!visible}>
        <ArrayLayoutToolbar
          label={computeLabel(
            label,
            required!,
            appliedUiSchemaOptions.hideRequiredAsterisk
          )}
          errors={errors}
          path={path}
          addItem={handleAddItem}
          createDefault={handleCreateDefaultValue}
        />
        <Grid container direction='row' spacing={2}>
          <Grid item xs>
            <List>
              {data > 0 ? (
                map(range(data), index => (
                  <ListWithDetailMasterItem
                    index={index}
                    path={path}
                    schema={schema}
                    enabled={enabled}
                    handleSelect={handleListItemClick}
                    removeItem={handleRemoveItem}
                    selected={selectedIndex === index}
                    key={index}
                    translations={translations}
                  />
                ))
              ) : (
                <p>No data</p>
              )}
            </List>
          </Grid>
          <Grid item xs>
            {selectedIndex !== undefined ? (
              <JsonFormsDispatch
                renderers={renderers}
                cells={cells}
                visible={visible}
                schema={schema}
                uischema={foundUISchema}
                path={composePaths(path, `${selectedIndex}`)}
              />
            ) : (
              <Typography variant='h6'>No Selection</Typography>
            )}
          </Grid>
        </Grid>
      </Hidden>
    </div>
    );
  };
  
  export const materialListWithDetailTester: RankedTester = rankWith(
    100+4,
    or(isObjectArray,isObjectArrayWithNesting)
  );
  
  export default withJsonFormsArrayLayoutProps(MaterialListWithDetailRenderer);
  