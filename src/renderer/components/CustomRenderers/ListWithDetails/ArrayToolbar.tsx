/*
  The MIT License

  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import { 
    Grid,
    IconButton,
    Hidden,
    Toolbar,
    Tooltip,
    Typography
  } from '@mui/material';
  import AddIcon from '@mui/icons-material/Add';
  import React from 'react';
  export interface ArrayLayoutToolbarProps {
    label: string;
    errors: string;
    path: string;
    addItem(path: string, data: any): () => void;
    createDefault(): any;
  }
  export const ArrayLayoutToolbar = React.memo(
    ({
      label,
      errors,
      addItem,
      path,
      createDefault
    }: ArrayLayoutToolbarProps) => {
      return (
        <Toolbar disableGutters={true}>
          <Grid container alignItems='center' justifyContent='space-between'>
            <Grid item>
              <Typography variant={'h6'}>{label}</Typography>
            </Grid>
            <Hidden smUp={errors.length === 0}>
              <Grid item>
              </Grid>
            </Hidden>
            <Grid item>
              <Grid container>
                <Grid item>
                  <Tooltip
                    id='tooltip-add'
                    title={`Add to ${label}`}
                    placement='bottom'
                  >
                    <IconButton
                      aria-label={`Add to ${label}`}
                      onClick={addItem(path, createDefault())}
                      size='large'>
                      <AddIcon />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      );
    }
  );
  