import { JsonSchema } from "@jsonforms/core";
import { electronStore } from "../util/lockedUtils";
import set from 'lodash/set';

export function setSchemaMask (
    schema: JsonSchema, 
    parentPath: string = '',
    schemaMaskId: string = ''
    ) {
    let paths= {};
    if (schema.properties) {
      for (const property in schema.properties) {
        const path = parentPath ? `${parentPath}.${property}` : property;
        const propertySchema = schema.properties[property];
        
        if (propertySchema.type === 'object') {
          paths = Object.assign(paths, setSchemaMask(propertySchema, path, schemaMaskId));
        } else if (propertySchema.type === 'array'){
          // Arrays of native type are treated as single native typy fields for the mask definition:
          // the array of native type is hidden in block with all its entries
          if(propertySchema.items && propertySchema.items["type"] !== "object"){
            electronStore.set( schemaMaskId + '.' + path, false, false )
          }
        }
        else {
          electronStore.set( schemaMaskId + '.' + path, false, false )
        }
      }
    }
  }