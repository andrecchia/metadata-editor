/*
  The MIT License
  
  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import React, { useEffect } from 'react';
import {
  JsonSchema,
  LayoutProps,
  RankedTester,
  rankWith,
  uiTypeIs,
  VerticalLayout,
} from '@jsonforms/core';
import { 
  MaterialLayoutRenderer, 
  MaterialLayoutRendererProps 
} from '@jsonforms/material-renderers';
import { withJsonFormsLayoutProps } from '@jsonforms/react';
import { getSchemaMaskId } from '../../../utils/utils';
import { electronStore } from '../util/lockedUtils';
import get from 'lodash/get'
import { setSchemaMask } from './layoutUtils';
import { Card } from '@mui/material';

/**
 * Default tester for a vertical layout.
 * @type {RankedTester}
 */
export const materialVerticalLayoutTester: RankedTester = rankWith(
  1+100,
  uiTypeIs('VerticalLayout')
);

export const MaterialVerticalLayoutRenderer = ({ uischema, schema, path, enabled, visible, renderers, cells }: LayoutProps) => {
  const verticalLayout = uischema as VerticalLayout;
  const childProps: MaterialLayoutRendererProps = {
    elements: verticalLayout.elements,
    schema,
    path,
    enabled,
    direction: 'column',
    visible
  };

  const schemaMaskId = getSchemaMaskId();
  // check and change array path, remove the '.i.' index for i-th entry (and/or the .i at the end of string)
  const arrayPathPatternEnd = /\.\d+$/     //pattern matching .integer.
  const arrayPathPatternMiddle = /\.\d+\./g     //pattern matching .integer[endOfString]
  let pathNoArray = path ? path.replace(arrayPathPatternEnd, '') : undefined
  pathNoArray = pathNoArray ? pathNoArray.replace(arrayPathPatternMiddle, '.') : undefined

  const maskValue = get(electronStore.get(schemaMaskId), pathNoArray as string);
  useEffect(()=>{
    if(maskValue === undefined && path !== undefined){
      // Enters here if the mask has been never defined, then populate it with false values as a default
      setSchemaMask(schema, pathNoArray, schemaMaskId)
      window.electron.maskHandler.insertIntoVisibilityState(
        get(electronStore.get(schemaMaskId), pathNoArray as string), 
        `${schemaMaskId}.${pathNoArray as string}`
      )
    }
  },
  /**
   *  `path` is needed as a dependency here because in case of array of objects only the 1st
   *  entry triggers event listener with empty dependency
   */ 

  [path])
  return (
    <Card style={
      { 
        marginBottom: '10px',
        marginRight: 'auto',
        marginLeft: 'auto', 
        border: 'none', 
        boxShadow: 'none'
      }
    }>
    <MaterialLayoutRenderer {...childProps} renderers={renderers} cells={cells} />
  </Card>
  );
};

export default withJsonFormsLayoutProps(MaterialVerticalLayoutRenderer);
