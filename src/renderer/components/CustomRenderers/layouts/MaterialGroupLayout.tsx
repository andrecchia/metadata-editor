/*
  The MIT License

  Copyright (c) 2017-2019 EclipseSource Munich
  https://github.com/eclipsesource/jsonforms

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/
import isEmpty from 'lodash/isEmpty';
import React, { useEffect, useRef, useState } from 'react';
import { Card, CardContent, CardHeader, Hidden } from '@mui/material';
import {
  GroupLayout,
  JsonSchema,
  LayoutProps,
  RankedTester,
  rankWith,
  uiTypeIs,
  withIncreasedRank,
} from '@jsonforms/core';
import {
  MaterialLabelableLayoutRendererProps,
  MaterialLayoutRenderer,
} from '@jsonforms/material-renderers';
import { withJsonFormsLayoutProps } from '@jsonforms/react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getSchemaMaskId } from '../../../utils/utils';
import { electronStore, useHandleFieldsCheckState } from '../util/lockedUtils';
import get from 'lodash/get'
import { setSchemaMask } from './layoutUtils';

export const groupTester: RankedTester = rankWith(100, uiTypeIs('Group'));  

const GroupComponent = React.memo(({ visible, enabled, uischema, label, ...props }: MaterialLabelableLayoutRendererProps) => {
  const groupLayout = uischema as GroupLayout;

  return (
    /**
     * This condition checks if the object at this path is the root (watching at the number of
     * '.' characters present in the string). Based on this a Card or Accordion is rendered.
     * WARNING: this fails if the root's name has a '.' in it (like: fie.ld). I should find 
     * another way..
     */
    props.path?.split(".").length===1 ?
    <Hidden xsUp={!visible}>
      <Card style={
        { 
          marginBottom: '10px',
          marginRight: 'auto',
          marginLeft: 'auto', 
        }
      }>
        {!isEmpty(label) && (
          <CardHeader title={label} />
        )}
        <CardContent>
          <MaterialLayoutRenderer {...props} visible={visible} enabled={enabled} elements={groupLayout.elements} />
        </CardContent>
      </Card>
    </Hidden> :
    <Hidden xsUp={!visible}>
      <Accordion 
        // TransitionProps={{ unmountOnExit: true }}
        //default collapse only if it is the parent field
        defaultExpanded={props.path?.split(".").length===2 ? false : true}
        //default collapse when children are >=4
        // defaultExpanded={Object.keys(props.schema?.properties as {}).length < 4}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{label}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <MaterialLayoutRenderer {...props} visible={visible} enabled={enabled} elements={groupLayout.elements} />
        </AccordionDetails>
      </Accordion>
    </Hidden>
  );
});

export const MaterializedGroupLayoutRenderer = ({ uischema, schema, path, visible, enabled, renderers, cells, direction, label }: LayoutProps) => {
  const groupLayout = uischema as GroupLayout;
  // Used to trigger the hiding/show
  const [isVisible, setIsVisible] = useState(visible);
  // Used to store the visibility that could come from nested (sub)groups
  const [visibility, setVisibility] = useState(false);
  const [fieldsCheckDict,_] =  useHandleFieldsCheckState();

  // check and change array path, remove the '.i.' index for i-th entry
  const arrayPathPattern = /\.\d+\./g   //pattern matching .integer.
  const pathNoArray = path.replace(arrayPathPattern, '.');

  const schemaMaskId = getSchemaMaskId();
  const maskValue = get(electronStore.get(schemaMaskId), pathNoArray)
  useEffect(()=>{
    if(maskValue === undefined){
      // Enters here if the mask has been never defined, then populate it with false values as a default
      setSchemaMask(schema, pathNoArray, schemaMaskId)

      const lastDot = pathNoArray.lastIndexOf('.')
      const beginOfPath = pathNoArray.substring(0, lastDot)
      const endOfPath = pathNoArray.substring(lastDot+1)
      // Inserts the partial mask for `path.to.this.group` as a nested object (see below) 
      // to trigger the insertion of `path.to.this` into the visibility group in the main process,
      // otherwise only `path.to.this.group` is inserted.
      window.electron.maskHandler.insertIntoVisibilityState(
        // The nested object mask
        { [endOfPath]: get(electronStore.get(schemaMaskId), pathNoArray as string) },
        `${schemaMaskId}.${beginOfPath as string}`
      )
    }

    window.electron.maskHandler.getVisibilityState(schemaMaskId+'.'+pathNoArray)
    .then( v =>  setVisibility(v) )

    //Register an event listener for electron-store at element mounting
    electronStore.handleElectronStoreChange((event, newVisibilityState)=>{ 
      setVisibility(newVisibilityState) 
    }, schemaMaskId+'.'+pathNoArray)

    //Remove an event listener for electron-store at element unmounting
    return () => electronStore.removeElectronStoreChangeHandler(schemaMaskId+'.'+pathNoArray)
  },[])

  useEffect(()=>{
      if(fieldsCheckDict.checkFields){
        /**
         * Group must be visible when at least one of these options are true:
         * - some fields is checked here or in nested (sub)groups [visibility=true]
         * - the `hide unchecked fields` button is not pushed [hideUncheckedFields=false]
         */
        setIsVisible( visibility || !fieldsCheckDict.hideUncheckedFields );
      }
  },[fieldsCheckDict.hideUncheckedFields, visibility])

  return (
    <div style={{ display: isVisible ? 'contents' : 'none' }}>
    <GroupComponent
      elements={groupLayout.elements}
      schema={schema}
      path={path}
      direction={direction}
      visible={visible}
      enabled={enabled}
      uischema={uischema}
      renderers={renderers}
      cells={cells}
      label={label}
    />
    </div>
  );
};

export default withJsonFormsLayoutProps(MaterializedGroupLayoutRenderer);

export const materialGroupTester: RankedTester = withIncreasedRank(
  1+100,
  groupTester
);
