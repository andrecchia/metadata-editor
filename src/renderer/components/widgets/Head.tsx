import MetadataEditorLogo from 'renderer/metadataEditorLogo.svg'
import InfoIcon from '@mui/icons-material/Info';
import { Button, IconButton, styled, Tooltip } from '@mui/material';
import { useAuth } from 'renderer/AuthContext';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState} from 'react';
import { useMetaStoreInstanceState } from '../Pages/SelectMetaStoreInstance';
import StorageIcon from '@mui/icons-material/Storage';
import { Card } from '@mui/material';


const P = styled('div')(({ theme }) => ({
  ...theme.typography.button,
  backgroundColor: theme.palette.background.paper,
  padding: theme.spacing(1),
}));

export const Head = ()=>{
  const auth = useAuth()
  const navigate = useNavigate()
  const [showInstanceText, setShowInstanceText] = useState(false)
  useEffect(() => {
    // This listens for menu: Settings->Set MetaStore instance message to navigate
    // to MetaStore_instance page
    window.electron.MetaStoreInstance.setInstanceListener(navigate)
    return () => window.electron.MetaStoreInstance.removeListener()
  }, []);
  return(
    <>
      <div style={{ position:'absolute',top:0,left:0 }}>  
        <div style={{ display: 'flex', alignSelf: 'flex-start', height: '39px', alignItems: 'center' }}>
          {!showInstanceText ?
          <Card
            variant={'outlined'}
            onMouseEnter={() => setShowInstanceText(true)}
            style={{ border: "none", boxShadow: "none" }}
          >
            <StorageIcon/>
          </Card>:
          <Card
            variant={'outlined'}
            onMouseLeave={() => setShowInstanceText(false)}
            style={{ border: "none", boxShadow: "none" }}
            >
            {/* This is not actually a button, I use button to make it render nice */}
            <Button 
              size="small"
              sx={{ "&.Mui-disabled": {color: "black"}}}
              disabled={true}
              onClick={() => {}}
            >
              MetaStore instance: {useMetaStoreInstanceState.getState().MetaStoreInstance}
            </Button>
          </Card>
          }
        </div>
        <div style={{ display: 'flex', alignSelf: 'flex-start' }}>
          {auth.authenticated && 
          <Button 
          size="small"
          onClick={() => window.electron.auth.signOut()}
          >
            Logout ({auth.userInfo!['preferred_username']})
          </Button>}
        </div>
      </div>
      <img src={MetadataEditorLogo}/>
    </>
)}

export const APIButton = ()=>{
    const handleButtonClick = () => {
        window.open(`http://localhost:${Number(localStorage.getItem('APIport'))}/api-docs`, '_blank');
      };
  return(
    <div
      style={{position:'absolute', right:'20px', top:'20px'}}
    >
        <Tooltip title="Click to read API documentation">
            <IconButton
              onClick={handleButtonClick}
            >
                <InfoIcon/> 
            </IconButton>
        </Tooltip>
    </div>
)}