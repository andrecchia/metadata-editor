import { ChangeEvent, Ref, forwardRef } from 'react';

interface InputFileUploadProps {
    handleOnChange: (event: ChangeEvent<HTMLInputElement>) => void;
}
/**
 * Hidden input element, to trigger the upload of local documents
 */
export const InputFileUpload = forwardRef((props: InputFileUploadProps, ref: Ref<HTMLInputElement>) => (
        <input 
            type="file" 
            data-testid='fileUpload' 
            id='fileUpload' 
            onChange={props.handleOnChange} 
            hidden
            ref={ref}
        />
    )
)