import Button from "@mui/material/Button";
import { Link, NavigateFunction } from "react-router-dom";

interface footbarProps {
    navigate: NavigateFunction;
    children?: any;
}

const Footbar = (props: footbarProps) =>(
    <div style={{ 
        position:'relative',
        width:'500px',
        left:'50%', 
        marginLeft:'-250px',
        paddingTop:'10px',
        bottom:'10px'
        }}>
    <Button
        component={Link} 
        variant="contained"
        to={'..'}
        onClick={(e) => {
            e.preventDefault();
            props.navigate(-1);
        }}
        >
        Back
    </Button>
    <Button 
        component={Link} 
        variant="contained" 
        to="/"
        >
        Home
    </Button>
    {props.children}
    </div>
)

export default Footbar;
