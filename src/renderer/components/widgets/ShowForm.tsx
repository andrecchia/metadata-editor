import {
    materialRenderers,
    materialCells,
  } from '@jsonforms/material-renderers';
import { useEffect, useState } from 'react';
import { JsonForms } from '@jsonforms/react';
import MaterialListWithDetailRenderer,{
  materialListWithDetailTester,
} from '../CustomRenderers/ListWithDetails/ListWithDetailsRenderer';
import { electronStore, useHandleFieldsCheckState } from '../CustomRenderers/util/lockedUtils';
import MaterializedGroupLayoutRenderer, { materialGroupTester } from '../CustomRenderers/layouts/MaterialGroupLayout';
import { getJsonSchemaPaths, getSchemaMaskId } from '../../utils/utils';
import { materialRenderersWithCheck } from '../CustomRenderers/controlsWithCheck';
import { LockFieldsButtonsToolbar } from './LockFieldsButtonsToolbar';
import { apiEndpointMessagesHandler } from '../../utils/apiEndpointHandler';
import  MaterialVerticalLayoutRenderer,{ materialVerticalLayoutTester } from '../CustomRenderers/layouts/MaterialVerticalLayout';
import { materialCustomComplexRenderers } from '../CustomRenderers/complex';
import { setSchemaMask } from '../CustomRenderers/layouts/layoutUtils';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from 'renderer/utils/theme';

const renderers = [
    ...materialRenderers,
    //register custom renderers
    ...materialRenderersWithCheck,
    ...materialCustomComplexRenderers,
    { tester: materialListWithDetailTester, renderer: MaterialListWithDetailRenderer},
    { tester: materialGroupTester, renderer: MaterializedGroupLayoutRenderer},
    { tester: materialVerticalLayoutTester, renderer: MaterialVerticalLayoutRenderer},
  ];
  
  interface ShowFormProps {
    schema: any;
    initialData?: any;
  }
  
  /**
   * Renders the selected JSON schema with \<JsonForms ... />.
   * In addition, it provides the checking status for the input fields,
   * trigger the fields clearing and hiding.
   */
  export const ShowForm = (props: ShowFormProps) =>{
    const {schema, initialData} = props;
    const [data, setData] = useState(initialData);
    const [fieldsCheckDict,_] =  useHandleFieldsCheckState();
  
    /** Get an object with schema's paths */ 
    const schemaPaths = getJsonSchemaPaths(schema)
  
    // Update the schema with data coming from document upload
    useEffect(()=>{setData(initialData)},[initialData]);
    // Update the schema with server data, coming from API requests
    useEffect(() => {
      // Add an ipc listener when the effect occurs
      apiEndpointMessagesHandler(setData,schemaPaths,data, fieldsCheckDict.checkFields)
      // Remove the listener when the effect 'changes' to avoid multiple listeners
      return () => window.electron.API.removeRequestListener()
    },[data, fieldsCheckDict.checkFields]);
    // Creates a 'schema mask' for the fields to be locked/unlocked, initialize everything to false.
    useEffect(() => {
      // When a new schema is uploaded delete the previous compiled document...
      sessionStorage.removeItem("compiledDocument")
      // And set the schema in sessionStorage
      sessionStorage.setItem("schema", JSON.stringify(schema))
      const schemaMaskId = getSchemaMaskId();
      const currentSchemaMask = electronStore.get(schemaMaskId);
      if( currentSchemaMask === undefined ){
        setSchemaMask(schema,'',schemaMaskId)
      }
      // initialize schema mask handler
      window.electron.maskHandler.assignMaskHandler(electronStore.get(schemaMaskId), schemaMaskId);
      // unset mask handler on unmounting
      return () => window.electron.maskHandler.unsetMaskHandler();
    },
    [schema])

    return(
    <>
        <LockFieldsButtonsToolbar data={data} setData={setData}/>
        <ThemeProvider theme={theme}>
          <JsonForms
          schema={schema}
          data={data}
          renderers={renderers}
          cells={materialCells}
          validationMode={'ValidateAndShow'}
          onChange={({ data, errors }) => {
            setData(data); 
            if( data !== undefined && Object.keys(data).length !== 0){
                sessionStorage.setItem("compiledDocument", JSON.stringify(data))
            }
          }}
          />
        </ThemeProvider>
    </>
    );
  }