import CircularProgress, {
  CircularProgressProps,
} from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Button, Dialog, DialogTitle } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import { NavigateFunction } from 'react-router-dom';
import { useEffect, useRef, useState } from 'react';

/**
 * Starts and handles the sign-in procedure, if already logged in allows to navigate to next page
 * @param authenticated authentication state
 * @param setWaitingLogin set true when there is a pending sign-in procedure (false if not) 
 * @param navigate page navigation function
 * @param path page to go afret login
 */
export const signInProcedure = (authenticated: boolean, setWaitingLogin: (value:boolean) => void , navigate: NavigateFunction, path: string) => {
  if(!authenticated){
    // When not authenticated start the signIn procedure ..
    // Set sign-in procedure to pending state
    setWaitingLogin(true)
    // Start the sign in
    window.electron.auth.signIn()
    .then(response =>{
      if(response && response.authenticated){
        navigate(path)
      }else{
        alert('Login failed')
      }
    })
    .catch(e=>console.log("ERROR LOGIN", e))
    .finally(()=>setWaitingLogin(false))
  } else {
    // .. when already authenticated got to the protected page
    navigate(path)
  }
}

const theme = createTheme({
  palette: {
    secondary: grey,
  },
});


function CircularProgressWithLabel(
  props: CircularProgressProps & { value: number },
) {
  const signInTimeout: number = Number(localStorage.getItem('signInTimeout'))
  const signInTimeoutInSeconds = signInTimeout/1000;
return (
    <Box sx={{ position: 'relative', display: 'inline-flex' }}>
       <ThemeProvider theme={theme}>
        <CircularProgress variant="determinate" {...props} value={props.value/signInTimeoutInSeconds*100} style={{margin:'auto'}}/>
      </ThemeProvider>
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Typography
          variant="caption"
          component="div"
          color="text.secondary"
        >{`${Math.round(props.value)} s`}</Typography>
      </Box>
    </Box>
  );
}

/**
 * Renders a dialog with a countdown waiting the sign-in. It allows to abort the sign-in procedure.
 * @param setWaitingLogin set true when there is a pending sign-in procedure (false if not) 
 */
export default function SignInWaitDialog({setWaitingLogin}) {
  const signInTimeout: number = Number(localStorage.getItem('signInTimeout'))
  const waitServerShutdown: number = Number(localStorage.getItem('waitServerShutdown'))
  const signInTimeoutInSeconds = signInTimeout/1000;
  // State containing the remaining time to sign-in
  const [progress, setProgress] = useState(signInTimeoutInSeconds);
  // Show/hide dialog
  const [open, setOpen] = useState(true)
  // Enable/disable 'Cancel' sign-in button
  const [cancelDisabled, setCancelDisabled] = useState(false)
  // Circular progress color
  const [color, setColor] = useState('primary')
  // Dialog text
  const [text, setText] = useState('Waiting for Sign-In ...')
  // object created by setInterval
  const timerRef = useRef<NodeJS.Timer>();

  /** Performs all the needed tasks when the signIn procedure is canceled.
   *  When the 'sign-in abort' message is delivered it is needed to wait 
   *  the server to being shut down properly (waitServerShutdown)
   */
  const onCancelingSignIn = () => {
    // Stops the countdown deleting the setInterval schedule
    clearInterval(timerRef.current);
    // Change text to signal that sign-in is being canceled (waiting waitServerShutdown time to expire)
    setText('Canceling Sign-In ...')
    // Change circular progress color to gray, so that it is clear that the sign-in
    // canceling signal is received and being processed (waiting waitServerShutdown time to expire)
    setColor('secondary')
    // Triggers the actual sign-in abort in the main process
    window.electron.auth.abortSignIn() 
    // Disable the 'Cancel' button, to avoid accidental multiple sign-in abortion
    setCancelDisabled(true)
    // Triggers the dialog closing after waitServerShutdown time is expired
    setTimeout(()=>{
      // Closes the dialog window
      setOpen(false)
      // Set the pending sign-in procedure to false (being aborted at this point)
      setWaitingLogin(false)
    }, waitServerShutdown)
  }

  useEffect(() => {
    timerRef.current = setInterval(() => {
      setProgress((prevProgress) => (prevProgress <= 0 ? 0 : prevProgress - 1));
    }, 1000);
    return () => {
      clearInterval(timerRef.current);
    };
  }, []);


  return (
  <Dialog open={open}>
    <DialogTitle>{text}</DialogTitle>
    <CircularProgressWithLabel value={progress} color={color as any}/>
    <Button 
      disabled={cancelDisabled}
      onClick={onCancelingSignIn}
    >
      Cancel
    </Button>
  </Dialog>
  );
}