import { electronStore, useHandleFieldsCheckState } from '../CustomRenderers/util/lockedUtils'
import IconButton from '@mui/material/IconButton';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import Tooltip from '@mui/material/Tooltip';
import get from 'lodash/get';
import omit from 'lodash/omit';
import { getSchemaMaskId, getJsonDocumentPaths } from '../../utils/utils';

/**
 * Show/hide icons to lock, clear, hide the unchecked fields
 * @param setData is the function returned by useState(), used to clear the unchecked fields
 * (that is set them to an empty value)
 * @returns
 */
export const LockFieldsButtonsToolbar = ({data, setData}) => {
  /* These are used to set and change the checking status, clearing and hiding */
    const [fieldsCheckState, setFieldsCheckState] = useHandleFieldsCheckState();
    const schemaMaskId = getSchemaMaskId();
    const resolvedMask = getJsonDocumentPaths(electronStore.get(schemaMaskId))
    /**
     * Clears checked fields
     */
    const clearCheckedFields = () => {
        for (const [key,val] of Object.entries(resolvedMask)){
            if(val && get(data,key)){
                setData(data => omit(data,[key]))
            }
        }
    }
    return(
        <>
        {/* This 1st icon enable/disable the toolbar */}
          <Tooltip 
            title={ fieldsCheckState.checkFields ? 
            "Disable fields locking" : "Enable fields locking"
            }>
            <IconButton onClick={
                  ()=>setFieldsCheckState(({...oldState}) => ({
                    checkFields: !oldState.checkFields, 
                    hideUncheckedFields: oldState.hideUncheckedFields
                  }))}>
                {fieldsCheckState.checkFields ? <LockOpenIcon/> : <LockIcon/> }
            </IconButton>
          </Tooltip>
          {fieldsCheckState.checkFields ? 
          <>
          <Tooltip title="Clear checked fields">
            <IconButton id='clearAllFields' onClick={clearCheckedFields}>
              <ClearAllIcon/>
            </IconButton>
          </Tooltip>
          <Tooltip title={ fieldsCheckState.hideUncheckedFields ? "Show hidden fields" : "Hide unchecked fields"}>
            <IconButton id='hideFields' onClick={
                  ()=>setFieldsCheckState(({...oldState}) => ({
                    checkFields: oldState.checkFields, 
                    hideUncheckedFields: !oldState.hideUncheckedFields
                  }))}>
              { fieldsCheckState.hideUncheckedFields ? <VisibilityIcon/> : <VisibilityOffIcon/>}
            </IconButton>
          </Tooltip>
          </> : <></>}
        </>
    );
}