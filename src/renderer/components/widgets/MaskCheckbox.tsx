import Checkbox from "@mui/material/Checkbox";

interface MaskCheckboxProps {
    checked: boolean | undefined;
    onChange: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void | undefined;
}
/**
 * Wrapper for MUI material checkbox, to style mask's checkbox different from the checkboxes
 * for booleans in the form
 * @returns The Checkbox styled element
 */
export const MaskCheckbox: React.FC<MaskCheckboxProps> = ({ checked, onChange }) => (
    <Checkbox 
        checked={checked} 
        style={{margin: 'auto'}} 
        onChange={onChange}
        size="small"
        color="default"
    />
)