import React from 'react';
import { ShowForm } from '../widgets/ShowForm';
import { HandleFieldsCheckProvider } from '../CustomRenderers/util/lockedUtils'
import { LoadDocumentsButtons } from '../widgets/LoadDocumentsButtons';

interface FormProps {
    schema: Object;
    inputElement: HTMLInputElement | null;
    data: Object | undefined;
  }

/**
 * Returns a memoized element with load buttons (rendered conditionally) and
 * the json form
 */
const Form = React.memo(({schema, inputElement, data}: FormProps) => (
    <HandleFieldsCheckProvider>
        <LoadDocumentsButtons 
            isDisabled={schema===undefined ? true : false}
            fileUpload={inputElement}
        />
        <ShowForm schema={schema} initialData={data}/> 
    </HandleFieldsCheckProvider>
))

export default Form