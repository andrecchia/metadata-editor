import Button from '@mui/material/Button';
import { useHandleFieldsCheckState } from '../CustomRenderers/util/lockedUtils';

interface LoadDocumentsButtonsProp {
    isDisabled: boolean;
    fileUpload: HTMLInputElement | null;
}

/**
 * Renders buttons to load and merge local JSON documents. Merge is only loaded conditionally when 
 * the user is in `lock mode` (i.e. when clear and hide fields buttons are present)
 */
export const LoadDocumentsButtons = ( {isDisabled, fileUpload}: LoadDocumentsButtonsProp ) => {
    const [fieldsCheckDict,_] =  useHandleFieldsCheckState();
    return(
        <div>
            <Button 
                variant="contained"
                disabled={isDisabled}
                onClick={()=>{
                    if(fileUpload){
                        fileUpload.setAttribute('name','document');
                        fileUpload.setAttribute('accept','.json');
                        fileUpload.value=''; //Needed if one wants to upload same file 2 times in a row
                        fileUpload.click();
                    }
                }}
            >
                Load JSON document
            </Button>
            {   
                fieldsCheckDict.checkFields ?
                <Button
                    variant="contained"
                    disabled={isDisabled}
                    onClick={()=>{
                        if(fileUpload){
                            fileUpload.setAttribute('name','merge');
                            fileUpload.setAttribute('accept','.json');
                            fileUpload.value='';
                            fileUpload.click();
                        }
                    }}
                >
                    Merge JSON document
                </Button> : undefined
            }
        </div>
    )
}