import React, { useRef, useState } from 'react';
import { Card, CardHeader, CardContent, IconButton, Collapse, Typography, Grid } from '@mui/material';
import '../../css/card.css'

const MenuCard = ({ title, description, children }) => {
  const [expanded, setExpanded] = useState(false);
  const handleExpandToggle = (value) => {
    setExpanded(value);
  };

  return (
    <Card
      onMouseEnter={() => handleExpandToggle(true)}
      onMouseLeave={() => handleExpandToggle(false)}
      className={expanded ? 'card-expanded' : 'card-collapsed'}
      variant="outlined"
      style={{margin: 'auto', paddingTop:'5px', paddingBottom:'10px', minWidth:'400px', maxWidth:'800px'}}
    >
      <CardHeader
        title={title}
      />
      <Collapse in={expanded} timeout="auto">
        <Grid 
          container 
          spacing={0.5} 
          direction="row"
        >
          {/* Fake grid element to make the description to be correctly centered */}
          <Grid item xs={2}>
          </Grid>
          <Grid item xs={8}>
          <CardContent>
            <Typography variant="body2">{description}</Typography>
          </CardContent>
          </Grid>          
          <Grid item xs={2} sx={{ transform:' scale(1.8)', transition: 'transform 0.01s' }}>
            {children}
          </Grid>
        </Grid>
      </Collapse>
    </Card>
  );
};

export default MenuCard;