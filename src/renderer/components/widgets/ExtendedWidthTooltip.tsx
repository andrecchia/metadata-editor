import { Tooltip, TooltipProps, styled, tooltipClasses } from "@mui/material";

/**
 * Custom Tooltip with no max width
 */
export const ExtendedWidthTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))({
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 'none',
    },
  });