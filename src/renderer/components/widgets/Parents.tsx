import { useEffect, useState } from "react";
import { Dropdown } from "./selectSchemaProperties";
import { scanSchemasFromMetaStore } from "../../utils/utils";
import set from 'lodash/set';
import  { validateAndGetErrors } from '../Pages/UploadToMetaStore'
import { Grid } from "@mui/material";
import { getAllMetaStorePages } from '../../utils/utils';
import { useAuth } from "renderer/AuthContext";
import { MetaStoreInstanceDomainMap } from "renderer/constants";
import { useMetaStoreInstanceState } from "../Pages/SelectMetaStoreInstance";

interface propsParentDropdown {
  parentType: any;
  property: any; 
  path: any;
  validate: any;
  setValidationErrors: any;
}
/**
 * Wraps the Dropdown component and fills the `parents` fields with the selected value from dropdown menu
 */
const ParentDropdown = ( props: propsParentDropdown ) => {
    const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
    const {parentType, property, path, validate, setValidationErrors } = props;
    const [currentValue, setCurrentValue] = useState<string|undefined>(undefined)
    const handleChange = (e) => {
        const formattedValue: string = e.target.value;
        /**
         * Regex that matches the pattern "id:", followed by optional whitespace \s*, and captures any characters (.*?) until the first occurrence of a comma.
         */
        const regex = /id:\s*(.*?),/i;
        /**
         * 0th element contains the full match, 1st element contains the subgroup (.*?), that is the actual id
         */
        const match = formattedValue.match(regex);
        // Get and parse the current document from sessionStorage
        let compiledDocument = JSON.parse(sessionStorage.getItem('compiledDocument') as string)
        // Set the field with the chosen value and write back to the sessionStorage compiled document
        set(compiledDocument, path + '.parentReference',`${MetaStoreInstanceDomainMap[MetaStoreInstance as string]}/api/v1/metadata/${match![1]}`)
        sessionStorage.setItem('compiledDocument',JSON.stringify(compiledDocument))
        // Perform the validation, if needed
        if(validate && setValidationErrors){
          const errorMessages = validateAndGetErrors(validate)
          setValidationErrors(errorMessages)
        }
        // Make the selected value to appear as the choice in the dropdown
        setCurrentValue(formattedValue);
      };
    return(
        <Dropdown
        id={parentType+path}
        value={currentValue}
        fieldName={
            /** from parents[n] to parents[n+1 
             * (because in the rendered forms arrays start from 1)
             */
            path.replace(/\[(\d+)\]/, (match, number) => {
                var incrementedNumber = parseInt(number) + 1;
                return '[' + incrementedNumber + ']'
            }) 
            + ' (' +parentType+')'
        }
        property={property}
        handleChange={handleChange}
        disabled={false}
        variant="standard"
        sx={{ maxWidth: '468px'}}
      /> 
    )
}

/**
 * Gets all the metadata documents from MetaStore and sort them dy date-time
 * @param queryParamenter (optional), some query parameter to filter metadata to get (schemaId for instance)
 * @returns array of strings containing documents id and last update date in this format: `id: ${itemId}, last update: ${formattedLastUpdate}`
 */
export const metadataDocumentIdSortedByDate = async (queryParamenter?: string, headers?: HeadersInit | undefined) => {
  const endpoint = queryParamenter ? 'metadata?' + queryParamenter : 'metadata';
  const data = await getAllMetaStorePages(endpoint, headers)
  // Sort the response array by date-time, most recent first
  let sortedArray = data.sort((a, b) => {
    let dateA = new Date(a.lastUpdate);
    let dateB = new Date(b.lastUpdate);
    return dateB.getTime() - dateA.getTime();
  });
  // Parse the response to create an array of string containing id and date-time
  // with format id: [id] - last update: [last update with format aaaa-mm-dd hh:mm.ss]
  const idsArray = sortedArray.map(item => {
    const itemId = item.id;
    const lastUpdate = item.lastUpdate.split("T");
    const lastUpdateDate = lastUpdate[0];
    const lastUpdateTime = lastUpdate[1].slice(0, -1);
    const formattedLastUpdate = `${lastUpdateDate} ${lastUpdateTime}`;
    const formattedString = `id: ${itemId}, last update: ${formattedLastUpdate}`
    return formattedString;
  });
  return idsArray
}

/**
 * For each parent array fetch the suitable (i.e. the ones with the right label) avaiable documents from MetaStore and define
 * the array of dropdown menus to be rendered
 * @param parentsArray Array of objects containing the parent (partial?) path in the document and parent type (precursor, sample, ...)
 * @param setParentsDropdown 
 */
const fetchMetaStoreParentsData = async (parentsArray, setParentsDropdown, validate, setValidationErrors, token) => {
    const schemaMetaStore = await scanSchemasFromMetaStore();
    const dropdowns: JSX.Element[] = [];
    let i: number = 0
    for (const parent of parentsArray) {
      // get query parameter containing all schemaIds for a given label, for instance `schemaId=mri_schema&schemaId=sem_schema`
      const schemaIdQueryParamenter = schemaMetaStore.schemaIdLabel[parent.parentType].map((item) => `schemaId=${item}`).join('&');
      const headers = {'Authorization': 'Bearer ' + token }
      const idsArray = await metadataDocumentIdSortedByDate(schemaIdQueryParamenter, headers)
      const parentDropdown = idsArray.length > 0 ? 
      (
        <ParentDropdown
          key={i}
          parentType={parent.parentType}
          property={idsArray}
          path={parent.path}
          validate={validate}
          setValidationErrors={setValidationErrors}
        />
      ) : 
      <span key={i}> 
        No metadada documents registered on MetaStore for type "{parent.parentType}" <br/><br/>
      </span>
      dropdowns.push(parentDropdown);
      i++;
    }

    setParentsDropdown(dropdowns);
  };


interface propsParents {
  parentsArray: any;
  validate: any;
  setValidationErrors: any;
}
/**
 * Renders dropdown menus to choose the parents among the avaiable documents in MetaStore
 * @param parentsArray Array of objects containing the parent (partial?) path in the document and parent type (precursor, sample, ...)
 */
export const Parents = (props: propsParents) => {
    const auth = useAuth()
    const {parentsArray, validate, setValidationErrors} = props;
    // Stores the ParentDropdown JSX
    const [parentsDropdown, setParentsDropdown] = useState<JSX.Element[]>([]);
    useEffect(() => {
      fetchMetaStoreParentsData(parentsArray, setParentsDropdown, validate, setValidationErrors, auth.token).catch((error) => {
        console.error('Error:', error);
      });
    }, []);

    return (
      <Grid container spacing={2}>
        {parentsDropdown.map((element, index) => (
          <Grid item xs={12} key={index}>
            {element}
          </Grid>
        ))}
      </Grid>
    );
}