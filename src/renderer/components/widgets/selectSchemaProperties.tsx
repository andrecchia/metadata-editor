import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import { Grid, SxProps, Theme } from '@mui/material';

type DropdownProps = {
  id: string;
  value: any;
  fieldName: string;
  property: any;
  handleChange: any;
  disabled: boolean;
  variant?: string;
  sx?: SxProps<Theme> | undefined;
};

export const Dropdown = ({
    id,
    value,
    fieldName,
    property,
    handleChange,
    disabled,
    variant = 'outlined',
    sx = { width: '300px' },
  }: DropdownProps) => {  
  const menuItems = property ? property.map(item => (
    <MenuItem key={item} value={item}>{item}</MenuItem>
    )
  ) : '';
  return (
    <Box sx={sx} margin={'auto'}>
      <FormControl fullWidth>
        <InputLabel id={id+'-label'}>{fieldName}</InputLabel>
        <Select
          labelId={id+'-label'}
          id={id}
          value={!property ? '' : property.includes(value) ? value : ''}
          label={fieldName}
          onChange={handleChange}
          disabled={disabled}
          variant={variant as 'outlined' | 'filled' | 'standard' | undefined}
        >
          {menuItems}
        </Select>
      </FormControl>
    </Box>
  );
}

export const SpinButton = ({id, fieldName, disabled, value, handleChange, min, max}) => {
  return (
    <TextField
      id={id}
      style={{width:'83px'}}
      label={fieldName}
      type="number"
      disabled={disabled}
      value={value}
      onChange={handleChange}
      InputProps={{ inputProps: { min: min, max: max } }}
      InputLabelProps={{
        shrink: true,
      }}
    />
  );
}

interface propsSelectSchemaProperties{
  schemaLabel: string[];
  schemaIdLabel: {};
  schemaLastVersion: {};
  value: {label: string, id: string, version: number} | undefined;
  setValue: React.Dispatch<
    React.SetStateAction<{
      label: string;
      id: string;
      version: number;
    } | undefined>
  >
}

export const SelectSchemaProperties = (props: propsSelectSchemaProperties) => {
  const { schemaLabel, schemaIdLabel, schemaLastVersion, value, setValue } = props;

  const handleLabelChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const newLabelValue = e.target.value as string;
    // When the schema label changes reset the id and version
    setValue!({ label: newLabelValue, id: '', version: -1 });
  };

  const handleIdChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const newIdValue = e.target.value as string;
    const newVersionValue = schemaLastVersion[newIdValue];
    // When the schema id changes set the version to the most recent one
    setValue!({ label: value?.label || '', id: newIdValue, version: newVersionValue });
  };

  const handleVersionChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const newVersionValue = e.target.value as number;
    setValue!({ label: value?.label || '', id: value?.id || '', version: newVersionValue });
  };
  return (
    <Grid container spacing={0} justifyContent={'center'} style={{ margin: 'auto' }}>
      <Grid item>
        <Dropdown
          id={'selectSchemaLabel'}
          value={value?.label || ''}
          fieldName={'Label'}
          property={schemaLabel}
          handleChange={handleLabelChange}
          disabled={false}
        />
      </Grid>
      <Grid item>
        <Dropdown
          id={'selectSchemaId'}
          value={value?.id || ''}
          fieldName={'Schema ID'}
          property={schemaIdLabel[value?.label || '']}
          handleChange={handleIdChange}
          disabled={!value?.label}
        />
      </Grid>
      <Grid item>
        <SpinButton
          id={'selectSchemaVersion'}
          value={!value?.version ? 0 : value?.version < 0 ? 0 : value?.version}
          fieldName={'Version'}
          min={1}
          max={schemaLastVersion[value?.id as string]}
          handleChange={handleVersionChange}
          disabled={!value?.id}
        />
      </Grid>
    </Grid>
  );
};