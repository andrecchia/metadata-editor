import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useAuth } from "renderer/AuthContext";

const PrivateRouteWrapper = ({ children }) => {

 let navigate = useNavigate();
 const auth = useAuth();

useEffect(() => {
  if (!auth.authenticated) {
    alert('Please login and retry');
    navigate('/');
  }
}, [auth.authenticated, navigate]);

 return auth.authenticated ? children : null;
};


export default PrivateRouteWrapper;