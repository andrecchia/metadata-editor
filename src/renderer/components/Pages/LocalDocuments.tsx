import { useState, useEffect, ChangeEvent, useRef } from 'react';
import Button from '@mui/material/Button'
import { Link, useNavigate } from "react-router-dom";
import { getSchemaFromMetaStore, loadFile } from '../../utils/utils';
import { APIButton, Head } from '../widgets/Head';
import Footbar from '../widgets/Footbar';
import { InputFileUpload } from '../widgets/InputFileUpload';
import Form from '../widgets/Form';

function LocalDocuments() {
  const inputRef = useRef<HTMLInputElement>(null);
  const [schema, setSchema] = useState<Object|undefined>();
  const [data, setData] = useState<Object|undefined>(undefined)
  const navigate = useNavigate();
  const [windowSize, setWindowSize] = useState([
    window.innerWidth,
    window.innerHeight,
  ]);

  useEffect(() => {
    const handleWindowResize = () => {
      setWindowSize([window.innerWidth, window.innerHeight]);
    };

    window.addEventListener('resize', handleWindowResize);

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  useEffect(() =>{
    if(sessionStorage.getItem("schemaMode")==="localDocument"){
      const currentSchemaId=sessionStorage.getItem("schemaId")
      const currentSchemaVersion=sessionStorage.getItem("schemaVersion")
      const currentCompiledDocument=sessionStorage.getItem("compiledDocument")
      const currentSchema=sessionStorage.getItem("schema")
      // In case previously schema and/or document were present reload them
      if(currentSchemaId !== null && 
         currentSchemaVersion !== null &&
         currentSchema !== null ){
            const parsedSchema = JSON.parse(currentSchema);
            let parsedDocument: Object|null|undefined;
            if( currentCompiledDocument !== null ){
              parsedDocument = JSON.parse(currentCompiledDocument as string);
              setData(parsedDocument as Object)
            }
            setSchema(parsedSchema);
          }
    } else {
          const fileUpload = inputRef.current
          fileUpload?.setAttribute('name','jmeDocument');
          fileUpload?.setAttribute('accept','.jme');
          fileUpload?.click();
        }
    },[]);

  /**
   * Set metadata schema or document from `obj`, it is triggered by an onChange event
   * during a file upload
   * @param e onChange event
   * @param obj a schema or document object
   */
  const handleLoadFile = async (e: ChangeEvent<HTMLInputElement>, obj) => {
    const targetName = e.target.name;
    if(targetName === "jmeDocument"){
      const {_metaStoreSchema,...jsonDocument} = obj;
      if(_metaStoreSchema === undefined){
        alert('Invalid .jme file (missing informations on schema Label, ID and Version)')
      } else {
        getSchemaFromMetaStore(_metaStoreSchema.schemaId, _metaStoreSchema.schemaVersion)
        .then(s => {
          setSchema(s);
          sessionStorage.setItem("schemaLabel", _metaStoreSchema.schemaLabel)
          sessionStorage.setItem("schemaId", _metaStoreSchema.schemaId)
          sessionStorage.setItem("schemaVersion", _metaStoreSchema.schemaVersion)
          // Indicates if we are getting a schema from MetaStore or local document
          sessionStorage.setItem("schemaMode", "localDocument")
          setData(jsonDocument)
        })
        .catch(e=>{
          console.log(e)
          alert(e)
        })
      }
    } else if(targetName === "document"){
      setData(obj)
    } else {
      fetch(`http://localhost:${Number(localStorage.getItem('APIport'))}/api/v1/compile`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      })
      .then(response => response.text())
      .then(data => console.log("RESPONSE",data))
      .catch(error => console.error(error));
    }
  }

  return (
    <div className='App'>
      <InputFileUpload handleOnChange={(e)=>{ loadFile(e,handleLoadFile) }} ref={inputRef}/>
      <Head/>
      <APIButton/>
      <div id="select-container">
        <Button 
          variant="contained"
          onClick={(e)=>{
            const fileUpload = inputRef.current
            if(fileUpload){
              fileUpload.setAttribute('name','jmeDocument');
              fileUpload.setAttribute('accept','.jme');
              fileUpload.value=''; //Needed if one wants to upload same file 2 times in a row
              fileUpload.click();
            }
        }}
        >
          Load .jme file
        </Button>
      </div>
      {
        schema ? 
        <Form schema={schema} inputElement={inputRef.current} data={data}/>
        : <div id="editor-container"  style={{minHeight: (windowSize[1]*0.6).toString()+'px'}}/>
      }
      <Footbar navigate={navigate}>
        <Button
          component={Link} 
          variant="contained" 
          to="/document_manager"
        >
          Done
        </Button>
      </Footbar>
    </div>
  );
}

export default LocalDocuments;