import { useEffect, useRef } from 'react';
import { useState } from 'react';
import { SelectSchemaProperties } from '../widgets/selectSchemaProperties';
import Button from '@mui/material/Button';
import { loadFile, scanSchemasFromMetaStore } from '../../utils/utils';
import { getSchemaFromMetaStore } from '../../utils/utils';
import { InputFileUpload } from '../widgets/InputFileUpload';
import { Link, useNavigate } from 'react-router-dom';
import { APIButton, Head } from '../widgets/Head';
import Footbar from '../widgets/Footbar';
import Form from '../widgets/Form';

export const MetaStoreSchemas = () =>{
  const inputRef = useRef<HTMLInputElement>(null);
  // Checks whether the loading is finished
  const [loading, setLoading] = useState(true);
  // Variables that stores result from API call
  /** Array of string with schema labels */ 
  const [schemaLabel, setSchemaLabel] = useState([] as string[]);
  /** 
   * Object with labels as keys and correspondent schema ids (arrays of strings) as values. 
   * @example {"input":["input_schema"], "measurement": ["SEM_schema", "STM_schema"], ... } 
   */
  const [schemaIdLabel, setSchemaIdLabel] = useState({});
  /**
   * Object with schemas id as keys and last version avaiable as values. 
   * @example: {'foo_schema': 3, 'bar_schema': 1, ...}
   */
  const [schemaLastVersion, setSchemaLastVersion] = useState({});

  const [value, setValue] = useState< {label: string, id: string, version: number} | undefined>(undefined);
  const [schema, setSchema] = useState<Object|undefined>(undefined);
  const [data, setData] = useState<Object|undefined>(undefined)

  const navigate = useNavigate();
  const [windowSize, setWindowSize] = useState([
    window.innerWidth,
    window.innerHeight,
  ]);

  useEffect(() => {
    const handleWindowResize = () => {
      setWindowSize([window.innerWidth, window.innerHeight]);
    };

    window.addEventListener('resize', handleWindowResize);

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  useEffect(() =>{
    scanSchemasFromMetaStore().then(({schemaLabel, schemaIdLabel, schemaLastVersion})=>{
      setSchemaLabel([...schemaLabel]);
      setSchemaIdLabel({...schemaIdLabel});
      setSchemaLastVersion({...schemaLastVersion});
      setLoading(false);
    })
    .then(()=>{
      if(sessionStorage.getItem("schemaMode")==="MetaStore"){
        const currentSchemaLabel=sessionStorage.getItem("schemaLabel")
        const currentSchemaId=sessionStorage.getItem("schemaId")
        const currentSchemaVersion=sessionStorage.getItem("schemaVersion")
        const currentCompiledDocument=sessionStorage.getItem("compiledDocument")
        const currentSchema=sessionStorage.getItem("schema")
        // In case previously schema and/or document were present reload them
        if(currentSchemaLabel !== null && 
           currentSchemaId !== null && 
           currentSchemaVersion !== null &&
           currentSchema !== null ){
            setValue({label: currentSchemaLabel, id: currentSchemaId, version: parseInt(currentSchemaVersion)})
            const parsedSchema = JSON.parse(currentSchema);
            let parsedDocument: Object|null|undefined;
            if( currentCompiledDocument !== null ){
              parsedDocument = JSON.parse(currentCompiledDocument as string);
              setData(parsedDocument as Object)
            }
            setSchema(parsedSchema);
        }
      }
    })
    .catch((e)=>console.log(e))

  },[]);

  /**
   * Renders MetaStore's schemas empty or compiled
   * @param schemaId 
   * @param schemaVersion 
   * @param initialData 
   */
  function renderMetaStoreSchema(schemaId: string, schemaVersion: number, initialData?: Object){
    if(initialData === undefined){
      //Load schema from MetaStore (in this case initialData is undefined)
      getSchemaFromMetaStore(schemaId, schemaVersion)
      .then(s => {
        setSchema(s);
        sessionStorage.setItem("schemaLabel", value?.label as string)
        sessionStorage.setItem("schemaId", schemaId)
        sessionStorage.setItem("schemaVersion", schemaVersion.toString())
        // Indicates if we are getting a schema from MetaStore or local document
        sessionStorage.setItem("schemaMode", "MetaStore")
      })
      .catch(e=>console.log(e))
    } else {
      setData(initialData)
    }
  }

  /**
   * Compiles a schema with a specific version (taking it from MetaStore) from a 
   * local document
   * @param obj a document object
   */
  const handleLoadFile = (e, obj) =>{
    if(e.target.name === 'merge'){
      fetch(`http://localhost:${Number(localStorage.getItem('APIport'))}/api/v1/compile`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      })
      .then(response => response.text())
      .then(data => console.log("RESPONSE",data))
      .catch(error => console.error(error));
    } else {
      renderMetaStoreSchema(value?.id as string, value?.version as number, obj);
    }
  }

  return(
    <div className='App'>
      <InputFileUpload handleOnChange={(e)=>{ loadFile(e,handleLoadFile) }} ref={inputRef}/>
      <Head/>
      <APIButton/>
      {
        loading ? <p>Loading...</p> :
        <>
          <SelectSchemaProperties
            schemaIdLabel={schemaIdLabel}
            schemaLabel={schemaLabel}
            schemaLastVersion={schemaLastVersion}
            value={value}
            setValue={setValue}
          />
          <Button 
            variant="contained" 
            onClick={() => {
                if( value?.id !== '' && value?.version !== undefined && value?.version > 0 ){
                  // When Load Schema is pushed trigger a rerendering:
                  // set schema as undefined (the new schema will be set in renderMetaStoreSchema), thiss will ensure a re-render
                  setSchema(undefined)    
                  // unset data and delete current data from sessionStorage
                  setData(undefined)    
                  sessionStorage.removeItem("compiledDocument")
                  renderMetaStoreSchema(value?.id, value?.version);     
                } else {
                  alert("Choose a schema ID and version")
                }
              } 
            }
          >
            Load Schema
          </Button>
          {
            schema ? 
            <Form schema={schema} inputElement={inputRef.current} data={data}/>
            : <div id="editor-container"  style={{minHeight: (windowSize[1]*0.6).toString()+'px'}}></div>
          }
        </>
      }
      <Footbar navigate={navigate}>
        <Button
          component={Link} 
          variant="contained" 
          to="/document_manager"
        >
          Done
        </Button>
      </Footbar>
    </div>
  )
}
