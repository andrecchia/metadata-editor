import { Head } from "../widgets/Head";
import { Button, Typography } from "@mui/material";
import { useState } from "react";
import { useAuth } from "renderer/AuthContext";
import { KeycloakConfigs, MetaStoreInstancesAvailable, MetaStoreInstancesURL } from "renderer/constants";
import { Dropdown } from "../widgets/selectSchemaProperties";
import { Link } from "react-router-dom";
import { create } from "zustand";

type MetaStoreInstanceState = {
    // BASE_URL: string | undefined
    MetaStoreInstance: string | undefined
    setMetaStoreInstanceState: (value: any) => void
}

export const useMetaStoreInstanceState = create<MetaStoreInstanceState>(set => ({
    // BASE_URL: undefined,
    MetaStoreInstance: undefined,
    setMetaStoreInstanceState: (NewMetaStoreInstance) => set({ 
        // BASE_URL: MSInstancesURL[NewMetaStoreInstance],
        MetaStoreInstance: NewMetaStoreInstance
    })
  }))

/**
 * Page to select the MetaStore instance
 * @returns 
 */
export const SelectMetaStoreInstance = () => {
    const auth = useAuth()
    const {setMetaStoreInstanceState} = useMetaStoreInstanceState()
    const [value, setValue] = useState<string|undefined>(useMetaStoreInstanceState.getState().MetaStoreInstance)
    const handleMSINstanceChange = (e: React.ChangeEvent<{ value: unknown }>) => {
      const newMetaStoreInstance = e.target.value as string;
      const introMessage = 'Changing MetaStore instance will: '
      const schemaMessage = '\n- delete all changes in the selected schema.'
      const logoutMessage = '\n- log you out from the current session.'
      const endMessage = '\nDo you want to proceed?'
      let message = introMessage
      if(sessionStorage.length>0){
        message += schemaMessage
      }
      if(auth.authenticated){
        message += logoutMessage
      }
      message += endMessage
      if(message !== introMessage+endMessage){
        // Show confirmation dialog when the user is logged and/or he is working with a schema
        if(window.confirm(message)){
          setMetaStoreInstanceState(newMetaStoreInstance)
          setValue(newMetaStoreInstance)
          localStorage.setItem('lastMetaStoreInstance', newMetaStoreInstance)
          if(auth.authenticated){
            window.electron.auth.signOut()
          }
          if(sessionStorage.length>0){
            sessionStorage.clear()
          }
          window.electron.auth.configOIDC(
            KeycloakConfigs[newMetaStoreInstance]
          )
        }
      } else {
        // Otherwise just set the new MS instance, nothing will be lost in this case
        setMetaStoreInstanceState(newMetaStoreInstance)
          setValue(newMetaStoreInstance)
          localStorage.setItem('lastMetaStoreInstance', newMetaStoreInstance)
          window.electron.auth.configOIDC(
            KeycloakConfigs[newMetaStoreInstance]
          )
      }
    };
    return(
    <div className='App'>
        <Head/>
      <div id='initial-buttons-container' className='initial-buttons-container' style={{margin:'auto', paddingTop:'5%'}}>
        <Typography 
          variant="h4" 
          component="h4"
          style={{padding:'20px', fontWeight: 540}}
        >
          Select MetaStore instance
        </Typography>
        <Dropdown
          id='MetaStoreInstanceDropdown'
          value={value}
          fieldName='MetaStore instance'
          property={MetaStoreInstancesAvailable}
          handleChange={handleMSINstanceChange}
          disabled={false}
          variant='filled'
        />
      </div>
        <Button
          component={Link} 
          variant="contained" 
          disabled={useMetaStoreInstanceState.getState().MetaStoreInstance === undefined}
          to="/"
        >
          Home
        </Button>
    </div>
    );
  }