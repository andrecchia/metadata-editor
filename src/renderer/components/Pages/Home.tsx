import { Link, useNavigate } from "react-router-dom";
import { Head } from "../widgets/Head";
import MenuCard from "../widgets/Card";
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import ArticleIcon from '@mui/icons-material/Article';
import { IconButton, Typography } from "@mui/material";
import TimelineIcon from '@mui/icons-material/Timeline';
import { useEffect, useState } from "react";
import { useAuth } from "renderer/AuthContext";
import SignInWaitDialog, { signInProcedure } from "../widgets/SignInUtil";
import { KeycloakConfigs } from "renderer/constants";
import { useMetaStoreInstanceState } from "./SelectMetaStoreInstance";

export const Home =()=>{
  const auth = useAuth();
  const {MetaStoreInstance, setMetaStoreInstanceState} = useMetaStoreInstanceState()
  // Signal that a sign-in procedure is pending (when true)
  const [waitingLogin, setWaitingLogin] = useState(false)
  const navigate = useNavigate()
  useEffect(()=>{
    const lastMSInstance = localStorage.getItem('lastMetaStoreInstance')
    // Go to MetaStore_instance page when no instance is stored in localStorage
    if( lastMSInstance === null) {
      navigate("/MetaStore_instance")
    }
    // If there is instance in localStorage but MetaStoreInstance is undefined
    else if(MetaStoreInstance === undefined) {
      setMetaStoreInstanceState(lastMSInstance)
        window.electron.auth.configOIDC(
          KeycloakConfigs[lastMSInstance]
        )
    }
  },[])
    return(
    <div className='App'>
        <Head/>
      <div id='initial-buttons-container' className='initial-buttons-container' style={{margin:'auto', paddingTop:'5%'}}>
        <Typography 
          variant="h4" 
          component="h4"
          style={{padding:'20px', fontWeight: 540}}
        >
          easily edit metadata documents
        </Typography>
        <MenuCard 
          title={'MetaStore schemas'} 
          description={"Get schemas from MetaStore. Edit, validate and upload to MetaStore or save them as local documents"}
        >
          <IconButton 
            component={Link}
            aria-label="MetaStore_schemas"
            to="/MetaStore_schemas"
          >
            <CloudDownloadIcon />
          </IconButton> 
        </MenuCard>
        <MenuCard 
          title={'Local documents'} 
          description={'Load and edit a previously saved (.jme) document'}
        >
          <IconButton 
           component={Link}
           aria-label="local_documents"
           to="/local_documents"
          >
            <ArticleIcon />
          </IconButton>
        </MenuCard>
        <MenuCard 
          title={'Provenance'} 
          description={'Generate the provenance for metadata documents (login required)'}
        > 
          { waitingLogin ? <SignInWaitDialog setWaitingLogin={setWaitingLogin}/> : null }
          <IconButton 
            aria-label='login'
            onClick={()=>signInProcedure(auth.authenticated,setWaitingLogin,navigate,'/provenance')}
          >
            <TimelineIcon/>
          </IconButton> 
        </MenuCard>
      </div>
    </div>
    );
  }