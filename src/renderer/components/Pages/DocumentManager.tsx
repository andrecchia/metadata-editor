import Button from '@mui/material/Button';
import { Link, useNavigate } from "react-router-dom";
import { saveFile } from '../../utils/utils';
import { Head } from '../widgets/Head';
import MenuCard from "../widgets/Card";
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import SaveIcon from '@mui/icons-material/Save';
import Footbar from '../widgets/Footbar';
import { IconButton, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useAuth } from 'renderer/AuthContext';
import SignInWaitDialog, { signInProcedure } from '../widgets/SignInUtil';

export const DocumentManager = () => {
    const auth = useAuth()
    const navigate = useNavigate();
    // Signal that a sign-in procedure is pending (when true)
    const [waitingLogin, setWaitingLogin] = useState(false)
    const [windowSize, setWindowSize] = useState([
      window.innerWidth,
      window.innerHeight,
    ]);
  
    useEffect(() => {
      const handleWindowResize = () => {
        setWindowSize([window.innerWidth, window.innerHeight]);
      };
  
      window.addEventListener('resize', handleWindowResize);
  
      return () => {
        window.removeEventListener('resize', handleWindowResize);
      };
    }, []);
    return (
    <div className='App'>
      <Head/>
      <div id="select-container" style={{margin:'auto', paddingTop:'5%',minHeight: (windowSize[1]*0.6).toString()+'px'}}>
        <MenuCard 
          title={'Upload to MetaStore'} 
          description={'Upload metadata documents to MetaStore (login required)'}
        >
          { waitingLogin ? <SignInWaitDialog setWaitingLogin={setWaitingLogin}/> : null }
          <IconButton 
            aria-label='login'
            onClick={()=>{
              if(sessionStorage.getItem('schema') === null){
                // When no schema is selected show an alert and block login/navigation
                // to '/upload_to_MetaStore' page
                alert('Please select a schema before to proceed')
              }else{
                signInProcedure(auth.authenticated,setWaitingLogin,navigate, '/upload_to_MetaStore')
              }
            }}
          >
            <CloudUploadIcon/>
          </IconButton>
        </MenuCard>
        <MenuCard 
          title={'Export'} 
          description={'Export metadata documents as .json files'}
        >
          <IconButton
            aria-label='export'
              onClick={() => {
                  if(sessionStorage.getItem('compiledDocument') === null){
                    alert('Cannot export an empty document')
                  } else{
                    saveFile(JSON.parse(sessionStorage.getItem("compiledDocument") as string))
                  }
                }
              }
          >
            <SaveAltIcon />
          </IconButton>
        </MenuCard>
        <MenuCard 
          title={'Save'} 
          description={'Save .jme metadata documents to be edited in a second moment'}
        >
          <IconButton
              onClick={() => {
                if(sessionStorage.getItem('compiledDocument') === null){
                  alert('Cannot save an empty document')
                } else{
                  saveFile(JSON.parse(sessionStorage.getItem("compiledDocument") as string), true)
                }
              }
            }
          >
              <SaveIcon />
          </IconButton>
        </MenuCard>
        </div>
        <Footbar navigate={navigate} />
      </div>
    );
  };