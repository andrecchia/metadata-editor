import Button from '@mui/material/Button';
import { useEffect, useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import { JsonForms } from '@jsonforms/react';
import { materialCells, materialRenderers } from '@jsonforms/material-renderers';
import set from 'lodash/fp/set';
import { Head } from '../widgets/Head';
import initialRecordSchema from '../../utils/recordSchema.json'
import recordUischema from '../../utils/recordUischema.json'
import Alert from '@mui/material/Alert';
import Footbar from '../widgets/Footbar';
import AlertTitle from '@mui/material/AlertTitle';
import { Parents } from '../widgets/Parents';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import React from 'react';
import Ajv, { ValidateFunction } from "ajv";
import addFormats from 'ajv-formats';
import Badge from '@mui/material/Badge';
import { ExtendedWidthTooltip } from '../widgets/ExtendedWidthTooltip';
import { Card, CardActions, CardContent, Typography } from '@mui/material';
import { getAllMetaStorePages } from '../../utils/utils';
import { useAuth } from 'renderer/AuthContext';
import { MetaStoreInstanceDomainMap, MetaStoreInstancesURL } from 'renderer/constants';
import { useMetaStoreInstanceState } from './SelectMetaStoreInstance';

/**
 * Renders a success message togheter with informations about the updated document
 * @param id id assigned from MetaStore to be printed in success message
 * @param version version assigned from MetaStore to be printed in success message
 */
const Success = ({id, version}) => {
  return(
    <Alert severity="success" style={{justifyContent: 'center', maxWidth: '400px', margin: 'auto'}}>
      <AlertTitle><strong>Success</strong></AlertTitle>
      Metadata document upload successful<br />
      <span style={{ fontFamily: 'monospace' }}>id:</span> <span style={{ fontFamily: 'monospace' }}>{id}</span><br />
      <span style={{ fontFamily: 'monospace' }}>version:</span> <span style={{ fontFamily: 'monospace' }}>{version}</span><br />
    </Alert>
  )
}
/**
 * Renders an error message as a consequence of a failed upload
 * @param message
 */
const Error = ({code, message}) => {
  return(
    <Alert severity="error" style={{justifyContent: 'center', maxWidth: '400px', margin: 'auto'}}>
      <AlertTitle><strong>Error</strong></AlertTitle>
      An error occurred on metadata upload<br />
      <span style={{ fontFamily: 'monospace' }}>Error code:</span> <span style={{ fontFamily: 'monospace' }}>{code}</span><br />
      <span style={{ fontFamily: 'monospace' }}>Error message:</span> <span style={{ fontFamily: 'monospace' }}>{message}</span><br />
    </Alert>
  )
}

/**
 * Uploads metadata document and metadata record to MetaStore
 * @param metadataRecord metadata record document
 * @param setSuccessAlert function set success message
 * @param setErrorAlert function set error message
 */
const uploadMetadataDocument = async (
  metadataRecord: Object, 
  setSuccessAlert: React.Dispatch<React.SetStateAction<any>>, 
  setErrorAlert: React.Dispatch<React.SetStateAction<any>>, 
  token: string|undefined) => {
    const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
  // Set documents to be uploaded in a FormData
  const record = {
    relatedResource: {
      identifierType: metadataRecord['related_resource_id_type'],
      identifier: metadataRecord['related_resource_id_type'] === "URL" ?
      metadataRecord['url'] : metadataRecord['internal']
    },
    schema: {
      identifier: sessionStorage.getItem("schemaId") as string,
      identifierType: "INTERNAL"
    },
    schemaVersion: parseInt(sessionStorage.getItem("schemaVersion") as string),
  }
  if(metadataRecord['NEP_proposal_id']){
    record['acl'] = [{
      sid: metadataRecord['NEP_proposal_id'].toString(),
      permission: 'ADMINISTRATE'
    }]
  }
  const formData = new FormData();
  formData.append('record', new Blob([JSON.stringify(record)], { type: 'application/json' }));
  formData.append('document', new Blob([sessionStorage.getItem('compiledDocument') as string], { type: 'application/json' }));

  try{
    // POST request to upload the document
    const response = await fetch(BASE_URL+'/metadata', {
      method: 'POST',
      headers: {'Authorization': 'Bearer ' + token },
      body: formData
    })
    // Document upload correctly
    if(response.status === 201){
      const responseJson = await response.json()
      setSuccessAlert({show: true, id: responseJson.id, version: responseJson.recordVersion})
    }
    // Document already exists, handle document update flow
    else if(response.status === 409){
      if(window.confirm(`A record with related resource '${record.relatedResource.identifier}' for the schema '${record.schema.identifier}' version '${record.schemaVersion}' already exists.
        Do you want to upload a new version for the metadata document?`)){
        // If the user answer yes we have to do a PUT request to upload the document
        const data = await getAllMetaStorePages(`metadata?resourceId=${record.relatedResource.identifier}&schemaId=${record.schema.identifier}`, {'Authorization': 'Bearer ' + token })
        // The response could be an array, select the element with the correct schema version
        const documentRecord = data.find(item => item.schemaVersion === record.schemaVersion);
        // To get the needed ETag do a GET request to with this header
        const headers={'Accept': 'application/vnd.datamanager.metadata-record+json', 'Authorization': 'Bearer ' + token}
        const response = await fetch(BASE_URL+`/metadata/${documentRecord.id}`,{ headers: headers})
        const ETag = response.headers.get('ETag');
        const putResponse = await fetch(BASE_URL+`/metadata/${documentRecord.id}`, {
          method: 'PUT',
          headers: { 'If-Match': ETag as string, 'Authorization': 'Bearer ' + token},
          body: formData
        })
        const putResponseJson = await putResponse.json();
        if(putResponse.status === 200){
          setSuccessAlert({show: true, id: putResponseJson.id, version: putResponseJson.recordVersion})
        }
      }
    } else if (response.status >= 400 && response.status <= 599) {
      const responseText = await response.text()
      const errorMessage = response.statusText + '\n' + responseText
      setErrorAlert({show: true, code: response.status, message: errorMessage})
    }
  } catch (e){
    const errorMessage = 'Unexpected error ' + e
    setErrorAlert({show: true, code:'unknown', message: errorMessage})
  }
  
}

/**
 * Finds the `parents` fields to be filled from dropdown menus
 * @returns Array of objects as `{path: 'somePath', parentType:'someType'}` 
 */
const parentsToFill = (): Object[]|undefined => {
  const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
  try{
    const compiledDocument = sessionStorage.getItem('compiledDocument') !== null ? 
    JSON.parse(sessionStorage.getItem('compiledDocument') as string) : {}
    const parentsArray: Object[]|undefined = compiledDocument.parents
    // Array containing only the parents with reference type 'MetaStore URI' that are not already filled
    let toFill: Object[] = []; 
    if(parentsArray){
      let i:number = 0; // Array index
      for(const parent of parentsArray){
        // Checks that the type is MetaStore URI and the reference is not defined or not valid
        if(  parent['parentType'] !== 'not applicable' && parent['parentReferenceType'] === 'MetaStore URI' && 
            ( parent['parentReference'] === undefined || !parent['parentReference'].startsWith(MetaStoreInstanceDomainMap[MetaStoreInstance as string])) ){
          // The object elements of the array will be objects like {path:'some (partial?) path', parentType:'some type'}
          toFill?.push({path:'parents['+i.toString()+']', parentType: parent['parentType']})
        }
        i++;
      }
      return toFill.length === 0 ? undefined : toFill;
    }else{
      return undefined;
    }
  }catch (e){
    console.log("ERROR parentsToFill", e)
  }

}

/**
 * Validate the document against the schema and returns the array of errors
 * @param validate validate function, returned by Ajv.compile(jsonSchema)
 * @returns array containing error messages (empty if there are none)
 */
export const validateAndGetErrors = (validate: ValidateFunction<unknown>): JSX.Element[] =>  {
  validate(JSON.parse(sessionStorage.getItem('compiledDocument') as string))
  const errorMessage = validate.errors?.map((element, index) => (
    <React.Fragment key={'schema-'+index.toString()}>
      <br />
      {'\u2022 ' /* Unicode character for bullet */}
      {element['message']}
    </React.Fragment>
  ));
  return errorMessage ? errorMessage : []
}

interface uploadProps{
  setSuccessAlert: React.Dispatch<React.SetStateAction<{
    show: boolean;
    id: undefined;
    version: undefined;
  }>>
  setErrorAlert: React.Dispatch<React.SetStateAction<{
    show: boolean;
    code: undefined;
    message: undefined;
  }>>
  metadataRecord: Object
  errorMessages: JSX.Element[]
}

/**
 * Renders the `Upload` button. It renders the button to upload the document if the document validation is
 * successful, or a not active error button that shows error messages on hover.
 */
const UploadButton = (props: uploadProps) => {
  const auth = useAuth()
  const {setSuccessAlert, setErrorAlert, metadataRecord, errorMessages} = props;
  const schemaErrors = errorMessages.filter(error => error.key?.toString().startsWith('schema-'));
  const recordErrors = errorMessages.filter(error => error.key?.toString().startsWith('record-'));
  
  return(
    errorMessages && errorMessages.length > 0 ? 
    <ExtendedWidthTooltip title={
      <span style={{fontSize:'11pt'}}>
        {schemaErrors.length > 0 && (
          <>
            <strong>Schema compiled with the following errors:</strong>
            {schemaErrors}
            <br />
            <br />
          </>
        )}
        {recordErrors.length > 0 && (
          <>
            <strong>Record compiled with the following errors:</strong>
            {recordErrors}
          </>
        )}
      </span>
    }>
      <Button 
        disableRipple
        variant="outlined" 
        color="error"
        startIcon={
          <Badge 
            badgeContent={errorMessages.length} 
            sx={{color: '#d32f2f', backgroundColor: 'transparent'}}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
          >
            <ErrorOutlineIcon color='error'/>
          </Badge>
          }
      >
        Upload
      </Button>
    </ExtendedWidthTooltip>
    : 
    <Button 
      variant="contained" 
      onClick={() => {
        // Reset success message
        setSuccessAlert({show: false, id: undefined, version: undefined})
        // Reset error message
        setErrorAlert({show: false, code:undefined, message: undefined})
        uploadMetadataDocument(metadataRecord, setSuccessAlert, setErrorAlert, auth.token as string)
      }}
    >
      Upload
    </Button>
  );
}

export const UploadToMetaStore = () => {
  const auth = useAuth()
  const [recordSchema, setRecordSchema] = useState<{}>(initialRecordSchema);
  const [metadataRecord, setMetadataRecord] = useState({});
  const [successAlert, setSuccessAlert] = useState({show: false, id: undefined, version: undefined});
  const [errorAlert, setErrorAlert] = useState({show: false, code: undefined, message: undefined});
  const navigate = useNavigate();
  // Validation for the record schema (containing related resources, ACL, ...)
  const [recordValidationErrors, setRecordValidationErrors] = useState<JSX.Element[]>([])
  // Array containing parent objects
  const [parentsArray, setParentsArray] = useState<Object[]|undefined>(undefined)
  // Validation variables:
  // Define ajv, used to validate
  const ajv = new Ajv({
    allErrors: true,
    verbose: true,
    strict: false,
  });
  // Add formats, like date-time and so on
  addFormats(ajv);
  // Define the validation function on the loaded schema
  const validate = ajv.compile(JSON.parse(sessionStorage.getItem('schema') as string))
  // State that contains validation errors
  const [validationErrors, setValidationErrors] = useState<JSX.Element[]>([]);

  useEffect(() =>{
    const proposalIds = auth.userInfo!['proposal_ids']
    if(proposalIds && proposalIds.length > 0){
      // If the users has some proposals put them in an enum to choose one of them
      const sortedProposalIds = proposalIds.sort((a, b)=>{return a-b})
      setRecordSchema(data => set('properties.NEP_proposal_id.enum',sortedProposalIds,data));
    } else{
      // If user does not have proposals remove the 'NEP_proposal_id' from record schema and uischema
      const updatedRecordSchema = {...recordSchema}
      // remove 'NEP_proposal_id' field from schema
      delete updatedRecordSchema!['properties']!.NEP_proposal_id
      setRecordSchema({...updatedRecordSchema, required: ["related_resource_id_type"]});
      // remove 'NEP_proposal_id' field from uischema
      const filteredElements = recordUischema.elements.filter(element => {
        return !(element.scope && element.scope.includes("NEP_proposal_id"));
      });
      recordUischema.elements = filteredElements;
    }
    setParentsArray(parentsToFill())
    // Perform a validation at rendering
    const errorMessages = validateAndGetErrors(validate)
    setValidationErrors(errorMessages)
  },[]);

  return (
    <div className='App'>
      <Head/>
      <Card sx={{ 
        minWidth: 275, 
        maxWidth: 600, 
        margin:'auto', 
        paddingTop: 5,
        paddingBottom: 2
      }}>
        <CardContent>
          <Card sx={{ 
            minWidth: 275, 
            maxWidth: 500, 
            minHeight: 270.83,
            margin:'auto', 
            border: 'none', 
            boxShadow: 'none' 
          }}>
            <CardContent>
              <Typography variant="h5" component="div">
                Metadata record
              </Typography>
              <JsonForms
                schema={recordSchema}
                data={metadataRecord}
                renderers={materialRenderers}
                cells={materialCells}
                uischema={recordUischema}
                onChange={ ({ data, errors }) => {
                  setMetadataRecord(data);
                  if(errors){
                    const errorMessages = errors.map((element, index) => (
                      <React.Fragment key={'record-'+index.toString()}>
                        <br />
                        {'\u2022 ' /* Unicode character for bullet */}
                        {element['message']}
                      </React.Fragment>
                    ));
                    setRecordValidationErrors(errorMessages)
                  }
                }}
              />
            </CardContent>
          </Card>
          { parentsArray ? 
            <Card sx={{ 
              minWidth: 275, 
              maxWidth: 500, 
              margin:'auto', 
              border: 'none', 
              boxShadow: 'none' 
            }}>
            <CardContent>
              <Typography variant="h5" component="div">
                Parents 
              </Typography>
              <Parents 
                parentsArray={parentsArray} 
                validate={validate}
                setValidationErrors={setValidationErrors}
              />
            </CardContent>
          </Card>
            : 
            null 
          }
          <UploadButton
            setSuccessAlert={setSuccessAlert}
            setErrorAlert={setErrorAlert}
            metadataRecord={metadataRecord}
            errorMessages={[...validationErrors,...recordValidationErrors]}
          />
        </CardContent>
      </Card>


      { successAlert.show ? <Success id={successAlert.id} version={successAlert.version}/> : null}
      { errorAlert.show ? <Error code={errorAlert.code} message={errorAlert.message}/> : null}
      <Footbar navigate={navigate}/>
    </div>
  );
}