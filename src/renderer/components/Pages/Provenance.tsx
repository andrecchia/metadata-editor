import { Head } from "../widgets/Head";
import Footbar from "../widgets/Footbar";
import { useNavigate } from "react-router-dom";
import { useEffect, useRef, useState } from "react";
import { saveFile, scanSchemasFromMetaStore } from "../../utils/utils";
import { Dropdown } from "../widgets/selectSchemaProperties";
import { Parents, metadataDocumentIdSortedByDate } from "../widgets/Parents";
import { Button, Card, CardContent, Grid, Typography } from "@mui/material";
import set from 'lodash/set';
import sha256 from 'crypto-js/sha256'
import { useAuth } from 'renderer/AuthContext';
import { MetaStoreInstanceDomainMap, MetaStoreInstancesURL } from "renderer/constants";
import { useMetaStoreInstanceState } from "./SelectMetaStoreInstance";

const getSchemaIds = async () =>{
    const {schemaLabel, schemaIdLabel, schemaLastVersion} = await scanSchemasFromMetaStore()
    let allSchemaIds: string[] = []
    for(const [key,value] of Object.entries(schemaIdLabel)){
        allSchemaIds = [...allSchemaIds,...value as string[]]
    }
    return { allSchemaIds, schemaIdLabel }
}

/**
 * Given a metadata document id it reconstruct the complete provenance.
 * Provenance is obtained looking at `parents` fields inside the documents
 * (that are supposed to be on MetaStore)
 * @private
 * @param documentId the document id for which to reconstruct the provenance
 * @param resultArray the array to store the document `parents` informations
 * @returns an array of arrays of objects. Each object contains informations about
 * parent and children in the provenance tree (the tree is reconstructed in Breadth-first search way)
 */
export const getProvenance = async (documentId: string, resultArray: any[] = [], token: string) => {
    const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
    /** Stores the documents to be checked, accessed in FIFO way */
    const queue: any[] = [];
    queue.push(documentId);
    while (queue.length > 0) {
      // Take the 1st document in the queue and retrieve its parents
      const currentDocumentId = queue.shift();
      // Get the metadata document to retrieve the parents
      const documentResponse = await fetch(BASE_URL + '/metadata/' + currentDocumentId, {
        method: 'GET',
        headers: { 
          'Authorization': 'Bearer ' + token 
        }
      });
      const document = await documentResponse.json();
      // Get the metadata record to retrieve the proposal id from acl
      const recordResponse = await fetch(BASE_URL + '/metadata/' + currentDocumentId, {
        method: 'GET',
        headers: { 
          'Accept': 'application/vnd.datamanager.metadata-record+json',
          'Authorization': 'Bearer ' + token 
        }
      })
      const record = await recordResponse.json()
      // Get the proposal id
      // take only sid containing all numbers
      let onlyNumbersSid: string | undefined;
      try{
        onlyNumbersSid=record['acl'].filter(item => /^\d+$/.test(item.sid))[0]['sid']
      }catch(e){
        onlyNumbersSid=undefined
      }
      const proposalId = onlyNumbersSid

      // Search the parents in the root document..
      let parents = document['parents']
      // ..if not found search it in the 1st nesting level
      if(!parents){
        // Get an array of keys of the document object
        const foundObject = Object.keys(document)
        // Map over the keys to get an array of values corresponding to each key
        .map(key => document[key])
        // Filter out non-object values from the array
        .filter(value => typeof value === 'object' && value !== null)
        // Find the first object in the array that contains the 'parents' key
        .find(value => 'parents' in value);

        // Assign to parents variable if found, or undefined
        parents = foundObject ? foundObject['parents'] : undefined;
      }

      // If parents is defined
      if (parents) {
        let partialResult: any[] = [];
        // Store all the parent in the (partial) result array
        for (const parent of parents) {
          if (parent['parentType'] !== 'not applicable') {
            partialResult.push({documentId:currentDocumentId,...parent, proposalId:proposalId});
          }
        }
        if(partialResult.length > 0){
          resultArray.push(partialResult);
        }
  
        // For all the current document parents, if they have some of them
        // in MetaStore put in the queue (if they aren't already)
        for (const parent of partialResult) {
          if (parent['parentReferenceType'] === 'MetaStore URI') {
            const schId = (parent['parentReference'] as string).split('metadata/');
            if (!queue.includes(schId[1])) {
                queue.push(schId[1]);
            }
          }
        }
      }
    }
  };

/**
 * Given a json document and a key get the complete path(s) to that key in dot notation: `path.to.some.array[0].element`
 * @private
 * @param obj json document
 * @param key the key for which to compute the path
 * @returns an array of strings containing the complete paths to some key
 */
export function findKeyPaths(obj, key, currentPath = '', paths:string[] = []) {
  if (typeof obj !== 'object' || obj === null) {
    return paths; // Object is not valid, return the paths found so far
  }
  for (let prop in obj) {
    if (prop === key) {
      paths.push(currentPath + (currentPath ? '.' : '') + prop) // Found the key, add the path to the paths array
    } else if (Array.isArray(obj[prop])) {
      for (let i = 0; i < obj[prop].length; i++) {
        findKeyPaths(obj[prop][i], key, currentPath + (currentPath ? '.' : '') + prop + '[' + i + ']', paths) // Search key in nested array
      }
    } else if (typeof obj[prop] === 'object' && obj[prop] !== null) {
      findKeyPaths(obj[prop], key, currentPath + (currentPath ? '.' : '') + prop, paths) // Search key in nested object
    }
  }

  return paths // Return the array of paths
}

/**
 * get metadata document uuid from its url
 * @param URL 
 * @returns the metadata document uuid
 */
const uuidFromURL = (URL: string) => {
  // Find the index of "metadata/" and "?"
  const startIndex = URL.indexOf("metadata/") + "metadata/".length;
  const endIndex = URL.indexOf("?");

  // Extract the substring between "metadata/" and "?"
  return URL.substring(startIndex, endIndex !== -1 ? endIndex : URL.length);
}
/**
 * Given the array containing the provenance tree generate a json object containing the provenance informations
 * @private
 * @param provenanceBFS array containing provenance tree (in Breadth-first search way)
 * @param rootType the document label for the document provenace root
 * @returns json object with provenance informations
 */
export const jsonFromProvenance = (provenanceBFS: any[], rootType: string) => {
  const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
  let jsonProvenance = {}
  let first = true;
  const proposalIds: string[] = []
  for(const provenanceField of provenanceBFS){
    // Enter here only for provenance root
    if(first){
      first = false;
      const currentNode = provenanceField[0]['documentId'];
      // Only for the provenance root define the json document
      // The document structure is:
      // 1) { document_id: { this_doc_info: info, array_of_parents:[parent1,parent2,..] } }
      // 2) The entries for array_of_parents (parent1, parent2,..) are objects as in 1)
      jsonProvenance = {[currentNode]:{
        referenceType:'MetaStore URI',
        reference:`${MetaStoreInstanceDomainMap[MetaStoreInstance as string]}/api/v1/metadata/${currentNode}`,
        type: rootType,
        parents:[]
      }}
    }

    // Takes trace of the parents array position for the current provenance element
    let i=0;
    for(const p of provenanceField){
      // Get current node id and child node id
      const currentNode = p['documentId'];
      /**
       *  When the parentReferenceType is a MetaStore link, use the document id generated by metastore,
       *  otherwise use the sha256 sum of parent type, reference and reference type
       */
      const childrenNode = p['parentReferenceType'] === 'MetaStore URI' ? 
        // Get document id
        uuidFromURL(p['parentReference'] as string) :
        // Compute sha256 sum
        sha256(p['parentReferenceType']+p['parentReference']+p['parentType']).toString();;
      // Take the paths to the current node (there could be multiple paths for the same id)
      const keyPaths = findKeyPaths(jsonProvenance, currentNode)
      // Loop over all the paths found, add the same next children in each of the paths
      for(const keyPath of keyPaths){
        // Define the path and array entry where to insert the child object
        let path = keyPath + '.parents['+i.toString()+']'
        // Child object
        const childrenObject: any = {
          [childrenNode]: {
            referenceType: p['parentReferenceType'],
            reference: p['parentReference'],
            type: p['parentType'],
            parents: p['parentType'] === 'input' ? undefined : []
          }
        };
        // Insert childObject at the given path
        set(jsonProvenance, path, childrenObject)
      }
      // Store the proposalId
      const proposalId = p['proposalId'];
      if(!proposalIds.includes(proposalId)){
        proposalIds.push(proposalId)
      }
      i++
    }
  }
  // There could be some `undefined` in the proposalIds array, remove them
  const notNullProposalIds = proposalIds.filter(item => item !== undefined);
  jsonProvenance['proposalIds'] = notNullProposalIds
  return jsonProvenance
}

export const Provenance = () => {
    const auth = useAuth()
    const navigate = useNavigate();
    /** Chosen schema id */
    const [schemaId, setSchemaId] = useState<string|undefined>(undefined);
    /** Avaiabel documents for a given schema ID */
    const [documentById, setDocumentById] = useState<Object[]>([])
    /** Array of string with schema ids */ 
    const [allSchemaId, setAllSchemaId] = useState([] as string[]);
    /** Selected document (by id) for which to obtain provenance  */
    const [documentId, setDocumentId] = useState<string|undefined>(undefined)
    const schemaIdLabel = useRef({})
    useEffect(()=>{
        // Get avaiable labels
        getSchemaIds()
        .then( schemaInfo => {
          setAllSchemaId(schemaInfo.allSchemaIds)
          schemaIdLabel.current = schemaInfo.schemaIdLabel
        })
        .catch(e=>console.log(e))
    },[])
    
    return(
    <div className='App'>
        <Head/>
        <Card sx={{ 
            minWidth: 275, 
            maxWidth: 500, 
            minHeight: 270.83,
            margin:'auto', 
            border: 'none', 
            boxShadow: 'none' 
          }}>
            <CardContent>
                <Typography variant="h5" component="div" style={{padding: '10px'}}>
                    Select metadata document
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Dropdown
                            id={'document-type'}
                            value={schemaId}
                            fieldName={'Related schema ID'}
                            property={allSchemaId}
                            handleChange={e=>{
                                setSchemaId(e.target.value)
                                const headers = {'Authorization': 'Bearer ' + auth.token }
                                metadataDocumentIdSortedByDate('schemaId='+e.target.value, headers)
                                .then( idsArray =>setDocumentById(idsArray))
                                .catch(e => console.log(e))
                            }}
                            disabled={false}
                            variant="standard"
                            sx={{ maxWidth: '468px'}}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        {   !schemaId ? null :
                            documentById.length === 0 ?
                            <span> No metadada documents registered on MetaStore for schemaId "{schemaId}" <br/><br/></span> :
                            <Dropdown
                                id={'metadata-document-id'}
                                value={documentId}
                                fieldName={'Metadata document ID'}
                                property={documentById}
                                handleChange={(e) => {
                                    const formattedValue: string = e.target.value;
                                    // Make the selected value to appear as the choice in the dropdown
                                    setDocumentId(formattedValue);
                                }}
                                disabled={false}
                                variant="standard"
                                sx={{ maxWidth: '468px'}}
                            /> 
                        }
                    </Grid>
                </Grid>
                <Button
                    variant="contained"
                    sx={{marginTop:'15px'}}
                    onClick={()=>{
                         /**
                         * Regex that matches the pattern "id:", followed by optional whitespace \s*, and captures any characters (.*?) until the first occurrence of a comma.
                         */
                         const regex = /id:\s*(.*?),/i;
                         /**
                          * 0th element contains the full match, 1st element contains the subgroup (.*?), that is the actual id
                          */
                         const match = documentId!.match(regex);
                         const result: any[] = [];
                         getProvenance(match![1], result, auth.token as string).then(()=>{
                            // Get label from schema id
                            let label: string = '';
                            // schemaIdLabel is an object like {label1:[schemaId1,schemaId2], label2:[...]}
                            for(const [k,v] of Object.entries(schemaIdLabel.current)){
                              // Checks if schemaId is inside the array of strings v, for the label k
                              if((v as string[]).includes(schemaId!)){
                                label = k;
                              }
                            }
                            if(result.length > 0){
                              // Get the provenance json
                              const provenanceJson = jsonFromProvenance(result,label)
                              // Save the provenance json as a file
                              saveFile(provenanceJson, false, match![1]+'_provenance.json')
                            }else{
                              alert(`Missing provenance information in metadata document with id "${match![1]}"`)
                            }
                         })
                    }}
                    disabled={documentId ? false : true}
                >
                    Get Provenance
                </Button>   
            </CardContent>
        </Card>
    
        <Footbar navigate={navigate}/>
    </div>
    );
}