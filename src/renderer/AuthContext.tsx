import { createContext, useState, useContext, ReactNode, useEffect } from 'react';

export type IAuthContextProps = {
  authenticated: boolean;
  token: string|null;
  userInfo: Object|null;
};

const authInitialState: IAuthContextProps = {
  authenticated: false,
  token: null as string|null,
  userInfo: null as Object|null
}

// Context
/**
 * createContext wrapper
 */
function createAuthContext(): React.Context<IAuthContextProps> {
  return createContext<IAuthContextProps>(authInitialState);
}

const authContext = createAuthContext()

// useContext
/**
 * Allows to get the context value, React.useContext wrapper
 * @returns current context value
 */
export function useAuth() {
  const context = useContext(authContext);
  if (!context) {
    throw new Error('Please add AuthProvider');
  }
  return context
}

// Provider
/**
 * Creates an AppAuth provider togheter with hooks to update auth state and token
 * @param AuthContext 
 * @returns 
 */
function createAuthProvider(AuthContext: React.Context<IAuthContextProps>) {
  const AppAuthProvider = ({ children }: { children: ReactNode }) => {
    const [auth, setAuth] =  useState(authInitialState)
    useEffect(() => {
      // Ipc listener to token refresh events. Update token on events
      window.electron.auth.authContextListener(setAuth)
      return () => window.electron.auth.removeAuthContextListener()
    }, []);

    return (
      <AuthContext.Provider value={auth}>
        {children}
      </AuthContext.Provider>
    );
  };

  return AppAuthProvider;
}

export const AuthProvider = createAuthProvider(authContext);