import { JsonSchema } from "@jsonforms/core";
import { ChangeEvent } from "react";
import { useMetaStoreInstanceState } from "renderer/components/Pages/SelectMetaStoreInstance";
import { MetaStoreInstancesURL } from "renderer/constants";
import { resolveTermURIs } from "./integrationTermsURI";

/**
 * Get all paths from an input JSON schema that follows the
 * JSON schema specifications (https://json-schema.org/)
 * @param schema 
 * @param parentPath 
 * @returns an object containing all path set to 'false':
 * ```{ path.to.1st.var: false,
 *   path.to.2nd.var: false,
 *   ...
 * }```
 */
export function getJsonSchemaPaths(schema: JsonSchema, parentPath: string = ''): {} {
  let paths= {};
  if (schema.properties) {
    for (const property in schema.properties) {
      const path = parentPath ? `${parentPath}.${property}` : property;
      const propertySchema = schema.properties[property];

      if (propertySchema.type === 'object') {
        paths = Object.assign(paths,getJsonSchemaPaths(propertySchema, path));
        // paths = paths.concat(getJsonPaths(propertySchema, path));
      } else {
        paths[path]=false;
      }
    }
  }

  return paths;
}


/**
 * Performs GET requests to MetaStore taking all the results avaiable among all the pages
 * @param endpoint where to perform the GET request, query parameters included
 * @param headers the header request, if needed
 * @returns array with the fetched results
 */
export async function getAllMetaStorePages(endpoint: string, headers?: HeadersInit){
  const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
  // Define the URL
  const URL = BASE_URL + '/' + endpoint + (endpoint.includes('?') ? '&' : '?')
  let data: any[] = [];
  // The size of the page to be returned (i.e. how many schemas)
  const size: number = 10;
  // Here will be stored the total schemas present on MetaStore
  let avaiableDocumentsNumber: number|undefined = undefined;
  let page: number = 0;
  // Loop until all the pages are taken
  while(avaiableDocumentsNumber === undefined || page < Math.ceil(avaiableDocumentsNumber/size) ){
    const response = await fetch(URL +'page=' + page.toString() + '&size=' + size.toString(),{
      headers: headers
    });
    // At the 1st iteration define get the information on how many schemas there are from response header
    if(avaiableDocumentsNumber === undefined){
      avaiableDocumentsNumber = parseInt(response.headers.get('content-range')?.split('/')[1] as string);
    }
    const response_json = await response.json();
    // Store all the schemas here
    data = [...data, ...response_json]
    page++;
  }
  return data;
}

/**
 * Retrieve all the schemas from current MetaStore instance
 * @returns schemaLabel: array of strings containing the labels for all the avaiable schemas
 * schemaIdLabel: as object with labels as keys and correspondent schema ids (arrays of strings) as values. 
 * @example {"input":["input_schema"], "measurement": ["SEM_schema", "STM_schema"], ... }
 *  schemaLastVersion: an object containing the schema id as a key and its last avaiable version
 *  as a value (@example: {'foo_schema': 3, 'bar_schema': 1, ...})
 */
export async function scanSchemasFromMetaStore(){
  let schemaLastVersion = {};
  let schemaIdLabel = {};
  const data = await getAllMetaStorePages('schemas')
  //                                                     if label is undefined or empty string...                   ... set 'no label' , else set label
  const schemaLabel = [...new Set(data.map(item => item.label === undefined || (item.label as string).trim().length === 0 ? 'no label' : item.label))] as string[];
  for (const label of schemaLabel){
    schemaIdLabel[`${label}`] = []
  }
  for(const obj of data){
    //               if label is undefined or empty string...                   ... set 'no label' , else set label
    const label = obj.label === undefined || (obj.label as string).trim().length === 0 ? 'no label' : obj.label
    schemaIdLabel[label].push(obj.schemaId) 
    schemaLastVersion[obj.schemaId] = obj.schemaVersion;
  }
  return {schemaLabel, schemaIdLabel, schemaLastVersion};
}

/**
 * Makes a call to MetaStore APIs to get the selected JSON schema.
 * @param schemaId 
 * @param schemaVersion
 */
export async function getSchemaFromMetaStore(schemaId, schemaVersion){
  const {MetaStoreInstance} = useMetaStoreInstanceState.getState()
    const BASE_URL = MetaStoreInstancesURL[MetaStoreInstance as string]
  const response = await fetch(BASE_URL + '/schemas/' + schemaId + '?version=' + schemaVersion);
  const schema = await response.json();
  if(400 <= response.status && response.status <= 599 ){
    throw new Error(JSON.stringify(schema));
  }
  delete schema.$schema;
  await resolveTermURIs(schema)
  if('$defs' in schema){
    return await window.electron.JSONschema.resolve(schema)
  }
  return schema;
}

/**
 * Allows to download text files
 * @param fileContent the object to be written in the text file
 * @param {boolean} isJme (default false) indicates if the file to be saved is a jme
 * @param {string} suggestedName (default `metadata_document.json`) suggested file name
 */
export function saveFile(fileContent, isJme = false, suggestedName='metadata_document.json') {
  // If we are saving .jme file we need to insert schema id and version
  if(isJme){
    fileContent = {
      ...fileContent,
      _metaStoreSchema : {
        schemaLabel : sessionStorage.getItem("schemaLabel"),
        schemaId : sessionStorage.getItem("schemaId"),
        schemaVersion : sessionStorage.getItem("schemaVersion")
      }
    }
    suggestedName = 'metadata_document.jme';
  }
  const fileData = JSON.stringify(fileContent);
  const blob = new Blob([fileData], { type: "text/plain" });
  const url = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.setAttribute('download', suggestedName);
  link.setAttribute('href', url);
  link.click();
}

/**
 * Load text files stored on user's computer using FileReader. 
 * It is triggered as an onChange event on an `<input type="file" [...] hidden/>` element.
 * @param e onChange event
 * @param handleLoadFile callback function to handle the file once loaded 
 */
export const loadFile = (e: ChangeEvent<HTMLInputElement>, handleLoadFile: (e,obj) => void) => {
  if (e.target.files) {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0]);
    fileReader.onloadend = () => {
      const content = fileReader.result;
      try{
        let obj = JSON.parse(content as string);
        handleLoadFile(e, obj);
      } catch (e){
        console.log("Error in file parsing:", e)
        alert('Invalid file')
      }
    }
  }
};

/**
 * Gives the schema mask id for the current selected schema
 * @returns the schema mask id as ```schema_id@schema_version```
 */
export const getSchemaMaskId = (): string => {
  const currentSchemaId = sessionStorage.getItem('schemaId') as string;
  const currentSchemaVersion = sessionStorage.getItem('schemaVersion') as string;
  return currentSchemaId + '@' + currentSchemaVersion;
}

/**
 * Unrolls a JSON (document) resolving paths for the nested properties, i.e. it goes from this:
 * ```
 * { foo:{
 *    bar: {
 *      field: value,
 *      array: [first, second]
 *     }
 *   }
 * }
 * ```
 * to this:
 * ```
 * { foo.bar.field: value,
 *   foo.bar.array.0: first,
 *   foo.bar.array.1: second }
 * ```
 * @param schema The schema to be resolved
 * @param parentPath used by self calls to correctly concatenate paths
 * @param isArray used by self calls to have bracket notations for arrays, ```foo.array[0]```
 * @returns a unrolled JSON
 */
export const getJsonDocumentPaths = (schema: JsonSchema, parentPath: string = '') : {} => {
  let paths = {};
    for (const k in schema) {
      const path = parentPath === '' ? k : `${parentPath}.${k}`;      
      if (typeof schema[k] === 'object') {
        paths = Object.assign(paths,getJsonDocumentPaths(schema[k], path));
      } 
      else {
        paths[path]=schema[k];
      }
    }
  return paths;
}