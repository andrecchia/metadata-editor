import set from 'lodash/fp/set';
import { getJsonDocumentPaths, getSchemaMaskId } from './utils';
import { electronStore } from '../components/CustomRenderers/util/lockedUtils';

/**
 * Checks whether a path refers to an array and if it is defined in a given schema
 * @param path the string containing path in dot notation e.g. ```path.to.1st.value```
 * @param schemaPaths object containing all schema paths with dot notation
 * @returns ```true``` if ```path``` refers to an array and its path exists 
 * in ```schemaPaths```, ```false``` otherwise
 */
  const checkForArray = (path: string, schemaPaths: {}) : boolean => {
    const pattern = /\.[0-9]+\./;
    if(pattern.test(path)){
      const partialKey = path.split(pattern)[0]
      const value = Object.keys(schemaPaths).includes(partialKey) ? 
      true : false
      return value
    } else {
      return false
    }
  }

  /**
   * Listen for messages coming from HTTP requests to the REST endpoints, performs
   * the operations on frontend based on the request content.
   * @param setData set function (returned by useState) to update the document
   * @param schemaPaths object containing all schema paths with dot notation: 
   * `path.to.var`
   * @param compiledData object containing the compiled document
   */
export const apiEndpointMessagesHandler = (setData: React.Dispatch<any>, schemaPaths: {}, compiledData: {}, checkFields: boolean) =>{
  window.electron.API.requestListener((event: any, value) => {
    try{
      const reqObj=JSON.parse(value);

      /**
       * Captures the API requests at http://localhost:3333/api/v1/compile... coming from the user.
       * Compiles the metadata schema with the details specified in the request. 
       */
      if(reqObj['path'] === '/api/v1/compile'){
        let err_keys : string[]=[]; //Stores the wrong keys coming from server
        const resolvedDocument=getJsonDocumentPaths(reqObj['content']);
        if(checkFields === false){
          for (const [key,val] of Object.entries(resolvedDocument)){
            // Compile the schema if key exists, store in key_err otherwise
            Object.keys(schemaPaths).includes(key) || 
            checkForArray(key, schemaPaths) ?
            setData(data => set(key,val,data)):
            err_keys.push(key);
          }
        } else {
          const schemaMaskId = getSchemaMaskId();
          const resolvedMask = getJsonDocumentPaths(electronStore.get(schemaMaskId))
          for (const [key,val] of Object.entries(resolvedDocument)){
            // Compile the schema if key exists, store in key_err otherwise
            if( Object.keys(schemaPaths).includes(key) || checkForArray(key, schemaPaths) ){
              if(resolvedMask[key] === true){ 
                setData(data => set(key,val,data)) 
              }
            } else{
              err_keys.push(key);
            }
          }
        }

        // Send a response with err_keys content (everything fine if err_keys is empty)
        let respBack={requestName: "RESP_COMPILE"};
        respBack["resp"] = err_keys
        event.sender.send('API:response',JSON.stringify(respBack));
      }

      /**
       * Captures the API requests at http://localhost:3333/api/v1/read... coming from the user.
       * Gives the schema ( or single field, depending on the request) content as a response 
       */
      if(reqObj['path'] === '/api/v1/read'){
        let respBack={requestName: "RESP_READ"};
        respBack["resp"] =  compiledData;
        event.sender.send('API:response',JSON.stringify(respBack));
      }
    } catch (e) {
      console.debug(e);
      event.sender.send('API:response',"ERROR at apiEndpointMessagesHandler");
    }
  })
}
