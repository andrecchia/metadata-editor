/**
 * This script preprocess a JSON schema to resolve piece of it from some online dictionary.
 * In collaboration with Gulzaure Abdildina @ KIT
 */
/** */

import { JsonSchema } from "@jsonforms/core";
import set from 'lodash/set';
import get from 'lodash/get'



/**
 * Search for `termURI` keys in a JSON schema
 * @param schema the schema to check
 * @returns an array of strings containing the paths in dot notation to the
 * found `termURI`s
 */
const getJsonSchemaPathsContainingTermURI = (schema: JsonSchema) : string[] => {
    const stack: { 
        currentSchema: JsonSchema; 
        parentPath: string; 
    }[] = [{currentSchema: schema, parentPath: ''}]
    let paths: string[] = []
    while(stack.length > 0){
        const {currentSchema, parentPath} = stack.pop()!
        for (const key in currentSchema) {
            const path = parentPath === '' ? key : `${parentPath}.${key}`;      
            if (typeof currentSchema[key] === 'object') {
                if((currentSchema[key] as Object).hasOwnProperty('termURI')){
                    paths.push(path)
                } else {
                    stack.push({
                        currentSchema: currentSchema[key], 
                        parentPath:path
                    })
                }
            } 
        }
    }
    return paths;
}

/** Interface for termURI resolver */
interface TermURIResolver {
    /**
     * Resolves a single term from `uri` and inserts it into `schema` at the given `path`
     * @param schema the schema in which to insert the resolved term
     * @param path the path in which to insert the term within the `schema`
     * @param uri the uri from which the term is resolved
     */
    resolveTermURI(schema: JsonSchema, path: string, uri: string): Promise<void>
}

/**
 * Resolver for evoks dictionary https://matwerk.datamanager.kit.edu/evoks/en/
 */
class EvoksResolver implements TermURIResolver {
    async resolveTermURI(
        schema: JsonSchema, 
        path: string,
        uri: string
    ): Promise<void> {
        const response = await fetch(uri)
        const dictionary = await response.json()
        if("graph" in dictionary){
            //Take substring between `uri=` and the first `&` that follows `uri`
            const match = uri.match(/uri=([^&]*)/);
            const subURI = match ? match[1] : null
            const term = dictionary.graph.find((obj) => obj.uri === subURI )

            this.resolveTitleAndDesc(schema, path, term)
            // If there is `narrower` resolve enum
            const narrower: {uri: string}[] = term["narrower"]
            if(narrower){
                const narrowerURIs: string[] = narrower.map(item => item.uri)
                const resolvedEnum: string[] = dictionary.graph
                    // take only objects with uri contained in narrowerURIs
                    // returns an array of objects
                    .filter(item => narrowerURIs.includes(item.uri))
                    // return an array of `prefLabel` strings from the array of object
                    .map(item => item["prefLabel"])
                set(schema as any, `${path}.enum`, resolvedEnum) 
            }
        } else {
            this.resolveTitleAndDesc(schema, path, dictionary)
        }
    }

    /**
     * Take a schema and resolves title and description from evoks online dictionaries
     * for a specific path
     * @param schema the schema with fields to resolve
     * @param path the path to the element to be resolved
     * @param resourceInfo the resource containing information used to resolve the
     * schema properties
     */
    resolveTitleAndDesc(schema: JsonSchema, path: string, resourceInfo ){
        // If there is `prefLabel` insert into title
        const prefLabel = resourceInfo["prefLabel"]
        if(prefLabel){
            set(schema as any, `${path}.title`, prefLabel)
        }
        // If there is the `definition` insert it into description
        const skosDefinition = resourceInfo["skos:definition"]
        if (skosDefinition) {
            const definition = skosDefinition.value;    
            set(schema as any, `${path}.description`, definition)   
        }
    }
}

/**
 * Resolver for tib dictionary https://terminology.tib.eu
 */
class TIBResolver implements TermURIResolver {
    async resolveTermURI(
        schema: JsonSchema, 
        path: string,
        uri: string
    ): Promise<void> {
        const response = await fetch(uri)
        const dictionary = await response.json()

        const label = dictionary['_embedded']['terms'][0]['label']
        if(label){
            set(schema as any, `${path}.title`, label)
        }

        const description = dictionary['_embedded']['terms'][0]['description'][0]
        if (description) {   
            set(schema as any, `${path}.description`, description)   
        }
    }
}

const termUriResolver: Record<string, TermURIResolver> = {
    'matwerk.datamanager.kit.edu': new EvoksResolver(),
    'service.tib.eu': new TIBResolver(),
}

/**
 * Take a schema and resolves properties from some online dictionaries
 * @param schema the schema with fields to resolve
 */
export const resolveTermURIs = async (schema: JsonSchema) => {
    const pathsToResolve = getJsonSchemaPathsContainingTermURI(schema)
    for(const path of pathsToResolve){
        const uri: string = get(schema as any, `${path}.termURI`)
        const resolverType = uri.match(/https:\/\/([^/]+)/)![1]
        await termUriResolver[resolverType].resolveTermURI(schema, path, uri)
    }
}