import './css/App.css';
import { Routes, Route } from 'react-router-dom';
import { UploadToMetaStore } from './components/Pages/UploadToMetaStore';
import PrivateRouteWrapper from './components/widgets/PrivateRouteWrapper';
import { Home } from './components/Pages/Home';
import { MetaStoreSchemas } from './components/Pages/MetaStoreSchemas';
import { DocumentManager } from './components/Pages/DocumentManager';
import LocalDocuments from './components/Pages/LocalDocuments';
import { Provenance } from './components/Pages/Provenance';
import { SelectMetaStoreInstance } from './components/Pages/SelectMetaStoreInstance';

function App() {
  return(
  <>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/MetaStore_instance" element={<SelectMetaStoreInstance />}/>
      <Route path="/local_documents" element={<LocalDocuments />} />
      <Route path="/document_manager" element={<DocumentManager />} />
      <Route 
        path="/upload_to_MetaStore" 
        element={
          <PrivateRouteWrapper>
            <UploadToMetaStore />
          </PrivateRouteWrapper>
        }
      /> 
      <Route path="/MetaStore_schemas" element={<MetaStoreSchemas />} />
      <Route path="/provenance" element={
          <PrivateRouteWrapper>
            <Provenance />
          </PrivateRouteWrapper>
        } 
      />
    </Routes>
  </>
 );
}

export default App;
