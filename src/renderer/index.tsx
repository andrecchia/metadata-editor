import { createRoot } from 'react-dom/client';
import { StrictMode } from 'react';
import App from './App';
import { MemoryRouter as Router} from "react-router-dom";
import { AuthProvider } from './AuthContext';

const container = document.getElementById('root') as HTMLElement;
const root = createRoot(container);
root.render(
    <StrictMode>
      {/* Make '/app' to be the webpage root */}
      <AuthProvider>
        <Router>
          <App/>
        </Router>
      </AuthProvider>
    </StrictMode>
  );

// /** Sets the constant variables in localStorage at rendering */
window.electron.infos.getConstants((event, value)=>{
  localStorage.setItem('signInTimeout', value.signInTimeout)
  localStorage.setItem('waitServerShutdown', value.waitServerShutdown)
  localStorage.setItem('APIport', value.APIport)
})