[![pipeline status](https://gitlab.com/andrecchia/metadata-editor/badges/master/pipeline.svg)](https://gitlab.com/andrecchia/metadata-editor/-/commits/master)
[![coverage report](https://gitlab.com/andrecchia/metadata-editor/badges/master/coverage.svg)](https://gitlab.com/andrecchia/metadata-editor/-/commits/master)
[![Latest Release](https://gitlab.com/andrecchia/metadata-editor/-/badges/release.svg)](https://gitlab.com/andrecchia/metadata-editor/-/releases)   

<table>
<tbody>
<tr>
<td width="50%">
    <img src="assets/exactlab__logo_RGB_202109-blue-web.svg" alt="eXact lab" width=380/>
</td>
<td width="50%">
    <img src="https://nffa.eu/content/images/2021/Logo%20NFFA.png" alt="NFFA" width=300/>
</td>
</tr>
</tbody>
</table>

This software is developed and maintained by [eXact lab](https://www.exact-lab.it/) as part of the [NFFA-Europe-Pilot](https://nffa.eu/) project, supported by grant agreements NFFA-Europe (No. 654360 from 1/9/2015 to 28/02/2021) and NFFA-Europe-Pilot (No. 101007417 from 1/03/2021 to 28/02/2026).


# Metadata editor
Metadata Editor is a cross-platform application designed to render and compile metadata schemas based on the [JSON Schema](https://json-schema.org/) standards. It enables users to load JSON schemas from various instances of [MetaStore](https://github.com/kit-data-manager/metastore2), fill in the required information, validate the data, and then upload the completed metadata documents back to MetaStore.\
Additionally, this application provides REST endpoints for reading and compiling the schemas.

This project is developed using:
- The [JSON Forms](https://jsonforms.io/) library, with the [React](https://it.reactjs.org/) framework (bootstrapped from [Create React App](https://github.com/facebook/create-react-app)) and [MUI Material](https://mui.com/) renderers.
- [Electron](https://www.electronjs.org/) framework.
- [Electron React Boilerplate](https://electron-react-boilerplate.js.org/) as a starting point to develop and build the app.
- [Express](https://expressjs.com/) to expose the REST APIs.

## Installation
Execute `npm install` to install the needed libraries, then launch one of the [Avaiable Scripts](#available-scripts).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\

The app will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run package`

Builds the app for production to the `release/build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Use a local MetaStore instance (for testing)

In development, having a local MetaStore instance is useful for uploading test schemas to render with the Metadata Editor. This allows to test document uploads and other interactions between the two systems.

To configure your local testing environment, follow the instructions provided in the [ME_testing_environment_local](https://gitlab.com/metadata-editor/me_testing_environment_local) repository.

Once set up, you can connect the Metadata Editor to your local MetaStore by selecting `Local MetaStore instance` in `Settings->Set MetaStore instance`. For functionalities that require login, use the username `test` and password `test` in the testing environment you have configured.

