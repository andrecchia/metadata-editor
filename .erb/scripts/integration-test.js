import { execSync } from 'child_process';
import fs from 'fs'

let isHeadless = false

/**** Command line arguments ****/
const commandLineArgs = () =>{
    const args = process.argv.slice(2);
    const argsLength = args.length

    // Only these arguments are allowed
    const allowedValues = ['--headless'];
    
    // Only 0 or 1 arguments are accepted
    if (argsLength > 1) {
        console.error('Error: 0 or 1 one argument are accepted. Received', argsLength);
        process.exit(1);
    } else if ( argsLength == 1 ){
        // Check if the given argument is a valid argument
        const argument = args[0];
        if (!allowedValues.includes(argument)) {
        console.error(`Error: Invalid argument. Allowed values are: ${allowedValues.join(', ')}`);
        process.exit(1);
        } else {
            isHeadless = true
        }
    }
}

/**
 * Creates 'NEP' realm, 'metastore' client and a test user for testing purposes in a 
 * docker keycloak instance, if these resources do not already exist
 */
const initializeKeycloak = async ( realm ) => {
    const KC_SERVER_URL = "http://test-keycloak:8080/auth"

    // Get admin token
    const r1 = await fetch(`${KC_SERVER_URL}/realms/master/protocol/openid-connect/token`, {
        method: 'POST',
        body: new URLSearchParams({
            'client_id': 'admin-cli',
            'username': 'admin',
            'password': 'pass',
            'grant_type': 'password'
        })
    });
    const adminToken = (await r1.json())['access_token']

    // Check if already initialized
    const r2 = await fetch(`${KC_SERVER_URL}/admin/realms`, {
        method: 'GET',
        headers: {'Authorization': `Bearer ${adminToken}` }
    });
    const realms = await r2.json()

    if( realms.length == 1){
        // When not initialized (only master realm exist)

        // Create realm
        const r3 = await fetch(`${KC_SERVER_URL}/admin/realms`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${adminToken}`,
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({
                realm : realm,
                enabled : true
            })
        });
        console.log('  Realm creation. Response status:', r3.status)

        // Create client
        const r4 = await fetch(`${KC_SERVER_URL}/admin/realms/${realm}/clients`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${adminToken}`,
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({
                clientId: "metastore",
                enabled : true,
                redirectUris: ["*"],
                webOrigins: ["*"],
                publicClient: true
            })
        });
        console.log('  Client creation. Response status:', r4.status)

        // Create user
        const r5 = await fetch(`${KC_SERVER_URL}/admin/realms/${realm}/users`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${adminToken}`,
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({
                username: "test",
                enabled : true,
                firstName: "test",
                lastName: "test",
                email: "test@user.com",
                credentials:[{
                    type:"password",
                    value:"test",
                    temporary:false
                }]
            })
        });
        console.log('  User creation. Response status:', r5.status)
        console.log('  Initialization completed')
    } else {
        console.log('  Already initialized')
    }
}

const startCI = async () => {
    try {
        console.log('Initializing keycloak...')
        await initializeKeycloak("TEST")

        console.log('Running tests...')
        const headless = isHeadless ? 'xvfb-run' : ''
        execSync(`DEBUG_PROD=true ${headless} npm run wdio`, {stdio: 'inherit'})
    } catch (error) {
        console.error('[ERROR]:', error)
        process.exit(1);
    }
}

const startLocal = async () => {
    const hosts = fs.readFileSync(`/etc/hosts`, 'utf8');
    if(hosts.search('test-keycloak') == -1 || hosts.search('metastore4docker') == -1){
        console.error(' "test-keycloak" and/or "metastore4docker" missing in /etc/hosts')
        process.exit(1);
    }

    const dockerComposePath = 'src/__test__/integration/docker-compose.yaml'
    try {

        console.log('Launching containers...')
        execSync(`docker-compose -f ${dockerComposePath} up -d --wait`, {stdio: 'inherit'})

        console.log('Initializing keycloak...')
        await initializeKeycloak("TEST")

        console.log('Running tests...')
        const headless = isHeadless ? 'xvfb-run' : ''
        execSync(`DEBUG_PROD=true ${headless} npm run wdio`, {stdio: 'inherit'})
    

        console.log('Stopping containers...')
        execSync(`docker-compose -f ${dockerComposePath} stop`, {stdio: 'inherit'})

    } catch (error) {
        console.error('[ERROR]:', error)
        console.error('[ERROR] Stopping containers...')
        execSync(`docker-compose -f ${dockerComposePath} stop`, {stdio: 'inherit'})

    }
}

// Call start
(async() => {
    commandLineArgs()

    const isCI = process.env.CI || false
    isCI ? 
        await startCI() :
        await startLocal()
})();