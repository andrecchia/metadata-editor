import packageJson from '../../release/app/package.json'
import { execSync } from 'child_process';

// Get old version of this project
const oldVersion = 'v' + packageJson['version']

/**** Command line arguments ****/
// The argument should contain the command for `npm version [ major | minor | patch | premajor | preminor | prepatch | prerelease | from-git]`

const args = process.argv.slice(2);
const argsLength = args.length
// Only these arguments are allowed
const allowedValues = ['minor', 'major', 'patch', 'premajor', 'preminor', 'prepatch', 'prerelease', 'from-git'];

// Only one argument is accepted
if (argsLength !== 1) {
    let addError = ''
    if(argsLength > 1){ addError = `Received ${argsLength}` }
    console.error('Error: One argument is required.', addError);
    process.exit(1);
}
const argument = args[0];
// Check if the given argument is a valid argument
if (!allowedValues.includes(argument)) {
  console.error(`Error: Invalid argument. Allowed values are: ${allowedValues.join(', ')}`);
  process.exit(1);
}

try {
    // Run npm version and get the output containing the updated version
    const newVersion = execSync(`npm --prefix release/app/ version ${argument}`).toString();
    // Add and commit with a bump version message
    const bumpMessage = "bump version " + oldVersion + " -> " + newVersion
    execSync(`git add release/app && git commit -m "${bumpMessage}"`, {stdio: 'inherit'})
    // Add a tag to the commit
    execSync(`git tag ${newVersion}`, {stdio: 'inherit'})
    console.log("bump version", oldVersion, "->", newVersion)
  } catch (error) {
    console.error('Error executing npm script:', error);
    process.exit(1);
  }